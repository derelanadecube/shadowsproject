package ru.krogenit.lighting.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.krogenit.lighting.EnumLightEvent;

@Getter
@NoArgsConstructor
public class PacketUpdateLight implements IMessage {

	private int x, y, z, id;
	private EnumLightEvent event;

	public PacketUpdateLight(int x, int y, int z, int id, EnumLightEvent event) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.id = id;
		this.event = event;
	}
	
	@Override
	public void fromBytes(ByteBuf data) {
		byte evenType = data.readByte();
		EnumLightEvent[] events = EnumLightEvent.values();
		event = events[evenType];
		x = data.readInt();
		y = data.readInt();
		z = data.readInt();
		id = data.readInt();
	}

	@Override
	public void toBytes(ByteBuf data) {
		data.writeByte(event.toByte());
		data.writeInt(x);
		data.writeInt(y);
		data.writeInt(z);
		data.writeInt(id);
	}
}
