package ru.xlv.guide;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.xlv.core.XlvsCore;
import ru.xlv.guide.handle.GuideManager;
import ru.xlv.guide.network.PacketGuideSync;

import static ru.xlv.guide.XlvsGuideMod.MODID;

@Mod(
        name = "XlvsGuideMod",
        modid = MODID,
        version = "1.0"
)
public class XlvsGuideMod {

    static final String MODID = "xlvsguide";

    @Mod.Instance(MODID)
    public static XlvsGuideMod INSTANCE;

    @Getter
    private final GuideManager guideManager = new GuideManager();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID, new PacketGuideSync());
    }
}
