package ru.xlv.guide.common;

public enum GuideCategory {

    LOCATION,
    ENTITY,
    ARMOR,
    WEAPON,
    GAME_MECHANIC

}
