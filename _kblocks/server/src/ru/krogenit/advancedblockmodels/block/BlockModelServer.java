package ru.krogenit.advancedblockmodels.block;

import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import ru.krogenit.advancedblockmodels.block.te.TEBlockModelServer;

public class BlockModelServer extends BlockModel {

    public BlockModelServer(Material mat, boolean isBrekable, String name) {
        super(mat, isBrekable, name);
    }

    @Override
    public TileEntity createNewTileEntity(World w, int meta) {
        return new TEBlockModelServer();
    }
}
