package ru.krogenit.advancedblockmodels.block;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import ru.krogenit.advancedblockmodels.block.te.TEBlockModelServer;
import ru.krogenit.advancedblockmodels.item.ItemBlockModelServer;

import java.util.ArrayList;
import java.util.List;

public class BlockModelsServer extends BlockModels {

    private static final List<String> models = new ArrayList<>();

    public static void registerModelBlocks() {
        modelBlock = new BlockModelServer(Material.rock, false, "block_model");
        GameRegistry.registerBlock(modelBlock, ItemBlockModelServer.class, "block_model");

        registerNewModel("armor/Engineer-Deco");
        registerNewModel("armor/Military-Deco");
        registerNewModel("armor/Police-Deco");
        registerNewModel("armor/Predator-Deco");
        registerNewModel("armor/Scientific-Deco");
        registerNewModel("armor/Spider-Deco");
        registerNewModel("armor/Vlad-Deco");
        registerNewModel("armor/Light-Deco");
        registerNewModel("armor/Jager-Deco");
        registerNewModel("armor/Invisible-Deco");
        registerNewModel("armor/Eye-Deco");
        registerNewModel("armor/Heavy-Deco");
        registerNewModel("armor/Warrior-Deco");

        registerNewModel("props/1/barrel/barrel");
        registerNewModel("props/1/box/box");
        registerNewModel("props/1/floor_lamp_on_stand/floor_lamp_on_stand");
        registerNewModel("props/1/fuel_tank/fuel_tank");
        registerNewModel("props/1/litter_bin_for_the_poor/litter_bin_for_the_poor");
        registerNewModel("props/1/scanner/scanner");
        registerNewModel("props/1/space_terminal/space_terminal");
        registerNewModel("props/1/toxic_tank/toxic_tank");
        registerNewModel("props/1/urn/urn");
        registerNewModel("props/1/wall-mounted_first-aid kit/wall-mounted_first-aid kit");

        registerNewModel("props/2/ahuenny_containers/ahuenny_containers1");
        registerNewModel("props/2/ahuenny_containers/ahuenny_containers2");
        registerNewModel("props/2/ahuenny_containers/ahuenny_containers3");
        registerNewModel("props/2/armchair/armchair");
        registerNewModel("props/2/boxes/boxes1");
        registerNewModel("props/2/boxes/boxes2");
        registerNewModel("props/2/containers/containers1");
        registerNewModel("props/2/containers/containers2");
        registerNewModel("props/2/containers/containers3");
        registerNewModel("props/2/containers/containers4");
        registerNewModel("props/2/corner_table/corner_table");
        registerNewModel("props/2/cryopod/cryopod");
        registerNewModel("props/2/energy_cell/energy_cell");
        registerNewModel("props/2/gun_box/gun_box");
        registerNewModel("props/2/locked_container/locked_container");
        registerNewModel("props/2/terra_hologram_table/terra_hologram_table");

        registerNewModel("props/3/and_another_capsule/and_another_capsule");
        registerNewModel("props/3/barrel/barrel");
        registerNewModel("props/3/box_of_the_ussr/box_of_the_ussr");
        registerNewModel("props/3/capsule_for_suspended_animation/capsule_for_suspended_animation");
        registerNewModel("props/3/capsule_for_suspended_animation/capsule_for_suspended_animation_opened");
        registerNewModel("props/3/cock_understand_that/cock_understand_that");
        registerNewModel("props/3/drill/drill");
        registerNewModel("props/3/generator/generator");
        registerNewModel("props/3/washing_machine/washing_machine");

        registerNewModel("barrel/barrel_1");
        registerNewModel("barrel/barrel_2");
        registerNewModel("box/box_2");
        registerNewModel("concrete/concrete_1");
        registerNewModel("cont/cont_1");
        registerNewModel("germo/germo_1");
        registerNewModel("trumpet/trumpet_2");
        registerNewModel("wheel/wheel_1");
        registerNewModel("wires/wires_1");

        registerNewModel("bags/bags_1");
        registerNewModel("chair/chair_2");
        registerNewModel("drink/drink_1");
        registerNewModel("keyboard/keyboard_1");
        registerNewModel("lenin/lenin_1");
        registerNewModel("maria/maria_1");
        registerNewModel("monitor/monitor_1");
        registerNewModel("paper/paper_1");
        registerNewModel("pipe/pipe_1");
        registerNewModel("shlako/shlako_1");
        registerNewModel("squareduct/squareduct_1");
        registerNewModel("squareduct/squareduct_2");
        registerNewModel("trash/trash_1");
        registerNewModel("tv/tv_1");

        registerNewModel("furniture/blueprint_1");
        registerNewModel("furniture/blueprint_2");
        registerNewModel("furniture/blueprint_3");
        registerNewModel("furniture/blueprint_4");
        registerNewModel("furniture/computer_chair_1");
        registerNewModel("furniture/office_chair_1");
        registerNewModel("furniture/table_1");
        registerNewModel("furniture/table_2");
        registerNewModel("furniture/table_3");

        System.out.println("Зарегистрировано " + models.size() + " моделей");
    }

    protected static void registerNewModel(String model) {
        models.add("advancedblocks:models/" + model + ".obj");
    }

    public static boolean hasModelInRegistry(String name) {
        return models.contains(name);
    }

    public static void registerTileEntities() {
        TileEntity.addMapping(TEBlockModelServer.class, "TEBlockModel");
    }
}
