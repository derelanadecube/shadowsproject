package ru.krogenit.advancedblockmodels.block.te;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModel;

import java.io.DataOutputStream;
import java.io.IOException;

public class TEModelServer extends TEModel {

    public TEModelServer(NBTBase tag, TEBlockModel te) {
        this.te = te;
        this.xCoord = te.xCoord;
        this.yCoord = te.yCoord;
        this.zCoord = te.zCoord;
        this.worldObj = MinecraftServer.getServer().getEntityWorld();
        NBTTagCompound c = (NBTTagCompound) tag;
        BlockModel.ModelCollideType[] types = BlockModel.ModelCollideType.values();
        this.type = types[c.getByte("ModelType")];
        this.modelName = c.getString("modelName");
        this.position = new Vector3f(c.getFloat("part_x"), c.getFloat("part_y"), c.getFloat("part_z"));
        this.rotation = new Vector3f(c.getFloat("part_rotx"), c.getFloat("part_roty"), c.getFloat("part_rotz"));
        this.scale = new Vector3f(c.getFloat("part_scalex"), c.getFloat("part_scaley"), c.getFloat("part_scalez"));

        if (this.scale.x <= 0) {
            this.scale.x = 1;
            this.scale.y = 1;
            this.scale.z = 1;
        }

        if (c.hasKey("aabb_minx")) {
            this.aabb.minX = c.getFloat("aabb_minx");
            this.aabb.minY = c.getFloat("aabb_miny");
            this.aabb.minZ = c.getFloat("aabb_minz");
            this.aabb.maxX = c.getFloat("aabb_maxx");
            this.aabb.maxY = c.getFloat("aabb_maxy");
            this.aabb.maxZ = c.getFloat("aabb_maxz");
            this.aabbCalculated = true;
        }
    }

    public TEModelServer(String model, TEBlockModel te, Vector3f pos, Vector3f rot, Vector3f scale, AxisAlignedBB aabb, BlockModel.ModelCollideType type) {
        this.type = type;
        this.te = te;

        if(aabb != null) {
            this.aabb.minX = aabb.minX;
            this.aabb.minY = aabb.minY;
            this.aabb.minZ = aabb.minZ;
            this.aabb.maxX = aabb.maxX;
            this.aabb.maxY = aabb.maxY;
            this.aabb.maxZ = aabb.maxZ;
            this.aabbCalculated = true;
        }

        this.position = pos;
        this.rotation = rot;
        this.scale = scale;
        this.modelName = model;
        this.worldObj = te.getWorldObj();
        this.xCoord = te.xCoord;
        this.yCoord = te.yCoord;
        this.zCoord = te.zCoord;

        te.placeHelpBlocks(worldObj, xCoord, yCoord, zCoord, xCoord + pos.x, yCoord + pos.y, zCoord + pos.z, this, TEBlockModel.HelpPlaceType.PLACE);
    }


    public NBTBase writeToNBT() {
        NBTTagCompound c = new NBTTagCompound();
        c.setByte("ModelType", type.toByte());
        c.setString("modelName", modelName);
        c.setFloat("part_x", position.x);
        c.setFloat("part_y", position.y);
        c.setFloat("part_z", position.z);
        c.setFloat("part_rotx", rotation.x);
        c.setFloat("part_roty", rotation.y);
        c.setFloat("part_rotz", rotation.z);
        c.setFloat("part_scalex", scale.x);
        c.setFloat("part_scaley", scale.y);
        c.setFloat("part_scalez", scale.z);

        if (aabbCalculated) {
            c.setFloat("aabb_minx", (float) aabb.minX);
            c.setFloat("aabb_miny", (float) aabb.minY);
            c.setFloat("aabb_minz", (float) aabb.minZ);
            c.setFloat("aabb_maxx", (float) aabb.maxX);
            c.setFloat("aabb_maxy", (float) aabb.maxY);
            c.setFloat("aabb_maxz", (float) aabb.maxZ);
        }

        return c;
    }

    public void writePacketData(DataOutputStream data) throws IOException {
        data.writeByte(type.toByte());
        data.writeUTF(modelName);
        data.writeFloat(position.x);
        data.writeFloat(position.y);
        data.writeFloat(position.z);
        data.writeFloat(rotation.x);
        data.writeFloat(rotation.y);
        data.writeFloat(rotation.z);
        data.writeFloat(scale.x);
        data.writeFloat(scale.y);
        data.writeFloat(scale.z);

        data.writeBoolean(aabbCalculated);
    }
}
