package ru.xlv.collideblocks.common.item;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ru.xlv.collideblocks.common.XlvsCollideBlocksMod;
import ru.xlv.collideblocks.common.tile.TileEntityBlockCollide;

public class ItemBlockCollide extends Item {
	public ItemBlockCollide() {
		this.setMaxStackSize(1);
	}

	public boolean onItemUse(ItemStack itemStack, EntityPlayer p, World w, int x, int y, int z, int side, float sx, float sy, float sz) {
		TileEntityBlockCollide tile = new TileEntityBlockCollide();
		if (itemStack.getTagCompound() != null) {
			tile.safeRead(itemStack.getTagCompound());
		}

		switch (side) {
			case 0:
				y--;
				break;
			case 1:
				y++;
				break;
			case 2:
				z--;
				break;
			case 3:
				z++;
				break;
			case 4:
				x--;
				break;
			case 5:
				x++;
		}

		w.setBlock(x, y, z, XlvsCollideBlocksMod.BLOCK_COLLIDE);
		w.setTileEntity(x, y, z, tile);
		return true;
	}

	@Override
	public void registerIcons(IIconRegister p_94581_1_) {

	}
}
