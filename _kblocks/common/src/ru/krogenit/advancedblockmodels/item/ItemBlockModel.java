package ru.krogenit.advancedblockmodels.item;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemBlockModel extends ItemBlock {
	
	public ItemBlockModel(Block block) {
		super(block);
	}

	@Override
	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int x, int y,
							 int z, int side, float hitX, float hitY, float hitZ) {
		return false;
	}

	@Override
	public String getItemStackDisplayName(ItemStack is) {
		String out;
		if (is.hasTagCompound() && is.getTagCompound().hasKey("Model")) {
			out = is.getTagCompound().getString("Name");
		} else
			out = "";

		return out;
	}
}
