package ru.krogenit.advancedblockmodels;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.GuiDisconnected;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraftforge.client.event.GuiOpenEvent;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;

public class GuiEvents {

	@SubscribeEvent
	public void event(GuiOpenEvent event) {
		if(event.gui instanceof GuiMainMenu || event.gui instanceof GuiDisconnected) {
			TEBlockModel.posToAdd.clear();
			TEBlockModel.partsToLoad.clear();
		}
	}
}
