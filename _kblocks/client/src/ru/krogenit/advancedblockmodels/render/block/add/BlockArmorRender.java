package ru.krogenit.advancedblockmodels.render.block.add;

import net.minecraft.util.ResourceLocation;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModelClient;
import ru.krogenit.client.renderer.IReloadableRenderer;
import ru.krogenit.client.renderer.ReloadableRendererManager;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

public class BlockArmorRender extends AbstractBlockModelRenderer implements IReloadableRenderer {

    private TextureDDS textureNormal;
    private TextureDDS textureDiffuse;
    private TextureDDS textureSpecular;
    private TextureDDS textureEmission;
    private TextureDDS textureGloss;

    private final boolean hasEmission;
    private final float emissionPower;
    private final String name;

    public BlockArmorRender(String modelName, String name, BlockModel.ModelRenderType type, boolean hasEmission, float emissionPower) {
        super(type, new Model(new ResourceLocation(modelName)));
        this.name = name;
        this.hasEmission = hasEmission;
        this.emissionPower = emissionPower;
        reload();
        ReloadableRendererManager.registerRenderer(this);
    }

    @Override
    public void reload() {
//        ResourceLocationStateful.Builder builder = ResourceLocationStateful.Builder.builder().withDomain("advancedblocks").temporary();
//        this.textureDiffuse = builder.of("textures/armor/" + name + "_d."+mc.gameSettings.textureSize+".dds");
//        this.textureNormal = builder.of("textures/armor/"+ name + "_n."+mc.gameSettings.textureSize+".dds");
//        this.textureSpecular = builder.of("textures/armor/" + name + "_s."+mc.gameSettings.textureSize+".dds");
//        if(hasEmission) this.textureEmission = builder.of("textures/armor/" + name + "_e."+mc.gameSettings.textureSize+".dds");
//        this.textureGloss = builder.of("textures/armor/" + name + "_g."+mc.gameSettings.textureSize+".dds");

        this.textureDiffuse = new TextureDDS(new ResourceLocation("advancedblocks", "textures/armor/" + name + "_d."+mc.gameSettings.textureSize+".dds"));
        this.textureNormal = new TextureDDS(new ResourceLocation("advancedblocks", "textures/armor/" + name + "_n."+mc.gameSettings.textureSize+".dds"));
        this.textureSpecular = new TextureDDS(new ResourceLocation("advancedblocks", "textures/armor/" + name + "_s."+mc.gameSettings.textureSize+".dds"));
        if(hasEmission) this.textureEmission = new TextureDDS(new ResourceLocation("advancedblocks", "textures/armor/" + name + "_e."+mc.gameSettings.textureSize+".dds"));
        this.textureGloss = new TextureDDS(new ResourceLocation("advancedblocks", "textures/armor/" + name + "_g."+mc.gameSettings.textureSize+".dds"));
    }

    private void setShaderState(IPBR shader) {
        TextureLoaderDDS.bindTexture(textureDiffuse);
        TextureLoaderDDS.bindNormalMap(textureNormal, shader);
        TextureLoaderDDS.bindSpecularMap(textureSpecular, shader);
        TextureLoaderDDS.bindGlossMap(textureGloss, shader);

        if(hasEmission) {
            TextureLoaderDDS.bindEmissionMap(textureEmission, shader, emissionPower);
        }
    }

    private void endShaderState(IPBR shader) {
        if(hasEmission) shader.setEmissionMapping(false);
        shader.setSpecularMapping(false);
        shader.setNormalMapping(false);
        shader.setGlossMapping(false);
        KrogenitShaders.finishCurrentShader();
        TextureLoaderDDS.unbind();
    }

    @Override
    public void render(TEModelClient part) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        setShaderState(shader);
        model.render(shader);
        endShaderState(shader);
    }

    @Override
    public void renderPost(TEModelClient part) {

    }
}
