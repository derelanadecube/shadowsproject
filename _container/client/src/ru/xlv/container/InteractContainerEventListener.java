package ru.xlv.container;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import ru.xlv.container.common.entity.EntityContainer;
import ru.xlv.container.gui.GuiContainer;
import ru.xlv.container.network.PacketContainerInteract;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.event.PlayerInteractWorldObjectEvent;

public class InteractContainerEventListener {

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerInteractWorldObjectEvent event) {
        Entity entity1 = event.getEntity();
        if(entity1 instanceof EntityContainer) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketContainerInteract(entity1.getEntityId(), true));
            Minecraft.getMinecraft().displayGuiScreen(new GuiContainer((EntityContainer) entity1));
        }
    }
}
