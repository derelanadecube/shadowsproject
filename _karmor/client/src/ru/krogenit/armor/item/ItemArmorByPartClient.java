package ru.krogenit.armor.item;

import net.minecraft.client.renderer.texture.IIconRegister;
import ru.krogenit.armor.CoreArmorClient;
import ru.xlv.core.common.inventory.MatrixInventory;

public class ItemArmorByPartClient extends ItemArmorByPart {

    public ItemArmorByPartClient(String name, MatrixInventory.SlotType slotType, SetFamily setFamily) {
        super(name, slotType, setFamily);
        setCreativeTab(CoreArmorClient.armorTab);
    }

    public ItemArmorByPartClient(String name, MatrixInventory.SlotType slotType) {
        super(name, slotType);
        setCreativeTab(CoreArmorClient.armorTab);
    }

    @Override
    public void registerIcons(IIconRegister p_94581_1_) {

    }
}
