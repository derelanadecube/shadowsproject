package ru.krogenit.armor.client;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import ru.xlv.core.common.inventory.MatrixInventory;

public interface IArmorRenderer {
	void render(ModelBiped modelBipedMain, MatrixInventory.SlotType type, EntityLivingBase p, ItemStack itemStack);
	
	void renderBody();
	void renderBodyInv(ItemRenderType type);
	void renderLegs();
	void renderBoots();
	void renderLegsInv(ItemRenderType type);
	void renderBootsInv(ItemRenderType type);
	void renderHead();
	void renderHeadInv(ItemRenderType type);
	void renderLeftHand();
	void renderRightHand();
	void renderLeftLeg();
	void renderRightLeg();
	void renderLeftBoots();
	void renderRightBoots();
	void renderLeftGlove();
	void renderRightGlove();
	void renderBracersInv(ItemRenderType type);
	
	void renderLeftBracerFirstPerson();
	void renderRightBracerFirstPerson();
	void renderLeftBodyArmFirstPerson();
	void renderRightBodyArmFirstPerson();
}
