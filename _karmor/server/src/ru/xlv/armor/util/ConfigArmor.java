package ru.xlv.armor.util;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.ToString;
import ru.xlv.core.common.item.quality.EnumItemQuality;
import ru.xlv.core.common.item.rarity.EnumItemRarity;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;

@ToString
@Getter
public class ConfigArmor implements IConfigGson {

    private static final String CONFIG_PATH_FORMAT = "config/armor/%s_cfg.json";

    @Configurable(comment = "ballisticProtection")
    private double ballisticProtection = 0D;
    @Configurable(comment = "cutProtection")
    private double cutProtection = 0D;
    @Configurable(comment = "energyProtection")
    private double energyProtection = 0D;
    @Configurable(comment = "thermalProtection")
    private double thermalProtection = 0D;
    @Configurable(comment = "fireProtection")
    private double fireProtection = 0D;
    @Configurable(comment = "electProtection")
    private double electProtection = 0D;
    @Configurable(comment = "toxicProtection")
    private double toxicProtection = 0D;
    @Configurable(comment = "radiationProtection")
    private double radiationProtection = 0D;
    @Configurable(comment = "explosionProtection")
    private double explosionProtection = 0D;
    @Configurable(comment = "weight")
    private double weight = 0D;

    @Configurable(comment = "Прочность брони.")
    private int hardness = 0;

    @Configurable(comment = "Название предмета.")
    private String displayName = "";
    @Configurable(comment = "Второе название предмета.")
    private String secondName = "";

    @Configurable(comment = "Ширина предмета в клетках. Минимальное значение: 1")
    private int matrixWidth = 1;
    @Configurable(comment = "Высота предмета в клетках. Минимальное значение: 1")
    private int matrixHeight = 1;

    @Configurable(comment = "Качество предмета. Возможные варианты: GARBAGE - Мусорный,WORN_OUT - Изношенное,LOW_QUALITY - Низкокачественное,FMCG - Ширпотреб,BASIC - Базовое качество, HIGH - Повышенное качество, DISTINCTIVE - Отличительное качество, HIGHEST - Наивысшее качество, PERFECT - Идеальное качество, CUSTOMIZED - Заказное качество")
    private EnumItemQuality itemQuality = EnumItemQuality.BASIC;
    @Configurable(comment = "Редкость предмета. Возможные варианты: SLAG - Шлаковый, COMMON - Распространенный, UNCOMMON - Необычный, RARE - Редкий, EPIC - Эпический, SPECIAL - Особый, LEGENDARY - Легендарный")
    private EnumItemRarity itemRarity = EnumItemRarity.COMMON;

    @Configurable(comment = "Описание предмета")
    private String description = "";

    @Configurable(comment = "Лозунг предмета.")
    private String slogan = "";

    @Configurable(comment = "Теги предмета через запятую. Возможные варианты: PISTOL, SMG, AUTOMATIC, RIFLE, CARBINE, SNIPER, SHOTGUN, MACHINE_GUN, LASER_GUN, ENERGY_GUN, UNREGISTERED_ITEM, REGISTERED_ITEM, UNINSURED_ITEM, INSURED_ITEM, PERSONAL_CANDIDATE, PERSONAL")
    private String tags = "";

    @Getter(value = AccessLevel.NONE)
    protected final String itemName;

    public ConfigArmor(String itemName) {
        this.itemName = itemName;
    }

    @Override
    public File getConfigFile() {
        return new File(String.format(CONFIG_PATH_FORMAT, itemName));
    }
}
