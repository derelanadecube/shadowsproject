package ru.xlv.armor.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import ru.xlv.armor.CoreArmorCommon;
import ru.xlv.armor.network.PacketArmorSync;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.event.ServerPlayerLoginEvent;

public class EventListener {

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(ServerPlayerLoginEvent event) {
        CoreArmorCommon.registeredArmorList.forEach(itemArmor -> XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(event.getServerPlayer().getEntityPlayer(), new PacketArmorSync(itemArmor)));
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(TickEvent.PlayerTickEvent event) {
        EntityPlayer player = event.player;
        if(player != null) {
            ItemStack stackInSlot = player.inventory.getStackInSlot(MatrixInventory.SlotType.HEAD.getAssociatedSlotIndex());
            if(stackInSlot != null && stackInSlot.getItem() instanceof ItemArmor) {
                player.addPotionEffect(new PotionEffect(Potion.nightVision.id, Integer.MAX_VALUE, 1));
            } else {
                player.removePotionEffect(Potion.nightVision.id);
            }
        }
    }
}
