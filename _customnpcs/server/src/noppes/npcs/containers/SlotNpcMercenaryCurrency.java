package noppes.npcs.containers;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import noppes.npcs.roles.RoleFollower;

import java.util.Iterator;

class SlotNpcMercenaryCurrency extends Slot
{
    RoleFollower role;

    public SlotNpcMercenaryCurrency(RoleFollower role, IInventory inv, int i, int j, int k)
    {
        super(inv, i, j, k);
        this.role = role;
    }

    /**
     * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in the case
     * of armor slots)
     */
    public int getSlotStackLimit()
    {
        return 64;
    }

    /**
     * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
     */
    public boolean isItemValid(ItemStack itemstack)
    {
        Item item = itemstack.getItem();
        Iterator var3 = this.role.inventory.items.values().iterator();
        ItemStack is;

        do
        {
            do
            {
                if (!var3.hasNext())
                {
                    return false;
                }

                is = (ItemStack)var3.next();
            }
            while (item != is.getItem());
        }
        while (itemstack.getHasSubtypes() && itemstack.getItemDamage() != is.getItemDamage());

        return true;
    }
}
