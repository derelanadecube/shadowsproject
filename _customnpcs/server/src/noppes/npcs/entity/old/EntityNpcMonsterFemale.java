package noppes.npcs.entity.old;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import noppes.npcs.ModelData;
import noppes.npcs.constants.EnumAnimation;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityNPCInterface;

public class EntityNpcMonsterFemale extends EntityNPCInterface
{
    public EntityNpcMonsterFemale(World world)
    {
        super(world);
        this.scaleX = this.scaleY = this.scaleZ = 0.9075F;
        this.display.texture = "customnpcs:textures/entity/monsterfemale/ZombieStephanie.png";
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        this.isDead = true;

        if (!this.worldObj.isRemote)
        {
            NBTTagCompound compound = new NBTTagCompound();
            this.writeToNBT(compound);
            EntityCustomNpc npc = new EntityCustomNpc(this.worldObj);
            npc.readFromNBT(compound);
            ModelData data = npc.modelData;
            data.breasts = 2;
            data.head.setScale(0.95F, 0.95F);
            data.legs.setScale(0.92F, 0.92F);
            data.arms.setScale(0.8F, 0.92F);
            data.body.setScale(0.92F, 0.92F);
            npc.ai.animationType = EnumAnimation.HUG;
            this.worldObj.spawnEntityInWorld(npc);
        }

        super.onUpdate();
    }
}
