package noppes.npcs.config;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.List;
import java.util.*;

public class GlyphCache
{
    private static final int TEXTURE_WIDTH = 256;
    private static final int TEXTURE_HEIGHT = 256;
    private static final int STRING_WIDTH = 256;
    private static final int STRING_HEIGHT = 64;
    private static final int GLYPH_BORDER = 1;
    private static Color BACK_COLOR = new Color(255, 255, 255, 0);
    private int fontSize = 18;
    private boolean antiAliasEnabled = false;
    private BufferedImage stringImage;
    private Graphics2D stringGraphics;
    private BufferedImage glyphCacheImage = new BufferedImage(256, 256, 2);
    private Graphics2D glyphCacheGraphics;
    private FontRenderContext fontRenderContext;
    private int[] imageData;
    private IntBuffer imageBuffer;
    private IntBuffer singleIntBuffer;
    private List<Font> allFonts;
    protected List<Font> usedFonts;
    private int textureName;
    private LinkedHashMap<Font, Integer> fontCache;
    private LinkedHashMap<Long, GlyphCache.Entry> glyphCache;
    private int cachePosX;
    private int cachePosY;
    private int cacheLineHeight;

    public GlyphCache()
    {
        this.glyphCacheGraphics = this.glyphCacheImage.createGraphics();
        this.fontRenderContext = this.glyphCacheGraphics.getFontRenderContext();
        this.imageData = new int[65536];
        this.imageBuffer = ByteBuffer.allocateDirect(262144).order(ByteOrder.BIG_ENDIAN).asIntBuffer();
        this.singleIntBuffer = GLAllocation.createDirectIntBuffer(1);
        this.allFonts = Arrays.asList(GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts());
        this.usedFonts = new ArrayList();
        this.fontCache = new LinkedHashMap();
        this.glyphCache = new LinkedHashMap();
        this.cachePosX = 1;
        this.cachePosY = 1;
        this.cacheLineHeight = 0;
        this.glyphCacheGraphics.setBackground(BACK_COLOR);
        this.glyphCacheGraphics.setComposite(AlphaComposite.Src);
        this.allocateGlyphCacheTexture();
        this.allocateStringImage(256, 64);
        GraphicsEnvironment.getLocalGraphicsEnvironment().preferLocaleFonts();
        this.usedFonts.add(new Font("SansSerif", 0, 72));
    }

    void setDefaultFont(String name, int size, boolean antiAlias)
    {
        this.usedFonts.clear();
        this.usedFonts.add(new Font(name, 0, 72));
        this.fontSize = size;
        this.antiAliasEnabled = antiAlias;
        this.setRenderingHints();
    }

    void setCustomFont(ResourceLocation location, int size, boolean antiAlias) throws Exception
    {
        InputStream stream = Minecraft.getMinecraft().getResourceManager().getResource(location).getInputStream();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Font font = Font.createFont(0, stream);
        ge.registerFont(font);
        font = font.deriveFont(0, 72.0F);
        this.usedFonts.clear();
        this.usedFonts.add(font);
        this.fontSize = size;
        this.antiAliasEnabled = antiAlias;
        this.setRenderingHints();
    }

    int fontHeight(String s)
    {
        Font font = this.lookupFont(s.toCharArray(), 0, s.length(), 0);
        return (int)font.getLineMetrics(s, this.fontRenderContext).getHeight();
    }

    GlyphVector layoutGlyphVector(Font font, char[] text, int start, int limit, int layoutFlags)
    {
        if (!this.fontCache.containsKey(font))
        {
            this.fontCache.put(font, Integer.valueOf(this.fontCache.size()));
        }

        return font.layoutGlyphVector(this.fontRenderContext, text, start, limit, layoutFlags);
    }

    Font lookupFont(char[] text, int start, int limit, int style)
    {
        Iterator iterator = this.usedFonts.iterator();
        Font fa;

        do
        {
            if (!iterator.hasNext())
            {
                fa = new Font("Arial Unicode MS", 0, this.fontSize);

                if (fa.canDisplayUpTo(text, start, limit) != start)
                {
                    this.usedFonts.add(fa);
                    return fa;
                }

                iterator = this.allFonts.iterator();
                Font font;

                do
                {
                    if (!iterator.hasNext())
                    {
                        font = (Font)this.usedFonts.get(0);
                        return font.deriveFont(style, (float)this.fontSize);
                    }

                    font = (Font)iterator.next();
                }
                while (font.canDisplayUpTo(text, start, limit) == start);

                this.usedFonts.add(font);
                return font.deriveFont(style, (float)this.fontSize);
            }

            fa = (Font)iterator.next();
        }
        while (fa.canDisplayUpTo(text, start, limit) == start);

        return fa.deriveFont(style, (float)this.fontSize);
    }

    GlyphCache.Entry lookupGlyph(Font font, int glyphCode)
    {
        long fontKey = (long)((Integer)this.fontCache.get(font)).intValue() << 32;
        return (GlyphCache.Entry)this.glyphCache.get(Long.valueOf(fontKey | (long)glyphCode));
    }

    void cacheGlyphs(Font font, char[] text, int start, int limit, int layoutFlags)
    {
        GlyphVector vector = this.layoutGlyphVector(font, text, start, limit, layoutFlags);
        Rectangle vectorBounds = null;
        long fontKey = (long)((Integer)this.fontCache.get(font)).intValue() << 32;
        int numGlyphs = vector.getNumGlyphs();
        Rectangle dirty = null;
        boolean vectorRendered = false;

        for (int index = 0; index < numGlyphs; ++index)
        {
            int glyphCode = vector.getGlyphCode(index);

            if (!this.glyphCache.containsKey(Long.valueOf(fontKey | (long)glyphCode)))
            {
                if (!vectorRendered)
                {
                    vectorRendered = true;
                    int rect;

                    for (rect = 0; rect < numGlyphs; ++rect)
                    {
                        Point2D entry = vector.getGlyphPosition(rect);
                        entry.setLocation(entry.getX() + (double)(2 * rect), entry.getY());
                        vector.setGlyphPosition(rect, entry);
                    }

                    vectorBounds = vector.getPixelBounds(this.fontRenderContext, 0.0F, 0.0F);

                    if (this.stringImage == null || vectorBounds.width > this.stringImage.getWidth() || vectorBounds.height > this.stringImage.getHeight())
                    {
                        rect = Math.max(vectorBounds.width, this.stringImage.getWidth());
                        int var18 = Math.max(vectorBounds.height, this.stringImage.getHeight());
                        this.allocateStringImage(rect, var18);
                    }

                    this.stringGraphics.clearRect(0, 0, vectorBounds.width, vectorBounds.height);
                    this.stringGraphics.drawGlyphVector(vector, (float)(-vectorBounds.x), (float)(-vectorBounds.y));
                }

                Rectangle var17 = vector.getGlyphPixelBounds(index, (FontRenderContext)null, (float)(-vectorBounds.x), (float)(-vectorBounds.y));

                if (this.cachePosX + var17.width + 1 > 256)
                {
                    this.cachePosX = 1;
                    this.cachePosY += this.cacheLineHeight + 1;
                    this.cacheLineHeight = 0;
                }

                if (this.cachePosY + var17.height + 1 > 256)
                {
                    this.updateTexture(dirty);
                    dirty = null;
                    this.allocateGlyphCacheTexture();
                    this.cachePosY = this.cachePosX = 1;
                    this.cacheLineHeight = 0;
                }

                if (var17.height > this.cacheLineHeight)
                {
                    this.cacheLineHeight = var17.height;
                }

                this.glyphCacheGraphics.drawImage(this.stringImage, this.cachePosX, this.cachePosY, this.cachePosX + var17.width, this.cachePosY + var17.height, var17.x, var17.y, var17.x + var17.width, var17.y + var17.height, (ImageObserver)null);
                var17.setLocation(this.cachePosX, this.cachePosY);
                GlyphCache.Entry var19 = new GlyphCache.Entry();
                var19.textureName = this.textureName;
                var19.width = var17.width;
                var19.height = var17.height;
                var19.u1 = (float)var17.x / 256.0F;
                var19.v1 = (float)var17.y / 256.0F;
                var19.u2 = (float)(var17.x + var17.width) / 256.0F;
                var19.v2 = (float)(var17.y + var17.height) / 256.0F;
                this.glyphCache.put(Long.valueOf(fontKey | (long)glyphCode), var19);

                if (dirty == null)
                {
                    dirty = new Rectangle(this.cachePosX, this.cachePosY, var17.width, var17.height);
                }
                else
                {
                    dirty.add(var17);
                }

                this.cachePosX += var17.width + 1;
            }
        }

        this.updateTexture(dirty);
    }

    private void updateTexture(Rectangle dirty)
    {
        if (dirty != null)
        {
            this.updateImageBuffer(dirty.x, dirty.y, dirty.width, dirty.height);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.textureName);
            GL11.glTexSubImage2D(GL11.GL_TEXTURE_2D, 0, dirty.x, dirty.y, dirty.width, dirty.height, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, this.imageBuffer);
        }
    }

    private void allocateStringImage(int width, int height)
    {
        this.stringImage = new BufferedImage(width, height, 2);
        this.stringGraphics = this.stringImage.createGraphics();
        this.setRenderingHints();
        this.stringGraphics.setBackground(BACK_COLOR);
        this.stringGraphics.setPaint(Color.WHITE);
    }

    private void setRenderingHints()
    {
        this.stringGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, this.antiAliasEnabled ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF);
        this.stringGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, this.antiAliasEnabled ? RenderingHints.VALUE_TEXT_ANTIALIAS_ON : RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        this.stringGraphics.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
    }

    private void allocateGlyphCacheTexture()
    {
        this.glyphCacheGraphics.clearRect(0, 0, 256, 256);
        this.singleIntBuffer.clear();
        GL11.glGenTextures(this.singleIntBuffer);
        this.textureName = this.singleIntBuffer.get(0);
        this.updateImageBuffer(0, 0, 256, 256);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.textureName);
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_ALPHA8, 256, 256, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, this.imageBuffer);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
    }

    private void updateImageBuffer(int x, int y, int width, int height)
    {
        this.glyphCacheImage.getRGB(x, y, width, height, this.imageData, 0, width);

        for (int i = 0; i < width * height; ++i)
        {
            int color = this.imageData[i];
            this.imageData[i] = color << 8 | color >>> 24;
        }

        this.imageBuffer.clear();
        this.imageBuffer.put(this.imageData);
        this.imageBuffer.flip();
    }

    static class Entry
    {
        public int textureName;
        public int width;
        public int height;
        public float u1;
        public float v1;
        public float u2;
        public float v2;
    }
}
