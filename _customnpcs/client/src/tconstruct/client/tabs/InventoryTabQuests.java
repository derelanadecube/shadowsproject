package tconstruct.client.tabs;

import net.minecraft.client.Minecraft;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import noppes.npcs.CustomItems;
import noppes.npcs.client.gui.player.GuiQuestLog;

public class InventoryTabQuests extends AbstractTab
{
    public InventoryTabQuests()
    {
        super(0, 0, 0, new ItemStack(CustomItems.letter));

        if (CustomItems.letter == null)
        {
            this.renderStack = new ItemStack(Items.book);
        }
    }

    public void onTabClicked()
    {
        Thread t = new Thread()
        {
            public void run()
            {
                try
                {
                    Thread.sleep(100L);
                }
                catch (InterruptedException var2)
                {
                    var2.printStackTrace();
                }

                Minecraft mc = Minecraft.getMinecraft();
                mc.displayGuiScreen(new GuiQuestLog(mc.thePlayer));
            }
        };
        t.start();
    }

    public boolean shouldAddToList()
    {
        return true;
    }
}
