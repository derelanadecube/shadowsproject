package noppes.npcs.client.renderer.blocks;

import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import noppes.npcs.CustomItems;
import noppes.npcs.blocks.BlockStool;
import noppes.npcs.blocks.tiles.TileColorable;
import noppes.npcs.client.model.blocks.ModelStool;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class BlockStoolRenderer extends BlockRendererInterface
{
    private final ModelStool model = new ModelStool();

    public BlockStoolRenderer()
    {
        ((BlockStool)CustomItems.stool).renderId = RenderingRegistry.getNextAvailableRenderId();
        RenderingRegistry.registerBlockHandler(this);
    }

    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float timeTick)
    {
        TileColorable tile = (TileColorable) tileEntity;
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.68F, (float) z + 0.5F);
        GL11.glScalef(1.2F, 1.1F, 1.2F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        this.setWoodTexture(tileEntity.getBlockMetadata());
        this.model.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        GL11.glPopMatrix();
    }

    public void renderTileEntityAtPost(TileEntity tileEntity, double x, double y, double z, float timeTick) {

    }

    public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer)
    {
        GL11.glPushMatrix();
        GL11.glTranslatef(0.0F, 1.0F, 0.0F);
        GL11.glScalef(1.2F, 1.1F, 1.2F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
        this.setWoodTexture(metadata);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        this.model.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        GL11.glPopMatrix();
    }

    public int getRenderId()
    {
        return CustomItems.stool.getRenderType();
    }
}
