package noppes.npcs.client.gui.model;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import noppes.npcs.ModelData;
import noppes.npcs.client.controllers.Preset;
import noppes.npcs.client.controllers.PresetController;
import noppes.npcs.client.gui.util.GuiNPCInterface;
import noppes.npcs.client.gui.util.GuiNPCStringSlot;
import noppes.npcs.client.gui.util.GuiNpcButton;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.util.EntityUtil;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;

public class GuiPresetSelection extends GuiNPCInterface
{
    private GuiNPCStringSlot slot;
    private GuiCreationScreen parent;
    private NBTTagCompound prevData;
    private ModelData playerdata;
    private EntityCustomNpc npc;

    public GuiPresetSelection(GuiCreationScreen parent, ModelData playerdata)
    {
        this.parent = parent;
        this.playerdata = playerdata;
        this.prevData = playerdata.writeToNBT();
        this.drawDefaultBackground = false;
        this.npc = new EntityCustomNpc(Minecraft.getMinecraft().theWorld);
        this.npc.modelData = playerdata.copy();
        PresetController.instance.load();
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        Vector list = new Vector();
        Iterator var2 = PresetController.instance.presets.values().iterator();

        while (var2.hasNext())
        {
            Preset preset = (Preset)var2.next();
            list.add(preset.name);
        }

        Collections.sort(list, String.CASE_INSENSITIVE_ORDER);
        this.slot = new GuiNPCStringSlot(list, this, false, 18);
        this.slot.registerScrollButtons(4, 5);
        this.buttonList.add(new GuiNpcButton(2, this.width / 2 - 100, this.height - 44, 98, 20, "Back"));
        this.buttonList.add(new GuiNpcButton(3, this.width / 2 + 2, this.height - 44, 98, 20, "Load"));
        this.buttonList.add(new GuiNpcButton(4, this.width / 2 - 49, this.height - 22, 98, 20, "Remove"));
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        Object entity = this.npc.modelData.getEntity(this.npc);

        if (entity == null)
        {
            entity = this.npc;
        }
        else
        {
            EntityUtil.Copy(this.npc, (EntityLivingBase)entity);
        }

        int l = this.width / 2 - 180;
        int i1 = this.height / 2 - 90;
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float)(l + 33), (float)(i1 + 131), 50.0F);
        GL11.glScalef(-50.0F, 50.0F, 50.0F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        float f2 = ((EntityLivingBase)entity).renderYawOffset;
        float f3 = ((EntityLivingBase)entity).rotationYaw;
        float f4 = ((EntityLivingBase)entity).rotationPitch;
        float f7 = ((EntityLivingBase)entity).rotationYawHead;
        float f5 = (float)(l + 33) - (float) mouseX;
        float f6 = (float)(i1 + 131 - 50) - (float) mouseY;
        GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();
        GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-((float)Math.atan((double)(f6 / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
        ((EntityLivingBase)entity).renderYawOffset = (float)Math.atan((double)(f5 / 40.0F)) * 20.0F;
        ((EntityLivingBase)entity).rotationYaw = (float)Math.atan((double)(f5 / 40.0F)) * 40.0F;
        ((EntityLivingBase)entity).rotationPitch = -((float)Math.atan((double)(f6 / 40.0F))) * 20.0F;
        ((EntityLivingBase)entity).rotationYawHead = ((EntityLivingBase)entity).rotationYaw;
        GL11.glTranslatef(0.0F, ((EntityLivingBase)entity).yOffset, 0.0F);
        RenderManager.instance.playerViewY = 180.0F;
        RenderManager.instance.renderEntityWithPosYaw((Entity)entity, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
        ((EntityLivingBase)entity).renderYawOffset = f2;
        ((EntityLivingBase)entity).rotationYaw = f3;
        ((EntityLivingBase)entity).rotationPitch = f4;
        ((EntityLivingBase)entity).rotationYawHead = f7;
        GL11.glPopMatrix();
        RenderHelper.disableStandardItemLighting();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        this.slot.drawScreen(mouseX, mouseY, partialTick);
        super.drawScreen(mouseX, mouseY, partialTick);
    }

    public void elementClicked()
    {
        Preset preset = PresetController.instance.getPreset(this.slot.selected);
        this.npc.modelData.readFromNBT(preset.data.writeToNBT());
    }

    public void doubleClicked()
    {
        this.playerdata.readFromNBT(this.npc.modelData.writeToNBT());
        this.close();
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    public void keyTyped(char character, int key)
    {
        if (key == 1)
        {
            this.close();
        }
    }

    public void close()
    {
        this.mc.displayGuiScreen(this.parent);
    }

    public FontRenderer getFontRenderer()
    {
        return this.fontRendererObj;
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        GuiNpcButton guibutton = (GuiNpcButton) guiButton;

        if (guibutton.id == 2)
        {
            this.close();
        }

        if (guibutton.id == 3)
        {
            this.playerdata.readFromNBT(this.npc.modelData.writeToNBT());
            this.close();
        }

        if (guibutton.id == 4)
        {
            PresetController.instance.removePreset(this.slot.selected);
            Vector list = new Vector();
            Iterator var4 = PresetController.instance.presets.values().iterator();

            while (var4.hasNext())
            {
                Preset preset = (Preset)var4.next();
                list.add(preset.name);
            }

            Collections.sort(list, String.CASE_INSENSITIVE_ORDER);
            this.slot.setList(list);
            this.slot.selected = "";
        }
    }

    public void save() {}
}
