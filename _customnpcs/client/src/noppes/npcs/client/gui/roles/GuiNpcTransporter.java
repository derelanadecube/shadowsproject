package noppes.npcs.client.gui.roles;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.nbt.NBTTagCompound;
import noppes.npcs.client.Client;
import noppes.npcs.client.gui.util.*;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.controllers.TransportLocation;
import noppes.npcs.entity.EntityNPCInterface;

import java.util.HashMap;
import java.util.Vector;

public class GuiNpcTransporter extends GuiNPCInterface2 implements IScrollData, IGuiData
{
    private GuiCustomScroll scroll;
    public TransportLocation location = new TransportLocation();
    private HashMap<String, Integer> data = new HashMap();

    public GuiNpcTransporter(EntityNPCInterface npc)
    {
        super(npc);
        Client.sendData(EnumPacketServer.TransportCategoriesGet, new Object[0]);
        Client.sendData(EnumPacketServer.TransportGetLocation, new Object[0]);
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        Vector list = new Vector();
        list.addAll(this.data.keySet());

        if (this.scroll == null)
        {
            this.scroll = new GuiCustomScroll(this, 0);
            this.scroll.setSize(143, 208);
        }

        this.scroll.guiLeft = this.guiLeft + 214;
        this.scroll.guiTop = this.guiTop + 4;
        this.addScroll(this.scroll);
        this.addLabel(new GuiNpcLabel(0, "gui.name", this.guiLeft + 4, this.height + 8));
        this.addTextField(new GuiNpcTextField(0, this, this.fontRendererObj, this.guiLeft + 60, this.guiTop + 3, 140, 20, this.location.name));
        this.addButton(new GuiNpcButton(0, this.guiLeft + 4, this.guiTop + 31, new String[] {"transporter.discovered", "transporter.start", "transporter.interaction"}, this.location.type));
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        GuiNpcButton button = (GuiNpcButton) guiButton;

        if (button.id == 0)
        {
            this.location.type = button.getValue();
        }
    }

    public void save()
    {
        if (this.scroll.hasSelected())
        {
            String name = this.getTextField(0).getText();

            if (!name.isEmpty())
            {
                this.location.name = name;
            }

            this.location.posX = this.player.posX;
            this.location.posY = this.player.posY;
            this.location.posZ = this.player.posZ;
            this.location.dimension = this.player.dimension;
            int cat = ((Integer)this.data.get(this.scroll.getSelected())).intValue();
            Client.sendData(EnumPacketServer.TransportSave, new Object[] {Integer.valueOf(cat), this.location.writeNBT()});
        }
    }

    public void setData(Vector<String> list, HashMap<String, Integer> data)
    {
        this.data = data;
        this.scroll.setList(list);
    }

    public void setSelected(String selected)
    {
        this.scroll.setSelected(selected);
    }

    public void setGuiData(NBTTagCompound compound)
    {
        TransportLocation loc = new TransportLocation();
        loc.readNBT(compound);
        this.location = loc;
        this.initGui();
    }
}
