package noppes.npcs.items;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.world.World;
import noppes.npcs.CustomItems;
import noppes.npcs.CustomNpcs;
import noppes.npcs.CustomNpcsPermissions;
import noppes.npcs.NoppesUtilServer;
import noppes.npcs.constants.EnumGuiType;
import noppes.npcs.entity.EntityCustomNpc;

public class ItemNpcWand extends Item
{
    public ItemNpcWand()
    {
        this.maxStackSize = 1;
        this.setCreativeTab(CustomItems.tab);
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (!par2World.isRemote)
        {
            return par1ItemStack;
        }
        else
        {
            CustomNpcs.proxy.openGui(0, 0, 0, EnumGuiType.NpcRemote, par3EntityPlayer);
            return par1ItemStack;
        }
    }

    /**
     * Callback for item usage. If the item does something special on right clicking, he will have one of those. Return
     * True if something happen and false if it don't. This is for ITEMS, not BLOCKS
     */
    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer player, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
        if (par3World.isRemote)
        {
            return true;
        }
        else
        {
            if (CustomNpcs.OpsOnly && !MinecraftServer.getServer().getConfigurationManager().func_152596_g(player.getGameProfile()))
            {
                player.addChatMessage(new ChatComponentTranslation("availability.permission", new Object[0]));
            }
            else
            {
                CustomNpcsPermissions var10000 = CustomNpcsPermissions.Instance;

                if (CustomNpcsPermissions.hasPermission(player, CustomNpcsPermissions.NPC_CREATE))
                {
                    EntityCustomNpc npc = new EntityCustomNpc(par3World);
                    npc.ai.startPos = new int[] {par4, par5, par6};
                    npc.setLocationAndAngles((double)((float)par4 + 0.5F), npc.getStartYPos(), (double)((float)par6 + 0.5F), player.rotationYaw, player.rotationPitch);
                    par3World.spawnEntityInWorld(npc);
                    npc.setHealth(npc.getMaxHealth());
                    NoppesUtilServer.sendOpenGui(player, EnumGuiType.MainMenuDisplay, npc);
                }
                else
                {
                    player.addChatMessage(new ChatComponentTranslation("availability.permission", new Object[0]));
                }
            }

            return true;
        }
    }

    public int getColorFromItemStack(ItemStack par1ItemStack, int par2)
    {
        return 9127187;
    }

    public boolean requiresMultipleRenderPasses()
    {
        return true;
    }

    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister par1IconRegister)
    {
        this.itemIcon = Items.iron_hoe.getIconFromDamage(0);
    }

    /**
     * Sets the unlocalized name of this item to the string passed as the parameter, prefixed by "item."
     */
    public Item setUnlocalizedName(String name)
    {
        GameRegistry.registerItem(this, name);
        return super.setUnlocalizedName(name);
    }
}
