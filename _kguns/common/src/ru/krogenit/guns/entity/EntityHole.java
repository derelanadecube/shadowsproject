package ru.krogenit.guns.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityHole extends Entity {

    private int maxLifeTicks = 400;
    public float colorAlpha = 1f;

    public int placeType;

    public EntityHole(World par1World, MovingObjectPosition obj) {
        super(par1World);
        this.placeType = obj.sideHit;
        this.setPosition(obj.hitVec.xCoord, obj.hitVec.yCoord, obj.hitVec.zCoord);
    }

    public EntityHole(World par1World) {
        super(par1World);
    }

    @Override
    protected void entityInit() {

    }

    public void onUpdate() {
        super.onUpdate();
        maxLifeTicks--;

        if (maxLifeTicks < 100) {
            colorAlpha = maxLifeTicks / 100f;
            if (maxLifeTicks < 0)
                setDead();
        }

    }

    @Override
    protected void readEntityFromNBT(NBTTagCompound nbttagcompound) {

    }

    @Override
    protected void writeEntityToNBT(NBTTagCompound nbttagcompound) {

    }

    protected float getGravityVelocity() {
        return 0.0F;
    }

    @SideOnly(Side.CLIENT)
    public float getShadowSize() {
        return 0.0F;
    }

    @SideOnly(Side.CLIENT)
    public boolean isInRangeToRenderDist(double par1) {
        double d1 = this.boundingBox.getAverageEdgeLength() * 4.0D;
        d1 *= 64.0D;
        return par1 < d1 * d1;
    }
}
