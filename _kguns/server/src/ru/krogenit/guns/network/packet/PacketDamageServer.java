package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.guns.item.ItemGun;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketDamageServer implements IPacketOut, IPacketInOnServer {

    private int entityId;
    private float damage;

    @Override
    public void read(EntityPlayer p, ByteBufInputStream data) throws IOException {
        damage = data.readFloat();
        entityId = data.readInt();
        handleMessageOnServerSide(p);
    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeFloat(damage);
        data.writeInt(entityId);
    }

    public void handleMessageOnServerSide(EntityPlayer p) {
        World w = p.worldObj;
        Entity e = w.getEntityByID(entityId);
        if(e != null) {
            ItemStack itemStack = p.getCurrentEquippedItem();
            if(itemStack != null) {
                Item item = itemStack.getItem();
                if(item instanceof ItemGun) {
                    ItemGun itemGun = (ItemGun) item;
                    if(itemStack.getItemDamage() < itemStack.getMaxDamage() && canHitEntity(p, e)) {
                        if (e.attackEntityFrom(DamageSource.causePlayerDamage(p), damage)) {
                            e.hurtResistantTime = 0;
                        }
                    }
                }
            }
        }
    }

    public static int MAX_DAMAGE_DISTANCE = 2048;
    public static float MAX_ANGLE = 15f;
    public static float ANGLE_FOR_RAYTRACE = 2f;

    private static boolean canHitEntity(EntityPlayer attacker, Entity target) {
        if(attacker.getDistanceToEntity(target) < MAX_DAMAGE_DISTANCE) {
            Vector3f targetPos = new Vector3f((float)target.posX, (float)target.posY, (float)target.posZ);
            Vector3f attackerPos = new Vector3f((float)attacker.posX, (float)attacker.posY, (float)attacker.posZ);
            Vector3f attackerDir = Vector3f.sub(targetPos, attackerPos, null);
            attackerDir.y = 0f;
            if(attackerDir.length() != 0) attackerDir.normalise();

            Vec3 attackerLook = attacker.getLook(1.0f);
            attackerLook.yCoord = 0f;
            attackerLook.normalize();
            Vector3f attackerLook1 = new Vector3f((float)attackerLook.xCoord, 0f, (float)attackerLook.zCoord);

            double angle = (Vector3f.angle(attackerDir, attackerLook1) / Math.PI * 180f);

            if(angle <= ANGLE_FOR_RAYTRACE) {//точно смотрим на цель и стреляем тоже точно
                return attacker.canEntityBeSeen(target);
            } else return Math.abs(angle) < MAX_ANGLE;//вариант когда игрок смотрит не точно на цель и стреляет с дробовика и из за разброса попадает в цель
        }

        return false;
    }
}
