package ru.krogenit.guns.item;

import ru.krogenit.guns.util.config.ConfigMelee;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;

public class ItemMeleeWeaponServer extends ItemMeleeWeapon {
    public ItemMeleeWeaponServer(String name, float damage) {
        super(name, damage, 10000);
        ConfigMelee configGun = new ConfigMelee(name);
        configGun.load();
        addAttributeDamage(CharacterAttributeType.BALLISTIC_DAMAGE, configGun.getBallisticDamage());
        addAttributeDamage(CharacterAttributeType.CUT_DAMAGE, configGun.getCutDamage());
        addAttributeDamage(CharacterAttributeType.ENERGY_DAMAGE, configGun.getEnergyDamage());
        addAttributeDamage(CharacterAttributeType.THERMAL_DAMAGE, configGun.getThermalDamage());
        addAttributeDamage(CharacterAttributeType.FIRE_DAMAGE, configGun.getFireDamage());
        addAttributeDamage(CharacterAttributeType.ELECTRIC_DAMAGE, configGun.getElectricDamage());
        addAttributeDamage(CharacterAttributeType.TOXIC_DAMAGE, configGun.getToxicDamage());
        addAttributeDamage(CharacterAttributeType.RADIATION_DAMAGE, configGun.getRadiationDamage());
        addAttributeDamage(CharacterAttributeType.EXPLOSION_DAMAGE, configGun.getExplosionDamage());
        addAttributeDamage(CharacterAttributeType.FROZEN_DAMAGE, configGun.getFrozenDamage());
        addAttributeDamage(CharacterAttributeType.FRACTAL_DAMAGE, configGun.getFractalDamage());
        addAttributeDamage(CharacterAttributeType.PRICKLE_DAMAGE, configGun.getPrickleDamage());
        addAttributeDamage(CharacterAttributeType.PENETRATION, configGun.getPenetration());
        setMaxDamage(configGun.getHardness());
        setInvMatrixSize(configGun.getMatrixWidth(), configGun.getMatrixHeight());
        setItemQuality(configGun.getItemQuality());
        setItemRarity(configGun.getItemRarity());
        setDisplayName(configGun.getDisplayName());
        setSecondName(configGun.getSecondName());
        setDescription(configGun.getDescription());
        String[] split = configGun.getTags().split(",");
        for (String s : split) {
            if(s.length() > 0) getItemTags().add(EnumItemTag.valueOf(s.trim()));
        }
    }

    private void addAttributeDamage(CharacterAttributeType characterAttributeType, double value) {
        if(value > 0D) {
            CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder().attributeType(characterAttributeType).valueAdd(value).build();
            characterAttributeBoostMap.put(characterAttributeType, characterAttributeMod);
        }
    }
}
