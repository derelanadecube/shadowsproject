package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import ru.krogenit.guns.entity.EntityBullet;
import ru.krogenit.guns.entity.EntityBulletClient;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketBulletClient implements IPacketOut, IPacketIn {

    EntityBullet bullet;
    String playerName;
    Vector3f vect;

    public PacketBulletClient(EntityBullet bullet) {
        this.bullet = bullet;
    }

    @Override
    public void read(ByteBufInputStream data) throws IOException {
        playerName = data.readUTF();
        vect = new Vector3f(data.readFloat(), data.readFloat(), data.readFloat());
        handleMessageOnClientSide();
    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeUTF(bullet.thrower.getDisplayName());
        data.writeFloat((float) bullet.motionX);
        data.writeFloat((float) bullet.motionY);
        data.writeFloat((float) bullet.motionZ);
    }

    public void handleMessageOnClientSide() {
        Minecraft mc = Minecraft.getMinecraft();
        if(mc.theWorld != null) {
            EntityPlayer p1 = mc.theWorld.getPlayerEntityByName(playerName);
            if (p1 != null) {
                EntityBulletClient bullet = new EntityBulletClient(mc.theWorld, p1, 0, 0, new Vector4f());
                bullet.motionX = vect.x;
                bullet.motionY = vect.y;
                bullet.motionZ = vect.z;
                mc.theWorld.spawnEntityInWorld(bullet);
            }
        }
    }
}