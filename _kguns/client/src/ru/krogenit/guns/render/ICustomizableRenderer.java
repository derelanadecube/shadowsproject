package ru.krogenit.guns.render;

import ru.xlv.core.common.util.config.IConfigGson;

import java.util.List;
import java.util.Map;

public interface ICustomizableRenderer extends IConfigGson {

    Map<String, List<AnimationNode>> getAnimations();
    Map<String, RenderObject> getRenderObjects();
    void init();
    long getAnimationTime();
    long getStartAnimationTime();
}
