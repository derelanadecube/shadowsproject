package ru.xlv.post;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.xlv.core.XlvsCore;
import ru.xlv.post.handle.PostHandler;
import ru.xlv.post.network.*;

import static ru.xlv.post.XlvsPostMod.MODID;

@Mod(
        name = "XlvsPostMod",
        version = "1.0",
        modid = MODID
)
public class XlvsPostMod {

    static final String MODID = "xlvspost";

    @Mod.Instance(MODID)
    public static XlvsPostMod INSTANCE;

    @Getter
    private final PostHandler postHandler = new PostHandler();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketPostMarkViewed(),
                new PacketPostSend(),
                new PacketPostSync(),
                new PacketPostTakeAttachment(),
                new PacketPostDelete(),
                new PacketPostNew()
        );
    }
}
