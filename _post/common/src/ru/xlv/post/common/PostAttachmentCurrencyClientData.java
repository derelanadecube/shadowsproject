package ru.xlv.post.common;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.IOException;

@Getter
@AllArgsConstructor
public class PostAttachmentCurrencyClientData extends PostAttachmentClientData {

    private int amount;

    @Override
    public void write(ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeInt(amount);
    }
}
