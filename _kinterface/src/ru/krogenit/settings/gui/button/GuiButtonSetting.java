package ru.krogenit.settings.gui.button;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.GameSettings;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.settings.gui.GuiGraphicsSettings;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;

import java.awt.*;

public class GuiButtonSetting extends GuiButtonAdvanced {

    private final String settingName;
    private float blending;
    public boolean isPressed;
    private final String[] values;
    private int currentValue;
    private final GameSettings.Options option;
    private final float[] blendingValues;
    private final GuiGraphicsSettings parentGui;
    private final float yBasePos;

    public GuiButtonSetting(int id, float x, float y, float width, float height, GameSettings.Options option, GuiGraphicsSettings parentGui) {
        super(id, x, y, width, height, "");
        this.option = option;
        this.settingName = I18n.format(option.getEnumString());
        this.values = Minecraft.getMinecraft().gameSettings.getValues(option);
        this.currentValue = Minecraft.getMinecraft().gameSettings.getCurrentValue(option);
        this.blendingValues = new float[values.length];
        this.parentGui = parentGui;
        this.yBasePos = y;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            if(isPressed) {
                GL11.glDisable(GL11.GL_TEXTURE_2D);
                GL11.glColor4f(0f,0f,0f,1f);
                float rectWidth = this.width * 1.1f;
                GuiDrawUtils.drawRect(this.xPosition, this.yPosition, rectWidth, this.height);
                float x = this.xPosition + rectWidth;
                float y = this.yPosition;
                float rectHeight = this.height * values.length;
                GuiDrawUtils.drawRect(x, y, rectWidth, rectHeight);

                GL11.glColor4f(11/255f, 142/255f, 170/255f, 1.0f);
                GL11.glBegin(GL11.GL_LINE_STRIP);
                GL11.glVertex2f(xPosition, yPosition);
                GL11.glVertex2f(x + rectWidth, yPosition);
                GL11.glVertex2f(x + rectWidth, y + rectHeight);
                GL11.glVertex2f(x, y + rectHeight);
                GL11.glVertex2f(x, yPosition + this.height);
                GL11.glVertex2f(xPosition, yPosition + this.height);
                GL11.glVertex2f(xPosition, yPosition);
                GL11.glEnd();

                GL11.glEnable(GL11.GL_TEXTURE_2D);
                GL11.glColor4f(1f,1f,1f,1f);
                Utils.bindTexture(TextureRegister.textureSettingsCaseBorder);
                for(int i=0;i<values.length - 1;i++) {
                    GuiDrawUtils.drawRect(x, y + this.height + i * this.height, rectWidth, 1);
                }

                drawText(mc, 0, 0);
                drawValues(mc, mouseX, mouseY);
            } else {
                boolean hovered = isHovered(mouseX, mouseY);
                if (hovered) {
                    blending += AnimationHelper.getAnimationSpeed() * 0.1f;
                    if (blending > 1) blending = 1;
                } else {
                    blending -= AnimationHelper.getAnimationSpeed() * 0.1f;
                    if (blending < 0) blending = 0f;
                }

                Utils.bindTexture(texture);
                if(enabled) GL11.glColor4f(1f, 1f, 1f, 1f);
                else GL11.glColor4f(0.3f, 0.3f, 0.3f, 1f);
                GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);

                if (blending > 0) {
                    GL11.glColor4f(1f, 1f, 1f, blending);
                    GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);
                }

                GL11.glColor4f(1f, 1f, 1f, 1f);
                drawText(mc, 0, 0);
            }
        }
    }

    protected void drawText(Minecraft mc, int xOffset, int yOffset) {
        float offsetY = mc.displayHeight / 400f;
        float textScale = 0.9f;

        String s = settingName.toUpperCase();
        GuiDrawUtils.drawRightStringNoXYScale(FontType.HelveticaNeueCyrLight, s, this.xPosition - this.width / 10.0f + xOffset,
                this.yPosition + this.height / 2.0f + yOffset - offsetY, textScale, enabled ? 0xffffff : 0x444444);

        int j = 0xFFFFFF;

        if(!isPressed) {
            if(enabled) {
                int r = ((0x959595 & 0xFF0000) >> 16) + (int)((255 - ((0x959595 & 0xFF0000) >> 16)) * blending);
                int g = ((0x959595 & 0xFF00) >> 8) + (int)((255 - ((0x959595 & 0xFF00) >> 8)) * blending);
                int b = (0x959595 & 0xFF) + (int)((255 - ((0x959595 & 0xFF))) * blending);
                j = (int) Long.parseLong(Integer.toHexString(new Color(r,g,b).getRGB()), 16);
            } else {
                j = 0x333333;
            }
        }

        try {
            s = values[currentValue].toUpperCase();
            GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrBold, s, this.xPosition + this.width / 2.0f + xOffset,
                    this.yPosition + this.height / 2.0f + yOffset - offsetY, textScale, j);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void drawValues(Minecraft mc, int mouseX, int mouseY) {
        float offsetY = mc.displayHeight / 400f;
        float textScale = 0.9f;

        for(int i=0;i<values.length;i++) {
            float rectWidth = this.width * 1.1f;
            float x = this.xPosition + rectWidth;
            float y = this.yPosition + this.height * i;
            float rectHeight = this.height;

            boolean hovered = mouseX >= x && mouseY >= y && mouseX < x + rectWidth && mouseY < y + rectHeight;
            if (hovered) {
                blendingValues[i] += AnimationHelper.getAnimationSpeed() * 0.1f;
                if (blendingValues[i] > 1) blendingValues[i] = 1f;
            } else {
                blendingValues[i] -= AnimationHelper.getAnimationSpeed() * 0.1f;
                if (blendingValues[i] < 0) blendingValues[i] = 0f;
            }

            int r = ((0x959595 & 0xFF0000) >> 16) + (int)((255 - ((0x959595 & 0xFF0000) >> 16)) * blendingValues[i]);
            int g = ((0x959595 & 0xFF00) >> 8) + (int)((255 - ((0x959595 & 0xFF00) >> 8)) * blendingValues[i]);
            int b = (0x959595 & 0xFF) + (int)((255 - ((0x959595 & 0xFF))) * blendingValues[i]);
            int j = (int) Long.parseLong(Integer.toHexString(new Color(r,g,b).getRGB()), 16);

            String s = values[i].toUpperCase();
            GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrBold, s, x + rectWidth / 2.0f,
                    this.yPosition + this.height / 2.0f - offsetY + this.height * i, textScale, j);
        }
    }

    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        if(isPressed) {
            float rectWidth = this.width * 1.1f;
            float x = this.xPosition + rectWidth;
            float y = this.yPosition;
            float rectHeight = this.height * values.length;
            if(this.enabled && this.visible && ((mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + rectWidth && mouseY < this.yPosition + this.height) ||
                    (mouseX >= x && mouseY >= y && mouseX < x + rectWidth && mouseY < y + rectHeight))) {
                isPressed = false;

                for(int i=0;i<values.length;i++) {
                    y = this.yPosition + this.height * i;
                    rectHeight = this.height;
                    boolean hovered = mouseX >= x && mouseY >= y && mouseX < x + rectWidth && mouseY < y + rectHeight;
                    if (hovered) {
                        currentValue = i;
                        if(option == GameSettings.Options.GRAPHICS_PRESET) {
                            parentGui.setPreset(i);
                        }
                        return true;
                    }
                }

                return true;
            }
        } else {
            if(this.enabled && this.visible && mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height) {
                isPressed = true;
                return true;
            }
        }

        return false;
    }

    public boolean isHovered(int mouseX, int mouseY) {
        if(isPressed) {
            float rectWidth = this.width * 1.1f;
            float x = this.xPosition + rectWidth;
            float y = this.yPosition;
            float rectHeight = this.height * values.length;
            return this.field_146123_n = this.enabled && this.visible && ((mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + rectWidth && mouseY < this.yPosition + this.height) || (mouseX >= x && mouseY >= y && mouseX < x + rectWidth && mouseY < y + rectHeight));
        } else {
            return this.field_146123_n = this.enabled && this.visible && mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
        }
    }

    public void setYPosByScroll(float scroll) {
        this.yPosition = yBasePos + scroll;
    }

    public void setCurrentValue(int value) {
        this.currentValue = value;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public GameSettings.Options getOption() {
        return option;
    }
}
