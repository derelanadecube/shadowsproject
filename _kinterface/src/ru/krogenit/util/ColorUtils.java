package ru.krogenit.util;

import net.minecraft.item.Item;
import org.lwjgl.util.vector.Vector3f;
import ru.xlv.core.common.item.ItemBase;
import ru.xlv.core.common.item.tag.EnumItemTag;

public class ColorUtils {
    public static final Vector3f SLAG_COLOR = new Vector3f(0.5f, 0.5f, 0.5f);
    public static final Vector3f NORMAL_COLOR = new Vector3f(1f, 1f, 1f);
    public static final Vector3f UNCOMMON_COLOR = new Vector3f(0.0f, 0.5f, 0.0f);
    public static final Vector3f RARE_COLOR = new Vector3f(0.0f, 0.0f, 1f);
    public static final Vector3f EPIC_COLOR = new Vector3f(0.5f, 0.0f, 0.5f);
    public static final Vector3f SPECIAL_COLOR = new Vector3f(1f, 0f, 0f);
    public static final Vector3f LEGENDARY_COLOR = new Vector3f(0.86f, 0.647f, 0.125f);

    public static final Vector3f TAG_PISTOL_COLOR = new Vector3f(0.25f, 0.25f, 0.25f);
    public static final Vector3f TAG_REVOLVER_COLOR = new Vector3f(0.25f, 0.25f, 0.25f);
    public static final Vector3f TAG_SMG_COLOR = new Vector3f(0.25f, 0.25f, 0.25f);
    public static final Vector3f TAG_AUTOMATIC_COLOR = new Vector3f(0.25f, 0.25f, 0.25f);
    public static final Vector3f TAG_RIFLE_COLOR = new Vector3f(0.25f, 0.25f, 0.25f);
    public static final Vector3f TAG_CARBINE_COLOR = new Vector3f(0.25f, 0.25f, 0.25f);
    public static final Vector3f TAG_SNIPER_COLOR = new Vector3f(0.25f, 0.25f, 0.25f);
    public static final Vector3f TAG_SHOTGUN_COLOR = new Vector3f(0.25f, 0.25f, 0.25f);
    public static final Vector3f TAG_MACHINE_GUN_COLOR = new Vector3f(0.25f, 0.25f, 0.25f);
    public static final Vector3f TAG_LASER_GUN_COLOR = new Vector3f(0.25f, 0.25f, 0.25f);

    public static final Vector3f TAG_UNREGISTERED_ITEM_COLOR = new Vector3f(0.75f, 0.5f, 0.5f);
    public static final Vector3f TAG_REGISTERED_ITEM_COLOR = new Vector3f(0.75f, 0.5f, 0.75f);
    public static final Vector3f TAG_UNINSURED_ITEM_COLOR = new Vector3f(0.75f, 0.5f, 0.5f);
    public static final Vector3f TAG_INSURED_ITEM_COLOR = new Vector3f(0.5f, 0.5f, 0.5f);
    public static final Vector3f TAG_PERSONAL_CANDIDATE_COLOR = new Vector3f(0.5f, 0.75f, 0.5f);
    public static final Vector3f TAG_PERSONAL_COLOR = new Vector3f(0.5f, 0.75f, 0.5f);

    private static final int SLAG_HEX_COLOR = 0x808080;
    private static final int COMMON_HEX_COLOR = 0xffffff;
    private static final int UNCOMMON_HEX_COLOR = 0x008000;
    private static final int RARE_HEX_COLOR = 0x0000FF;
    private static final int EPIC_HEX_COLOR = 0x9F0080;
    private static final int SPECIAL_HEX_COLOR = 0xFF0000;
    private static final int LEGENDARY_HEX_COLOR = 0xDAA520;

    public static Vector3f getColorByTag(EnumItemTag type) {
        switch (type) {
            case REVOLVER: return TAG_REVOLVER_COLOR;
            case SMG: return TAG_SMG_COLOR;
            case AUTOMATIC: return TAG_AUTOMATIC_COLOR;
            case RIFLE: return TAG_RIFLE_COLOR;
            case CARBINE: return TAG_CARBINE_COLOR;
            case SNIPER: return TAG_SNIPER_COLOR;
            case SHOTGUN: return TAG_SHOTGUN_COLOR;
            case MACHINE_GUN: return TAG_MACHINE_GUN_COLOR;
            case LASER_GUN: return TAG_LASER_GUN_COLOR;
            case UNREGISTERED_ITEM: return TAG_UNREGISTERED_ITEM_COLOR;
            case REGISTERED_ITEM: return TAG_REGISTERED_ITEM_COLOR;
            case UNINSURED_ITEM: return TAG_UNINSURED_ITEM_COLOR;
            case INSURED_ITEM: return TAG_INSURED_ITEM_COLOR;
            case PERSONAL_CANDIDATE: return TAG_PERSONAL_CANDIDATE_COLOR;
            case PERSONAL: return TAG_PERSONAL_COLOR;
            default: return TAG_PISTOL_COLOR;
        }
    }

    public static Vector3f getColorByRarity(Item item) {
        if(item instanceof ItemBase) {
            switch (((ItemBase) item).getItemRarity()) {
                case SLAG: return SLAG_COLOR;
                case UNCOMMON: return UNCOMMON_COLOR;
                case RARE: return RARE_COLOR;
                case EPIC: return EPIC_COLOR;
                case SPECIAL: return SPECIAL_COLOR;
                case LEGENDARY: return LEGENDARY_COLOR;
                default: return NORMAL_COLOR;
            }
        } else {
            return NORMAL_COLOR;
        }
    }

    public static int getHexColorByRarity(Item item) {
        if(item instanceof ItemBase) {
            switch (((ItemBase) item).getItemRarity()) {
                case SLAG: return SLAG_HEX_COLOR;
                case UNCOMMON: return UNCOMMON_HEX_COLOR;
                case RARE: return RARE_HEX_COLOR;
                case EPIC: return EPIC_HEX_COLOR;
                case SPECIAL: return SPECIAL_HEX_COLOR;
                case LEGENDARY: return LEGENDARY_HEX_COLOR;
                default: return COMMON_HEX_COLOR;
            }
        } else {
            return 0xffffff;
        }
    }
}
