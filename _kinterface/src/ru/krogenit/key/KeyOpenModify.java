package ru.krogenit.key;

import net.minecraft.client.settings.KeyBinding;
import ru.krogenit.client.key.AbstractKey;
import ru.krogenit.modifi.GuiGunModify;

public class KeyOpenModify extends AbstractKey {
    private boolean keyDown = false;
    private boolean keyUp = true;

    public KeyOpenModify(KeyBinding keyBindings) {
        super(keyBindings);
    }

    @Override
    public void keyDown() {
        if (mc.currentScreen == null && !keyDown && mc.thePlayer != null) {
            mc.displayGuiScreen(new GuiGunModify());
        }
    }

    @Override
    public void keyUp() {
        if (!keyUp) {
            keyDown = false;
            keyUp = true;
        }
    }
}
