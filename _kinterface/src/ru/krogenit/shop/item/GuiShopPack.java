package ru.krogenit.shop.item;

import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.customfont.FontType;

public class GuiShopPack extends GuiButtonAdvanced {

    private final ShopPack shopPack;

    public GuiShopPack(int id, float x, float y, float width, float height, ShopPack shopPack) {
        super(id, x, y, width, height, shopPack.getName());
        this.shopPack = shopPack;
    }

    public void draw(int mouseX, int mouseY) {
        GL11.glColor4f(1f, 1f, 1f, 1f);
        GuiDrawUtils.drawRect(TextureRegister.textureShopMainBanner, xPosition, yPosition, width, height);
        GuiDrawUtils.drawRightStringNoXYScale(FontType.HelveticaNeueCyrBlack, displayString, xPosition + width - ScaleGui.get(42),
                yPosition + height - ScaleGui.get(82), 76 / 32f, 0xffffff);
        GuiDrawUtils.drawRightStringNoXYScale(FontType.HelveticaNeueCyrBlack, "V 1.0204 BETA", xPosition + width - ScaleGui.get(42),
                yPosition + height - ScaleGui.get(37), 22 / 32f, 0xffffff);

        float width = ScaleGui.get(232);
        float height = ScaleGui.get(154);
        float x = xPosition + this.width - ScaleGui.get(252);
        float y = yPosition + ScaleGui.get(20);
        float xOffset = ScaleGui.get(255);
        float itemOffset = ScaleGui.get(13);
        for(ItemStack itemStack : shopPack.getItemStacks()) {
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glColor4f(0f, 0f, 0f, 0.2f);
            GuiDrawUtils.drawRect(x, y, width, height);
            GL11.glColor4f(98 / 255f, 98 / 255f, 98 / 255f, 1.0f);
            GL11.glLineWidth(ScaleGui.get(2f));
            GL11.glBegin(GL11.GL_LINE_LOOP);
            GL11.glVertex2f(x, y);
            GL11.glVertex2f(x + width, y);
            GL11.glVertex2f(x + width, y + height);
            GL11.glVertex2f(x, y + height);
            GL11.glEnd();
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GuiDrawUtils.renderItem(itemStack, 1.0f, itemOffset, itemOffset, x, y, width, height, 0f);
            x -= xOffset;
        }

    }

    public boolean mousePressed(int mouseX, int mouseY) {
        return mouseX >= xPosition && mouseY >= yPosition && mouseX < xPosition + width && mouseY < yPosition + height;
    }


}
