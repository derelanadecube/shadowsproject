package ru.krogenit.shop.item;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.item.ItemStack;
import ru.krogenit.pda.EnumPdaLeftMenuSection;

@AllArgsConstructor
@Getter
public class ShopItem {

    private final int id;
    private final ItemStack itemStack;
    private final int creditCost;
    private final int platinaCost;
    private final EnumPdaLeftMenuSection[] categories;
    private final EnumItemBadge[] badges;

}
