package ru.krogenit.shop;

import net.minecraft.client.gui.GuiButton;
import org.apache.commons.lang3.StringUtils;
import org.lwjgl.input.Keyboard;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.IGuiSearch;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiButtonLeftMenu;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.shop.button.GuiButtonLeftMenuShop;
import ru.krogenit.shop.field.GuiSearchField;
import ru.krogenit.shop.item.EnumItemBadge;
import ru.krogenit.shop.item.ShopItem;
import ru.krogenit.shop.item.ShopPack;
import ru.krogenit.shop.pages.GuiPageShop;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;
import ru.xlv.shop.common.ShopItemCategory;
import ru.xlv.shop.common.ShopItemCost;
import ru.xlv.shop.handle.ShopHandler;
import ru.xlv.shop.handle.ShopItemManager;
import ru.xlv.shop.handle.ShopItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GuiShop extends GuiPda implements IGuiSearch {

    private GuiSearchField searchField;
    private static final List<ShopItem> shopItems = new ArrayList<>();
    private static final List<ShopPack> shopPacks = new ArrayList<>();

    @Override
    protected void createGui() {
        createControlButtons();
        GuiButtonAnimated buttonAnimated = new GuiButtonAnimated(3, 0,0,0,0, "ПОПОЛНИТЬ БАЛАНС");
        buttonAnimated.setTexture(TextureRegister.textureShopButtonBalance);
        buttonAnimated.setTextureHover(TextureRegister.textureESCButtonHover);
        buttonAnimated.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(buttonAnimated);

        GuiButtonLeftMenuShop b = new GuiButtonLeftMenuShop(EnumPdaLeftMenuSection.SHOP_MAIN, 0,0,0,0, "ГЛАВНАЯ", false);
        b.setTexture(EnumPdaLeftMenuSection.SHOP_MAIN);
        leftMenuButtons.add(b);
        b = new GuiButtonLeftMenuShop(EnumPdaLeftMenuSection.SHOP_NEW, 0,0,0,0, "НОВИНКИ", false);
        b.setTexture(EnumPdaLeftMenuSection.SHOP_NEW);
        leftMenuButtons.add(b);
        b = new GuiButtonLeftMenuShop(EnumPdaLeftMenuSection.SHOP_UNIQUE, 0,0,0,0, "ОСОБОЕ", false);
        b.setTexture(EnumPdaLeftMenuSection.SHOP_UNIQUE);
        leftMenuButtons.add(b);
        b = new GuiButtonLeftMenuShop(EnumPdaLeftMenuSection.SHOP_KITS, 0,0,0,0, "КОМПЛЕКТЫ", false);
        b.setTexture(EnumPdaLeftMenuSection.SHOP_KITS);
//        leftMenuButtons.add(b);
        b = new GuiButtonLeftMenuShop(EnumPdaLeftMenuSection.SHOP_SALE, 0,0,0,0, "РАСПРОДАЖА", false);
        b.setTexture(EnumPdaLeftMenuSection.SHOP_SALE);
//        leftMenuButtons.add(b);
        b = new GuiButtonLeftMenuShop(EnumPdaLeftMenuSection.SHOP_CASE, 0,0,0,0, "КЕЙСЫ", false);
        b.setTexture(EnumPdaLeftMenuSection.SHOP_CASE);
//        leftMenuButtons.add(b);

        b = new GuiButtonLeftMenuShop(EnumPdaLeftMenuSection.SHOP_WEAPONS, 0,0,0,0, "ОРУЖИЕ", false);
        b.setTexture(EnumPdaLeftMenuSection.SHOP_WEAPONS);
        leftMenuButtons.add(b);
        b = new GuiButtonLeftMenuShop(EnumPdaLeftMenuSection.SHOP_ARMOR, 0,0,0,0, "БРОНЯ", false);
        b.setTexture(EnumPdaLeftMenuSection.SHOP_ARMOR);
        leftMenuButtons.add(b);
        b = new GuiButtonLeftMenuShop(EnumPdaLeftMenuSection.SHOP_MISC, 0,0,0,0, "РАСХОДНИКИ", false);
        b.setTexture(EnumPdaLeftMenuSection.SHOP_MISC);
        leftMenuButtons.add(b);
        b = new GuiButtonLeftMenuShop(EnumPdaLeftMenuSection.SHOP_EXPERIENCE, 0,0,0,0, "ОПЫТ / УСИЛЕНИЯ", false);
        b.setTexture(EnumPdaLeftMenuSection.SHOP_EXPERIENCE);
        leftMenuButtons.add(b);
        b = new GuiButtonLeftMenuShop(EnumPdaLeftMenuSection.SHOP_VISUAL, 0,0,0,0, "ВИЗУАЛЬНЫЕ ЭФФЕКТЫ", false);
        b.setTexture(EnumPdaLeftMenuSection.SHOP_VISUAL);
        leftMenuButtons.add(b);
    }

    @Override
    public void initGui() {
        super.initGui();
        searchField = new GuiSearchField(ScaleGui.getCenterX(610), ScaleGui.getCenterY(95), ScaleGui.get(655), ScaleGui.get(37), ScaleGui.get(28 / 32f), "Поиск по магазину...", this);
        GuiButtonAnimated button = (GuiButtonAnimated) buttonList.get(3);
        button.xPosition = ScaleGui.getCenterX(1123); button.yPosition = ScaleGui.getCenterY(977); button.setWidth(ScaleGui.get(231)); button.setHeight(ScaleGui.get(40));
        Keyboard.enableRepeatEvents(true);

        syncCategory(EnumPdaLeftMenuSection.SHOP_MAIN, null);
    }

    @Override
    protected void openFirstPage() {
        leftMenuAction(leftMenuButtons.get(0));
    }

    private EnumPdaLeftMenuSection getAssociatedSection(ShopItemCategory shopItemCategory) {
        switch (shopItemCategory) {
            case ARMOR: return EnumPdaLeftMenuSection.SHOP_ARMOR;
            case BOOSTER: return EnumPdaLeftMenuSection.SHOP_EXPERIENCE;
            case CONSUMABLE: return EnumPdaLeftMenuSection.SHOP_MISC;
            case KIT: return EnumPdaLeftMenuSection.SHOP_KITS;
            case NEW: return EnumPdaLeftMenuSection.SHOP_NEW;
            case MAIN: return EnumPdaLeftMenuSection.SHOP_MAIN;
            case WEAPON: return EnumPdaLeftMenuSection.SHOP_WEAPONS;
            case SPECIAL: return EnumPdaLeftMenuSection.SHOP_UNIQUE;
            case SELL_OUT: return EnumPdaLeftMenuSection.SHOP_SALE;
            case VISUAL_EFFECT: return EnumPdaLeftMenuSection.SHOP_VISUAL;
            default: return null;
        }
    }

    private ShopItemCategory getAssociatedCategory(EnumPdaLeftMenuSection section) {
        switch (section) {
            case SHOP_ARMOR: return ShopItemCategory.ARMOR;
            case SHOP_EXPERIENCE: return ShopItemCategory.BOOSTER;
            case SHOP_MISC: return ShopItemCategory.CONSUMABLE;
            case SHOP_KITS: return ShopItemCategory.KIT;
            case SHOP_NEW: return ShopItemCategory.NEW;
            case SHOP_MAIN: return ShopItemCategory.MAIN;
            case SHOP_WEAPONS: return ShopItemCategory.WEAPON;
            case SHOP_UNIQUE: return ShopItemCategory.SPECIAL;
            case SHOP_SALE: return ShopItemCategory.SELL_OUT;
            case SHOP_VISUAL: return ShopItemCategory.VISUAL_EFFECT;
            default: return null;
        }
    }

    public ShopItem[] getItemsContainsName(String name) {
        List<ShopItem> findedItems = new ArrayList<>();
        for(ShopItem shopItem : shopItems) {
            String itemName = shopItem.getItemStack().getDisplayName();
            String unlocName = shopItem.getItemStack().getUnlocalizedName();
            if(StringUtils.containsIgnoreCase(itemName, name) || StringUtils.containsIgnoreCase(unlocName, name))
                findedItems.add(shopItem);
        }

        return findedItems.toArray(new ShopItem[0]);
    }

    public ShopItem[] getItemsInCategory(EnumPdaLeftMenuSection category) {
        List<ShopItem> findedItems = new ArrayList<>();
        for(ShopItem shopItem : shopItems) {
            EnumPdaLeftMenuSection[] categories = shopItem.getCategories();
            for(EnumPdaLeftMenuSection itemCategory : categories) {
                if(itemCategory == category) {
                    findedItems.add(shopItem);
                    break;
                }
            }
        }

        return findedItems.toArray(new ShopItem[0]);
    }

    public ShopPack[] getPacksInCategory(EnumPdaLeftMenuSection category) {
        List<ShopPack> findedItems = new ArrayList<>();
        for(ShopPack shopPack : shopPacks) {
            EnumPdaLeftMenuSection packCategory = shopPack.getCategory();
            if(packCategory == category) {
                findedItems.add(shopPack);
            }
        }

        return findedItems.toArray(new ShopPack[0]);
    }

    public void tryBuyItemCredits(ShopItem item) {
        ShopHandler.buy(item.getId(), ShopItemCost.Type.CREDITS).thenAcceptSync(result -> {
            if(result.isSuccess()) {
                SoundUtils.playGuiSound(SoundType.LETTER_SENT);
            }
            System.out.println(result.getResponseMessage());
        });
    }

    public void tryBuyItemPlatina(ShopItem item) {
        ShopHandler.buy(item.getId(), ShopItemCost.Type.PLATINUM).thenAcceptSync(result -> {
            if(result.isSuccess()) {
                SoundUtils.playGuiSound(SoundType.LETTER_SENT);
            }
            System.out.println(result.getResponseMessage());
        });
    }

    @Override
    public void search(String text) {
        if(text.length() > 0) {
            ShopItem[] findedItems = getItemsContainsName(text);
            for(GuiButtonLeftMenu b : leftMenuButtons) {
                b.setIsPressed(false);
            }

            GuiPageShop guiPageShopStandard = new GuiPageShop(this, findedItems, new ShopPack[]{});
            nextPage = guiPageShopStandard;
            nextPage.initGui();
            playOpenCloseAnim = true;
        }
    }

    @Override
    protected void initLeftButtons() {
        float buttonWidth = 306;
        float buttonHeight = 44;
        float x = 346;
        float y = 185;
        float yOffset = 11 + buttonHeight;

        float subButtonWidth = 246;
        float subButtonHeight = 44;

        for(GuiButtonLeftMenu b : leftMenuButtons) {
            b.xPosition = b.xPositionBase = ScaleGui.getCenterX(x, buttonWidth); b.yPosition = b.yPositionBase = ScaleGui.getCenterY(y, buttonHeight); b.width = ScaleGui.get(buttonWidth); b.height = ScaleGui.get(buttonHeight);

            for(GuiButtonLeftMenu sub : b.getSubButtons()) {
                sub.width = sub.widthBase = ScaleGui.get(subButtonWidth);
                sub.height = ScaleGui.get(subButtonHeight);
            }

            if(b.getEnumId() == EnumPdaLeftMenuSection.SHOP_CASE) y += 91;
            else if(b.getEnumId() == EnumPdaLeftMenuSection.SHOP_UNIQUE) y += 91f * 3f;//TODO: временно для убранных 3 кнопок;
            else y += yOffset;
        }
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 1 || b.id == 2) {
            this.mc.displayGuiScreen(null);
        }
    }

    @Override
    protected void leftMenuAction(GuiButtonLeftMenu b) {
        boolean ignoreClassCheck = true;

        switch (b.getEnumId()) {
            case SHOP_MAIN:
            case SHOP_NEW:
            case SHOP_UNIQUE:
            case SHOP_KITS:
            case SHOP_SALE:
            case SHOP_CASE:
            case SHOP_WEAPONS:
            case SHOP_ARMOR:
            case SHOP_MISC:
            case SHOP_EXPERIENCE:
            case SHOP_VISUAL:
                Runnable runnable = () -> {
                    setLeftMenuButtonPressed(b.getEnumId(), !b.isPressed(), true, false);
                    GuiPageShop guiPageShopStandard = new GuiPageShop(this, getItemsInCategory(b.getEnumId()), getPacksInCategory(b.getEnumId()));
                    nextPage = guiPageShopStandard;
                    nextPage.initGui();
                    if(nextPage != null) {
                        if(page != null && !ignoreClassCheck && page.getClass().equals(nextPage.getClass())) {
                            nextPage = null;
                            playOpenCloseAnim = true;
                        } else {
                            playOpenCloseAnim = true;
                        }
                    }
                };
                if(b.getEnumId() != EnumPdaLeftMenuSection.SHOP_CASE) {
                    syncCategory(b.getEnumId(), runnable);
                } else {
                    runnable.run();
                }
        }
    }

    private void syncCategory(EnumPdaLeftMenuSection enumPdaLeftMenuSection, Runnable runnable) {
        ShopItemManager.syncCategory(getAssociatedCategory(enumPdaLeftMenuSection), shopItems1 -> {
            synchronized (shopItems) {
                shopItems.removeIf(shopItem -> Arrays.asList(shopItem.getCategories()).contains(enumPdaLeftMenuSection));
                for (ru.xlv.shop.common.ShopItem shopItem : shopItems1) {
                    if(shopItem instanceof ShopItemStack) {
                        int creditCost = -1;
                        int platinumCost = -1;
                        for (ShopItemCost cost : shopItem.getCosts()) {
                            switch (cost.getType()) {
                                case CREDITS:
                                    creditCost = cost.getAmount();
                                    break;
                                case PLATINUM:
                                    platinumCost = cost.getAmount();
                            }
                        }
                        List<EnumPdaLeftMenuSection> categories = new ArrayList<>();
                        for (ShopItemCategory category : shopItem.getCategories()) {
                            categories.add(getAssociatedSection(category));
                        }
                        shopItems.add(new ShopItem(shopItem.getId(), ((ShopItemStack) shopItem).getItemStack(), creditCost, platinumCost, categories.toArray(new EnumPdaLeftMenuSection[0]), new EnumItemBadge[0]));
                    }
                }
            }
            if (runnable != null) {
                runnable.run();
            }
        });
    }

    private void drawMoney() {
        String platina = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getPlatinum();
        String credits = "" + XlvsMainMod.INSTANCE.getClientMainPlayer().getCredits();
        float fs = 80 / 32f;
        float slcredits = FontType.Marske.getFontContainer().width(credits) * fs;
        float slplatina = FontType.Marske.getFontContainer().width(platina) * fs;

        float creditWidth = 28;
        float creditHeight = 28;
        float platinaWidth = 28;
        float platinaHeight = 28;
        float x = 1062;
        float y = 990;
        GuiDrawUtils.drawRightStringCenter(FontType.Marske, platina, x, y - 4, fs, 0xDFDEDE);
        GuiDrawUtils.drawRightStringCenter(FontType.Marske, credits, x - platinaWidth * 2f - slplatina, y - 4, fs, 0xDFDEDE);
        y += 5;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconCredit, x - slcredits - slplatina - platinaWidth * 3f, y, creditWidth, creditHeight);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconPlatina, x - slplatina - platinaWidth, y, platinaWidth, platinaHeight);
    }

    @Override
    protected void drawBackground() {
        GuiDrawUtils.drawRect(TextureRegister.textureShopBackground, 0, 0, ScaleGui.screenWidth, ScaleGui.screenHeight);
    }

    @Override
    protected void drawBackgroundThings(int mouseX, int mouseY, float partialTick) {
        drawButtons(mouseX, mouseY, partialTick);

        drawBorders();
        drawProfileId();
        drawLoadingBar();
        drawControlsString();
        drawStatus();
        searchField.drawTextBox(mouseX, mouseY);
        drawMoney();
        drawLeftMenu(mouseX, mouseY, partialTick);
        GuiDrawUtils.drawCenter(TextureRegister.textureShopDevider, 172, 507, 370, 1);
        drawWorkSpace();
        drawPage(mouseX, mouseY, partialTick);
        drawWindows(mouseX, mouseY, partialTick);
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        searchField.updateCursorCounter();
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        searchField.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        super.mouseClickMove(mouseX, mouseY, lastButtonClicked, timeSinceMouseClick);
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        super.mouseMovedOrUp(mouseX, mouseY, which);
    }

    @Override
    public void handleMouseInput() {
        super.handleMouseInput();
    }

    @Override
    public void keyTyped(char character, int key) {
        super.keyTyped(character, key);
        searchField.textboxKeyTyped(character, key);
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        Keyboard.enableRepeatEvents(false);
    }
}
