package ru.krogenit.shop.field;

import net.minecraft.client.gui.Gui;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.GuiTextFieldAdvanced;
import ru.krogenit.client.gui.api.IGuiSearch;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.customfont.FontType;
import ru.xlv.customfont.StringCache;

import static org.lwjgl.opengl.GL11.glColor4f;

public class GuiSearchField extends GuiTextFieldAdvanced {

    private final IGuiSearch parentGui;
    private final String startText;

    public GuiSearchField(float x, float y, float width, float height, float fontScale, String startText, IGuiSearch parentGui) {
        super(FontType.HelveticaNeueCyrLight, x, y, width, height, fontScale);
        setText(startText);
        this.startText = startText;
        this.enabledColor = 0xcccccc;
        this.enableBackgroundDrawing = false;
        this.parentGui = parentGui;
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        mouseX = Mouse.getX();
        mouseY = mc.displayHeight - Mouse.getEventY() - 1;
        boolean flag = mouseX >= this.xPosition && mouseX < this.xPosition + this.width && mouseY >= this.yPosition && mouseY < this.yPosition + this.height;

        if (this.canLoseFocus) {
            this.setFocused(flag);
            if(isFocused && text.equals(startText)) {
                setText("");
            }
        }

        if (this.isFocused && mouseButton == 0) {
            float l = mouseX - (xPosition + ScaleGui.get(15));

            StringCache stringCache = fontContainer.getTextFont();
            String s = stringCache.trimStringToWidth(this.text.substring(this.lineScrollOffset), this.getWidth(), false);
            this.setCursorPosition(stringCache.trimStringToWidth(s, l / fontScale, false).length() + this.lineScrollOffset);
        }

        if(mouseX >= this.xPosition + width + ScaleGui.get(15) && mouseX < this.xPosition + this.width + ScaleGui.get(80f) && mouseY >= this.yPosition && mouseY < this.yPosition + this.height) {
            parentGui.search(getText());
        }
    }

    public void drawTextBox(int mouseX, int mouseY) {
        boolean hovered = mouseX >= this.xPosition && mouseX < this.xPosition + this.width + ScaleGui.get(80f) && mouseY >= this.yPosition && mouseY < this.yPosition + this.height;

        if(hovered) {
            GL11.glColor4f(153 / 255f, 153 / 255f, 153 / 255f, 1.0f);
        } else {
            GL11.glColor4f(102 / 255f, 102 / 255f, 102 / 255f, 1.0f);
        }

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glLineWidth(ScaleGui.get(2f));
        GL11.glBegin(GL11.GL_LINE_STRIP);
        GL11.glVertex2f(xPosition, yPosition);
        GL11.glVertex2f(xPosition, yPosition + height);
        GL11.glVertex2f(xPosition + width, yPosition + height);
        GL11.glVertex2f(xPosition + width, yPosition);
        GL11.glVertex2f(xPosition, yPosition);
        GL11.glEnd();
        GL11.glBegin(GL11.GL_LINES);
        float offX = ScaleGui.get(10f);
        GL11.glVertex2f(xPosition + width, yPosition + height / 2f);
        GL11.glVertex2f(xPosition + width + offX, yPosition + height / 2f);
        GL11.glEnd();
        float x1 = xPosition + width + offX;
        float width1 = ScaleGui.get(69);
        GuiDrawUtils.drawRect(x1, yPosition, width1, height);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        float iconSize = ScaleGui.get(19);
        GuiDrawUtils.drawRect(TextureRegister.textureShopIconSearch, xPosition + width - ScaleGui.get(27), yPosition + ScaleGui.get(9), iconSize, iconSize);
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "Искать", xPosition + width + offX + ScaleGui.get(11), yPosition + ScaleGui.get(17), 28 / 32f, 0xffffff);

        int i = this.isEnabled ? this.enabledColor : this.disabledColor;
        int j = this.cursorPosition - this.lineScrollOffset;
        int k = this.selectionEnd - this.lineScrollOffset;
        String s = fontContainer.getTextFont().trimStringToWidth(this.text.substring(this.lineScrollOffset), this.getWidth(), false);
        boolean flag = j >= 0 && j <= s.length();
        boolean flag1 = this.isFocused && this.cursorCounter / 6 % 2 == 0 && flag;
        float l = this.xPosition + ScaleGui.get(15);
        float i1 = this.yPosition + (this.height) / 2f;
        float j1 = l;

        if (k > s.length()) {
            k = s.length();
        }

        if (s.length() > 0) {
            String s1 = flag ? s.substring(0, j) : s;
            GL11.glPushMatrix();
            GL11.glTranslatef(l, i1, 0f);
            GL11.glScalef(fontScale, fontScale, 1.0f);
            j1 = fontContainer.drawString(s1, 0, 0, i) * fontScale + l;
            GL11.glPopMatrix();
        }

        boolean flag2 = this.cursorPosition < this.text.length() || this.text.length() >= this.getMaxStringLength();
        float k1 = j1;

        if (!flag) {
            k1 = j > 0 ? l + this.width : l;
        } else if (flag2) {
            k1 = j1 - 1;
            --j1;
        }

        if (s.length() > 0 && flag && j < s.length()) {
            GuiDrawUtils.drawStringNoScale(fontContainer, s.substring(j), j1, i1, fontScale, i);
        }

        if (flag1) {
            if (flag2) {
                Gui.drawRect(k1, i1 - ScaleGui.get(8f), k1 + 1, i1 + ScaleGui.get(10), -3092272);
            } else {
                GuiDrawUtils.drawStringNoScale(fontContainer, "_", k1, i1, fontScale, i);
            }
        }

        if (k != j) {
            float l1 = l + fontContainer.getTextFont().getStringWidth(s.substring(0, k)) * fontScale;
            this.drawCursorVertical(k1, i1 - ScaleGui.get(5), l1 - ScaleGui.get(1), i1 + ScaleGui.get(9));
        }

        glColor4f(1f, 1f, 1f, 1f);
    }

    @Override
    public boolean textboxKeyTyped(char character, int key) {
        boolean value = super.textboxKeyTyped(character, key);
        if(isFocused) {
            parentGui.search(getText());
        }
        return value;
    }
}
