package ru.krogenit.pda.gui.pages.auction;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.gui.GuiButton;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.customfont.FontType;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GuiPageAuction extends AbstractGuiScreenAdvanced {

    private float firstPoint, secondPoint, thirdPoint, fs1, fs2, fs3;
    private int totalLots, dayLots;
    private int openedLots, closedLots, incomingPurchases, incomingPayments;
    private int gold, silver, bronze;
    private int goldBank, silverBank, bronzeBank;

    private List<GuiAuctionLot> lots;

    public GuiPageAuction(float minAspect) {
        super(minAspect);
        lots = new ArrayList<>();
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();

        float buttonWidth = 231;
        float buttonHeight = 40;
        float x = 755;
        float y = ScaleGui.getCenterY(840, buttonHeight);
        GuiButtonAnimated b = new GuiButtonAnimated(0, ScaleGui.getCenterX(x,buttonWidth), y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "АУКЦИОН");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);
        x+=270;

        b = new GuiButtonAnimated(1, ScaleGui.getCenterX(x,buttonWidth), y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВАШИ ЛОТЫ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        x+=270;
        b = new GuiButtonAnimated(2, ScaleGui.getCenterX(x,buttonWidth), y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВАШИ СТАВКИ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        x+=270;
        b = new GuiButtonAnimated(3, ScaleGui.getCenterX(x,buttonWidth), y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВЫСТАВИТЬ ЛОТ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        buttonWidth = 1096;
        buttonHeight = 154;
        x = 1160;
        y = 694;
        for(GuiAuctionLot lot : lots) {
            lot.setXPosition(ScaleGui.getCenterX(x, buttonWidth)); lot.setYPosition(ScaleGui.getCenterY(y, buttonHeight)); lot.setWidth(ScaleGui.get(buttonWidth)); lot.setHeight(ScaleGui.get(buttonHeight));
            lot.createButtons();
        }
    }

    public void addAuctionItem(AuctionItem item) {
        GuiAuctionLot lot = new GuiAuctionLot(0,0,0,0, "ВАША СТАВКА ВЫИГРАЛА!", item);
        lot.setTexture(TextureRegister.texturePDAAuctionIconDone);
        lots.add(lot);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
//        if(b.id == 0) {
//
//        } else if(b.id == 1) {
//
//        } else if(b.id == 2) {
//
//        } else if(b.id == 3) {
//
//        }
    }

    private void updateAnimation() {
        firstPoint = AnimationHelper.updateSlowEndAnim(firstPoint, 1f, 0.1f, 0.0001f);
        if(firstPoint > 0.9f) {
            fs1 = AnimationHelper.updateSlowEndAnim(fs1, 1f, 0.1f, 0.0001f);
            if(fs1 > 0.9f) {
                secondPoint = AnimationHelper.updateSlowEndAnim(secondPoint, 1f, 0.1f, 0.0001f);
                if(secondPoint > 0.9f) {
                    fs2 = AnimationHelper.updateSlowEndAnim(fs2, 1f, 0.1f, 0.0001f);
                    if (fs2 > 0.9f) {
                        thirdPoint = AnimationHelper.updateSlowEndAnim(thirdPoint, 1f, 0.1f, 0.0001f);
                        if(thirdPoint > 0.9f) {
                            fs3 = AnimationHelper.updateSlowEndAnim(fs3, 1f, 0.1f, 0.0001f);
                        }
                    }
                }
            }
        }
    }


    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        updateAnimation();
        GL11.glColor4f(1f, 1f, 1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAAuctionServerImage, 1160, 331, 1096, 299 * firstPoint);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 501, 1096 * fs1, 2);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 588, 1096 * fs1, 2);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 800, 1096 * fs1, 2);

        LocalDateTime time = LocalDateTime.now();
        float x = 1665;
        float y = 236;
        float fs = (72 / 32f) * fs1;
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrBlack, "AUCTION HOUSE", x, y, fs, 0xffffff);
        y+=44;
        fs = (18 / 32f) * secondPoint;
        String timeString = DateTimeFormatter.ofPattern("dd.MM.yyyy").format(time);
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrBlack, "DATE " + timeString, x, y, fs, 0xffffff);
        fs = (52 / 32f) * fs2;
        y += 116;
        String s = "" + totalLots;
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrMedium,  s, x, y, fs, 0x1BC3EC);
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrMedium,  "ВСЕГО ЛОТОВ: ", x - FontType.HelveticaNeueCyrMedium.getFontContainer().width(s) * fs, y, fs, 0xffffff);
        fs = (52 / 32f) * fs3;
        y += 30;
        s = "" + dayLots;
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrMedium,  s, x, y, fs, 0x1BC3EC);
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrMedium,  "НОВЫХ ЛОТОВ ЗА ДЕНЬ: ", x - FontType.HelveticaNeueCyrMedium.getFontContainer().width(s) * fs, y, fs, 0xffffff);

        x = 629;
        y = 543;
        float iconWidth = 37;
        float stringMinusYOffset = 14;
        float stringPositiveYOffset = 10;
        float stringXOffset = 27;
        fs = 1.15f  * fs1;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        s = "ОТКРЫТЫЕ ЛОТЫ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  "" + openedLots, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ЗАКРЫТЫЕ ЛОТЫ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + closedLots, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        x += 301;
        fs = 1.15f  * fs2;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * secondPoint, iconWidth * secondPoint);
        s = "ВХОДЯЩИЕ ПОКУПКИ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + incomingPurchases, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ВХОДЯЩИЕ ПЛАТЕЖИ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + incomingPayments, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        float iconYOffset = 2;
        float valuesYOffset = 0;
        float valuesXOffset = 12;
        x += 334;
        fs = 1.15f * fs3;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * thirdPoint, iconWidth * thirdPoint);
        iconWidth = 14 * fs3;
        float iconHeight = 14 * fs3;
        s = "ВАШИ СРЕДСТВА: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        float xOffset = FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + stringXOffset + 10;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconGold, x + xOffset, y - stringMinusYOffset + iconYOffset, iconWidth, iconHeight);
        fs = 1.0f  * fs3;
        s = "" + gold;
        xOffset +=  valuesXOffset;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y - stringMinusYOffset + valuesYOffset, fs, 0xffffff);
        xOffset += FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + 20;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconSilver, x + xOffset, y - stringMinusYOffset + iconYOffset, iconWidth, iconHeight);
        s = "" + silver;
        xOffset += valuesXOffset;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y - stringMinusYOffset + valuesYOffset, fs, 0xffffff);
        xOffset += FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + 20;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconBronze, x + xOffset, y - stringMinusYOffset + iconYOffset, iconWidth, iconHeight);
        s = "" + bronze;
        xOffset += valuesXOffset;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y - stringMinusYOffset + valuesYOffset, fs, 0xffffff);

        s = "СРЕДСТВА В БАНКЕ: ";
        fs = 1.15f  * fs3;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        xOffset = FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + stringXOffset + 10;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconGold, x + xOffset, y + stringPositiveYOffset + iconYOffset, iconWidth, iconHeight);
        fs = 1.0f  * fs3;
        s = "" + goldBank;
        xOffset +=  valuesXOffset;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y + stringPositiveYOffset + valuesYOffset, fs, 0xffffff);
        xOffset += FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + 20;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconSilver, x + xOffset, y + stringPositiveYOffset + iconYOffset, iconWidth, iconHeight);
        s = "" + silverBank;
        xOffset += valuesXOffset;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y + stringPositiveYOffset + valuesYOffset, fs, 0xffffff);
        xOffset += FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + 20;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconBronze, x + xOffset, y + stringPositiveYOffset + iconYOffset, iconWidth, iconHeight);
        s = "" + bronzeBank;
        xOffset += valuesXOffset;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y + stringPositiveYOffset + valuesYOffset, fs, 0xffffff);

        drawButtons(mouseX, mouseY, partialTick);

        for(GuiAuctionLot lot : lots) {
            lot.drawButton(mouseX, mouseY);
        }
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (mouseButton == 0) {
            for (GuiButton b : this.buttonList) {
                if (b.mousePressed(this.mc, mouseX, mouseY)) {
                    this.selectedButton = b;
                    b.playClickSound(this.mc.getSoundHandler());
                    this.actionPerformed(b);
                }
            }
            for(GuiAuctionLot lot : lots) {
                lot.mousePressed(this.mc, mouseX, mouseY);
            }
        }
    }
}
