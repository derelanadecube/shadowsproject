package ru.krogenit.pda.gui.windows.friends;

import net.minecraft.client.gui.GuiButton;
import org.lwjgl.input.Keyboard;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.GuiPopup;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.pda.gui.windows.EnumWindowType;
import ru.krogenit.pda.gui.windows.GuiWinButtonWithSubs;
import ru.krogenit.pda.gui.windows.mail.GuiWindowWriteMessage;
import ru.krogenit.pda.gui.windows.players.GuiWindowPlayers;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.player.ClientPlayer;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;
import ru.xlv.friend.network.PacketFriendPlayerListGet;
import ru.xlv.friend.network.PacketFriendRemove;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.common.Group;
import ru.xlv.group.network.PacketGroupInvite;

import java.util.Set;

public class GuiWindowFriends extends GuiWindowPlayers {

    private int onlineFriends, totalFriends;
    private int incomingFriendsRequests;

    public GuiWindowFriends(GuiPda pda, float width, float height) {
        super(pda, width, height);
        this.windowType = EnumWindowType.FRIENDS;
        setHeader("ДРУЗЬЯ");
        setLeftCornerDesc("FILE №89065012_CODE_FRIENDS");
        setBackgroundTexture(TextureRegister.texturePDAWinFriendsBg);
    }

    protected void initButtons() {
        float buttonWidth = 159;
        float buttonHeight = 39;
        float x = ScaleGui.get(579);
        float y = ScaleGui.get(70);
        GuiButtonAnimated button = new GuiButtonAnimated(2, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ЗАЯВКИ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
        x += ScaleGui.get(178);
        button = new GuiButtonAnimated(3, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "СЕРВЕР");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
    }

    @Override
    protected void initPlayers() {
        float buttonWidth = ScaleGui.get(996);
        float buttonHeight = ScaleGui.get(37);

        Set<String> allFriendNames = friendHandler.getAllFriendNames();
        int id = 1;
        for(String friendName : allFriendNames) {
            //TODO: получение уровня и класса и последнее время захода
            GuiButtonFriend s = new GuiButtonFriend(id, 0, 0, buttonWidth, buttonHeight, this, friendName, CharacterType.MEDIC,
                    0, 0, false);
            players.add(s);
            playerByName.put(friendName, s);
            id++;
        }
    }

    @Override
    protected void getNewInfo(int offset) {
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendPlayerListGet(PacketFriendPlayerListGet.FilterType.FRIENDS, offset)).thenAcceptSync(clientPlayers -> {
            if(clientPlayers.size() > 0) {
                for (ClientPlayer clientPlayer : clientPlayers) {
                    GuiButtonFriend friend = (GuiButtonFriend) playerByName.get(clientPlayer.getPlayerName());
                    if (friend != null) {
                        friend.setCharacterType(clientPlayer.getCharacterType());
                        friend.setLevel(clientPlayer.getLevel());
                        friend.setLastOnline(System.currentTimeMillis() - clientPlayer.getLastOnlinePeriodTimeMills());
                    }
                }
            }
        });

        requestedElements += 20;
    }

    @Override
    protected void onFriendsSynced() {
        super.onFriendsSynced();
        incomingFriendsRequests = friendHandler.getAllIncomingInvites().size();
        Set<String> allFriendNames = friendHandler.getAllFriendNames();
        totalFriends = allFriendNames.size();
        onlineFriends = 0;
        for(String friendName : allFriendNames) {
            if(mc.theWorld.getPlayerEntityByName(friendName) != null) {
                onlineFriends++;
            }
        }
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 3) {
            pda.showServerPlayers(); return;
        }
        super.actionPerformed(b);
    }

    @Override
    public void subAction(GuiWinButtonWithSubs button, int id) {
        GuiButtonFriend b = (GuiButtonFriend) button;
        if(id == 0) {
            pda.showOrCreateWindow(EnumWindowType.MAIL_WRITE, new GuiWindowWriteMessage(pda, b.getPlayerName(), ""), false);
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_WRITE, true);
        } else if(id == 1) {
            pda.showProfile(b.getPlayerName());
        } else if(id == 2) {
            Group group = XlvsGroupMod.INSTANCE.getGroup();
            if(group == null || (group.getLeader().equals(mc.thePlayer.getDisplayName()) && !group.getPlayers().contains(b.getPlayerName()))) {
                XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketGroupInvite(b.getPlayerName())).thenAcceptSync(s -> {
                    pda.setPopup(new GuiPopup(pda, "ПРИГЛАШЕНИЕ В ГРУППУ ОТПРАВЛЕНО", s, GuiPopup.green));
                });
            }
        } else if(id == 3) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendRemove(b.getPlayerName())).thenAcceptSync(s -> {
                pda.setPopup(new GuiPopup(pda, "ИГРОК УДАЛЕН ИЗ ДРУЗЕЙ", s, GuiPopup.green));
                totalFriends--;
                friendHandler.removeFriend(b.getPlayerName());
                if(mc.theWorld.getPlayerEntityByName(b.getPlayerName()) != null) {
                    onlineFriends--;
                }
                refreshValues();
            });
        }
    }

    @Override
    protected void drawBackgroundThings() {
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(128), ScaleGui.get(872), ScaleGui.get(2));
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(208), ScaleGui.get(872), ScaleGui.get(2));

        float x = windowXAnim + ScaleGui.get(62);
        float y = windowYAnim + ScaleGui.get(91);
        float iconWidth = ScaleGui.get(37);
        float stringMinusYOffset = ScaleGui.get(14);
        float stringPositiveYOffset = ScaleGui.get(10);
        float stringXOffset = ScaleGui.get(27);
        float fs = ScaleGui.get(1.2f) * fs1;
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        String s = "ДРУЗЬЯ: ";
        FontContainer fontContainer = FontType.HelveticaNeueCyrLight.getFontContainer();
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, "" + totalFriends, x + stringXOffset + fontContainer.width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ОНЛАЙН: ";
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, onlineFriends + "", x + stringXOffset + fontContainer.width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        x += ScaleGui.get(249);
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        s = "ЗАЯВКИ: ";
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, "" + incomingFriendsRequests, x + stringXOffset + fontContainer.width(s) * fs, y, fs, 0x1BC3EC);

        fs = 28 / 32f;
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "№", windowXAnim + ScaleGui.get(57), windowYAnim + ScaleGui.get(234.5f), fs, 0x666666);
    }

    @Override
    protected void drawScroll() {
        guiScroll.drawScroll(windowXAnim + ScaleGui.get(909),
                windowYAnim + ScaleGui.get(250),
                ScaleGui.get(528)  * secondPoint * collapseAnim,
                windowYAnim + ScaleGui.get(788));
    }

    @Override
    public void onWindowClosed() {
        Keyboard.enableRepeatEvents(false);
        pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.COMMUNITY_FRIENDS, false);
    }
}
