package ru.krogenit.animator;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.common.MinecraftForge;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.guns.render.*;
import ru.krogenit.inventory.GuiMatrixSlot;
import ru.krogenit.inventory.GuiMatrixSlotSpecial;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryUtils;
import ru.xlv.customfont.FontType;

import java.text.DecimalFormat;
import java.util.*;

public class GuiAnimation extends AbstractGuiScreenAdvanced {

    @Deprecated private AbstractItemGunRenderer gunRenderer;
    @Deprecated private AbstractMeleeWeaponRenderer meleeWeaponRenderer;
    private ICustomizableRenderer renderer;
    private final List<GuiTextField> textFields = new ArrayList<>();
    private final Map<Integer, Vector3fConfigurable> vectorButtonIds = new HashMap<>();
    private final Map<Vector2f, String> vectorInfo = new HashMap<>();
    private final Map<Integer, AnimationNode> nodes = new HashMap<>();
    private float translationValue = 0.1f;
    private static final DecimalFormat decimalFormat = new DecimalFormat("###.####");
    private boolean base, baseAnims, loadAnims, unloadAnims, reloadAnims, hitAnims, rightClickAnims;
    private GuiMatrixSlot matrixSlot;
    private GuiMatrixSlotSpecial matrixSlotSpecialPistol;
    private GuiMatrixSlotSpecial matrixSlotSpecialMain;
    private final ItemStack itemStack = mc.thePlayer.getCurrentEquippedItem();

    @Deprecated
    public GuiAnimation(AbstractItemGunRenderer renderer) {
        this.gunRenderer = renderer;
    }

    @Deprecated
    public GuiAnimation(AbstractMeleeWeaponRenderer weaponRenderer) {
        this.meleeWeaponRenderer = weaponRenderer;
    }

    public GuiAnimation(ICustomizableRenderer renderer) {
        this.renderer = renderer;
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        textFields.clear();
        vectorButtonIds.clear();
        vectorInfo.clear();
        nodes.clear();
        float x = 0f;
        float y = 25f;
        float fieldWidth = 80f;
        float size = 40f;
        float offsetButtonX = 40f;
        float textScale = ScaleGui.get(2f);

        if(base)
            initBase();
        else if(baseAnims) initBaseAnims();
        else if(loadAnims) initAnims(gunRenderer.getReloadAnimationNodes());
        else if(unloadAnims) initAnims(gunRenderer.getUnloadAnimationNodes());
        else if(hitAnims) initAnims(meleeWeaponRenderer.getHitAnimations());
        else if(rightClickAnims) initAnims(meleeWeaponRenderer.getRightClickAnimations());
        else if(reloadAnims) initAnims(gunRenderer.getFullReloadAnimationNodes());

        x = 0f;
        y = 1000f;
        float buttonHeight = size / 1.5f;
        GuiTextFieldAdvanced field = new GuiTextFieldAdvanced(FontType.FUTURA_PT_MEDIUM, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y+buttonHeight), ScaleGui.get(fieldWidth), ScaleGui.get(size/1.25f), ScaleGui.get(2f));
        field.setText(decimalFormat.format(translationValue));
        textFields.add(field);
        buttonList.add(new GuiButtonAdvanced(1000, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(size), ScaleGui.get(buttonHeight),"-", textScale));
        buttonList.add(new GuiButtonAdvanced(1001, ScaleGui.getCenterX(x+size), ScaleGui.getCenterY(y), ScaleGui.get(size), ScaleGui.get(buttonHeight),"+", textScale));
        vectorInfo.put(new Vector2f(x, y-buttonHeight*2f), "это значение");
        vectorInfo.put(new Vector2f(x, y-buttonHeight*1.5f), "добавляется на");
        vectorInfo.put(new Vector2f(x, y-buttonHeight), "кнопки + и -");

        float buttonWidth = 200;
        buttonHeight = 40f;
        x = 200;
        y = 1000;
        buttonList.add(new GuiButtonAdvanced(1006, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y - 40), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),"Сменить вид", textScale));
        buttonList.add(new GuiButtonAdvanced(1002, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),"Базовая настройка", textScale));
        if(renderer == null) {
            x += buttonWidth + 2;
            buttonList.add(new GuiButtonAdvanced(1003, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Базовые анимации", textScale));
        }
        if(gunRenderer != null) {
            x += buttonWidth + 2;
            buttonList.add(new GuiButtonAdvanced(1008, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Зарядка", textScale));
            x += buttonWidth + 2;
            buttonList.add(new GuiButtonAdvanced(1013, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Разрядка", textScale));
            x += buttonWidth + 2;
            buttonList.add(new GuiButtonAdvanced(1018, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Перезарядка", textScale));
        } else if(meleeWeaponRenderer != null) {
            x += buttonWidth + 2;
            buttonList.add(new GuiButtonAdvanced(1014, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Анимация удара", textScale));
            x += buttonWidth + 2;
            buttonList.add(new GuiButtonAdvanced(1015, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Анимация защиты", textScale));
        }

        x += buttonWidth + 2;
        buttonList.add(new GuiButtonAdvanced(1016, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),"Экспорт", textScale));
        x += buttonWidth + 2;
        buttonList.add(new GuiButtonAdvanced(1017, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),"Импорт", textScale));
    }

    private void initAnims(List<AnimationNode> anims) {
        float x = 0f;
        float y = 0;
        float buttonWidth = 240;
        float buttonHeight = 30f;
        float textScale = ScaleGui.get(2f);

        buttonList.add(new GuiButtonAdvanced(1009, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),"добавить", textScale));
        x+=buttonWidth;
        buttonList.add(new GuiButtonAdvanced(1010, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),"удалить", textScale));
        x+=buttonWidth;
        buttonList.add(new GuiButtonAdvanced(1011, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),"play/stop", textScale));
        x+=buttonWidth;
        buttonList.add(new GuiButtonAdvanced(1012, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),"reset", textScale));
        x += buttonWidth;
        vectorInfo.put(new Vector2f(x, y+6), "типы: 0 - линейная, 1 - затихает в конце, 2 - постепено растет, 3 - плавно растет и затихает");

        int id = 0;
        y = 0f;
        for(AnimationNode node : anims) {
            nodes.put(id, node);
            addAnimationNode(id, y, node);
            y += 50f;
            id += 58;
        }
    }

    private void addAnimationNode(int id, float y, AnimationNode node) {
        float x = 0f;
        float fieldWidth = 60f;
        float size = 30f;
        float buttonHeight = 30f;
        float textScale = ScaleGui.get(2f);
        float buttonHeight1 = size / 1.5f;
        x = 0;
        y += buttonHeight*2.5f;
        GuiTextFieldAdvanced field = new GuiTextFieldAdvanced(FontType.FUTURA_PT_MEDIUM, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y+buttonHeight1),  ScaleGui.get(fieldWidth), ScaleGui.get(size/1.25f), ScaleGui.get(2f));
        field.setText("" + node.getId());
        textFields.add(field);
        buttonList.add(new GuiButtonAdvanced(id, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(size), ScaleGui.get(buttonHeight1),"-", textScale));
        buttonList.add(new GuiButtonAdvanced(++id, ScaleGui.getCenterX(x+size), ScaleGui.getCenterY(y), ScaleGui.get(size), ScaleGui.get(buttonHeight1),"+", textScale));
        vectorInfo.put(new Vector2f(x, y - 20), "айди");
        id+=1;
        x += fieldWidth;
        field = new GuiTextFieldAdvanced(FontType.FUTURA_PT_MEDIUM, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y+buttonHeight1),  ScaleGui.get(fieldWidth), ScaleGui.get(size/1.25f), ScaleGui.get(2f));
        field.setText("" + node.getType().ordinal());
        textFields.add(field);
        buttonList.add(new GuiButtonAdvanced(id, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(size), ScaleGui.get(buttonHeight1),"-", textScale));
        buttonList.add(new GuiButtonAdvanced(++id, ScaleGui.getCenterX(x+size), ScaleGui.getCenterY(y), ScaleGui.get(size), ScaleGui.get(buttonHeight1),"+", textScale));
        vectorInfo.put(new Vector2f(x, y - 20), "тип");
        id+=1;
        x += fieldWidth;
        addVector(id, node.getSpeed(), x, y, size, fieldWidth, "скорость");
        id+=6;
        x += fieldWidth * 3f;
        addVector(id, node.getWeights()[0], x, y, size, fieldWidth, "общая позиция");
        id+=6;
        x += fieldWidth * 3f;
        addVector(id, node.getWeights()[1], x, y, size, fieldWidth, "общий поворот");
        id+=6;
        x += fieldWidth * 3f;
        addVector(id, node.getWeights()[2], x, y, size, fieldWidth, "позиция оружия");
        id+=6;
        x += fieldWidth * 3f;
        addVector(id, node.getWeights()[3], x, y, size, fieldWidth, "поворот оружия");
        id+=6;
        x += fieldWidth * 3f;
        addVector(id, node.getWeights()[4], x, y, size, fieldWidth, "позиция л руки");
        id+=6;
        x += fieldWidth * 3f;
        addVector(id, node.getWeights()[5], x, y, size, fieldWidth, "поворот л руки");
        id+=6;
        x += fieldWidth * 3f;
        addVector(id, node.getWeights()[6], x, y, size, fieldWidth, "позиция п руки");
        id+=6;
        x += fieldWidth * 3f;
        addVector(id, node.getWeights()[7], x, y, size, fieldWidth, "поворот п руки");
    }

    private void initBaseAnims() {
        float x = 0f;
        float y = 25f;
        float fieldWidth = 80f;
        float size = 40f;
        float buttonWidth = 240;
        float buttonHeight = 30f;
        float textScale = ScaleGui.get(2f);

        if(gunRenderer != null) {
            int id = 0;
            addVector(id, gunRenderer.getSprintingTranslate(), x, y, size, fieldWidth, "бег");
            id+=6;
            y+= size*1.5f;
            addVector(id, gunRenderer.getSprintingRotation(), x, y, size, fieldWidth, "");
            y+= size*1.5f;
            buttonList.add(new GuiButtonAdvanced(1004, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),"play/stop", textScale));

            id+=6;
            y+= size*2f;
            addVector(id, gunRenderer.getAimPosition(), x, y, size, fieldWidth, "прицеливание");
            id+=6;
            y+= size*1.5f;
            addVector(id, gunRenderer.getAimRotation(), x, y, size, fieldWidth, "");
            y+= size*1.5f;
            buttonList.add(new GuiButtonAdvanced(1005, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),"play/stop", textScale));
        }
    }

    private void initBase() {
        float x = 0f;
        float y = 25f;
        float fieldWidth = 80f;
        float size = 40f;

        float cellHeight = ScaleGui.get(57);

        int w = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
        int h = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
        float slotX = 1300;
        float slotY = 300;
        matrixSlot = new GuiMatrixSlot(ScaleGui.getCenterX(slotX), ScaleGui.getCenterY(slotY), cellHeight * w, cellHeight * h, itemStack, null);
        slotY += 57 * h;
        float iconWidth = ScaleGui.get(130);
        float iconHeight = ScaleGui.get(129);
        matrixSlotSpecialPistol = new GuiMatrixSlotSpecial(ScaleGui.getCenterX(slotX), ScaleGui.getCenterY(slotY), iconWidth, iconHeight, itemStack, MatrixInventory.SlotType.ADD_WEAPON, null);
        matrixSlotSpecialPistol.setBg(TextureRegister.textureInvSlotSpecBg);
        matrixSlotSpecialPistol.setFrame(TextureRegister.textureInvSlotSpecFrame);
        matrixSlotSpecialPistol.setGradient(TextureRegister.textureInvSlotSpecGradient);
        matrixSlotSpecialPistol.setSlotTexture(TextureRegister.TEXTURE_INV_SLOT_PISTOL);
        slotY += 130;
        iconWidth = ScaleGui.get(326);
        iconHeight = ScaleGui.get(165);
        matrixSlotSpecialMain = new GuiMatrixSlotSpecial(ScaleGui.getCenterX(slotX), ScaleGui.getCenterY(slotY), iconWidth, iconHeight, itemStack, MatrixInventory.SlotType.MAIN_WEAPON, null);
        matrixSlotSpecialMain.setBg(TextureRegister.TEXTURE_INV_SLOT_WPN_BG);
        matrixSlotSpecialMain.setFrame(TextureRegister.TEXTURE_INV_SLOT_WPN_FRAME);
        matrixSlotSpecialMain.setGradient(TextureRegister.TEXTURE_INV_SLOT_WPN_GRADIENT);
        matrixSlotSpecialMain.setSlotTexture(TextureRegister.TEXTURE_INV_SLOT_MAIN);

        int id = 0;
        if(renderer != null) {
            for(RenderObject renderObject : renderer.getRenderObjects().values()) {
                if(y > 800) {
                    x += 260;
                    y = 25f;
                }

                addVector(id, renderObject.getPosition(), x, y, size, fieldWidth, renderObject.getName());
                id+=6;
                y+= size*1.5f;
                addVector(id, renderObject.getRotation(), x, y, size, fieldWidth, "");
                id+=6;
                y+= size*1.5f;
                addVector(id, renderObject.getScale(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 2.5f;
            }
        }

        if(gunRenderer != null) {
            if(mc.gameSettings.thirdPersonView == 0) {
                addVector(id, gunRenderer.getAllPosition(), x, y, size, fieldWidth, "расположение от 1 лица");
                id+=6;
                y+= size*1.5f;
                addVector(id, gunRenderer.getAllRotation(), x, y, size, fieldWidth, "");
                id+=6;
                y+= size*1.5f;
                addVector(id, gunRenderer.getAllScale(), x, y, size, fieldWidth, "");
            } else {
                addVector(id, gunRenderer.getEquippedPosition(), x, y, size, fieldWidth, "расположение от 3 лица");
                id+=6;
                y+= size*1.5f;
                addVector(id, gunRenderer.getEquippedRotation(), x, y, size, fieldWidth, "");
                id+=6;
                y+= size*1.5f;
                addVector(id, gunRenderer.getEquippedScale(), x, y, size, fieldWidth, "");
            }

            if(mc.gameSettings.thirdPersonView == 0) {
                id += 6;
                y += size * 2.5f;
                addVector(id, gunRenderer.getGunPosition(), x, y, size, fieldWidth, "оружие");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getGunRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getGunScale(), x, y, size, fieldWidth, "");

                id += 6;
                y += size * 2.5f;
                addVector(id, gunRenderer.getRightHandPosition(), x, y, size, fieldWidth, "правая рука");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getRightHandRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getRightHandScale(), x, y, size, fieldWidth, "");

                id += 6;
                y += size * 2.5f;
                addVector(id, gunRenderer.getLeftHandPosition(), x, y, size, fieldWidth, "левая рука");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getLeftHandRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getLeftHandScale(), x, y, size, fieldWidth, "");

                x += 260;
                y = 25;
                id += 6;
                addVector(id, gunRenderer.getInventoryPosition(), x, y, size, fieldWidth, "расположение в инвентаре");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getInventoryRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getInventoryScale(), x, y, size, fieldWidth, "");

                y += size * 2.5f;
                id += 6;
                addVector(id, gunRenderer.getEntityPosition(), x, y, size, fieldWidth, "расположение на земле");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getEntityRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getEntityScale(), x, y, size, fieldWidth, "");

                y += size * 2.5f;
                id += 6;
                addVector(id, gunRenderer.getInHudPosition(), x, y, size, fieldWidth, "расположение в худе");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getInHudRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getInHudScale(), x, y, size, fieldWidth, "");

                x += 260;
                y = 25;
                id += 6;
                addVector(id, gunRenderer.getMaxKnockAnimation(), x, y, size, fieldWidth, "макс отдача, только x");
                id += 6;
                y += size * 1.5f;
                addVector(id, gunRenderer.getMaxKnockRotation(), x, y, size, fieldWidth, "");

                id+=6;
                y+= size*2.5f;
                addVector(id, gunRenderer.getFlashPosition(), x, y, size, fieldWidth, "вспышка");
                id+=6;
                y+= size*1.5f;
                addVector(id, gunRenderer.getFlashRotationVector(), x, y, size, fieldWidth, "");
                id+=6;
                y+= size*1.5f;
                addVector(id, gunRenderer.getFlashScale(), x, y, size, fieldWidth, "");
                y+= size*1.5f;
                buttonList.add(new GuiButtonAdvanced(1007, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(250), ScaleGui.get(30),"show/hide", ScaleGui.get(2f)));
            } else {
                id+=6;
                y+= size*2.5f;
                addVector(id, gunRenderer.getFlash3rdPosition(), x, y, size, fieldWidth, "вспышка (поз и размер)");
                id+=6;
                y+= size*1.5f;
                addVector(id, gunRenderer.getFlash3rdRotation(), x, y, size, fieldWidth, "");
                id+=6;
                y+= size*1.5f;
                addVector(id, gunRenderer.getFlash3rdScale(), x, y, size, fieldWidth, "");
                y+= size*1.5f;
                buttonList.add(new GuiButtonAdvanced(1007, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(250), ScaleGui.get(30),"show/hide", ScaleGui.get(2f)));
            }
        } else if(meleeWeaponRenderer != null) {
            if(mc.gameSettings.thirdPersonView == 0) {
                addVector(id, meleeWeaponRenderer.getAllPosition(), x, y, size, fieldWidth, "расположение от 1 лица");
                id+=6;
                y+= size*1.5f;
                addVector(id, meleeWeaponRenderer.getAllRotation(), x, y, size, fieldWidth, "");
                id+=6;
                y+= size*1.5f;
                addVector(id, meleeWeaponRenderer.getAllScale(), x, y, size, fieldWidth, "");
            } else {
                addVector(id, meleeWeaponRenderer.getEquippedPosition(), x, y, size, fieldWidth, "расположение от 3 лица");
                id+=6;
                y+= size*1.5f;
                addVector(id, meleeWeaponRenderer.getEquippedRotation(), x, y, size, fieldWidth, "");
                id+=6;
                y+= size*1.5f;
                addVector(id, meleeWeaponRenderer.getEquippedScale(), x, y, size, fieldWidth, "");
            }

            if(mc.gameSettings.thirdPersonView == 0) {
                id += 6;
                y += size * 2.5f;
                addVector(id, meleeWeaponRenderer.getGunPosition(), x, y, size, fieldWidth, "оружие");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getGunRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getGunScale(), x, y, size, fieldWidth, "");

                id += 6;
                y += size * 2.5f;
                addVector(id, meleeWeaponRenderer.getRightHandPosition(), x, y, size, fieldWidth, "правая рука");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getRightHandRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getRightHandScale(), x, y, size, fieldWidth, "");

                id += 6;
                y += size * 2.5f;
                addVector(id, meleeWeaponRenderer.getLeftHandPosition(), x, y, size, fieldWidth, "левая рука");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getLeftHandRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getLeftHandScale(), x, y, size, fieldWidth, "");

                x += 260;
                y = 25;
                id += 6;
                addVector(id, meleeWeaponRenderer.getInventoryPosition(), x, y, size, fieldWidth, "расположение в инвентаре");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getInventoryRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getInventoryScale(), x, y, size, fieldWidth, "");

                y += size * 2.5f;
                id += 6;
                addVector(id, meleeWeaponRenderer.getInHudPosition(), x, y, size, fieldWidth, "расположение в худе");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getInHudRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getInHudScale(), x, y, size, fieldWidth, "");

                y += size * 2.5f;
                id += 6;
                addVector(id, meleeWeaponRenderer.getEntityPosition(), x, y, size, fieldWidth, "расположение на земле");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getEntityRotation(), x, y, size, fieldWidth, "");
                id += 6;
                y += size * 1.5f;
                addVector(id, meleeWeaponRenderer.getEntityScale(), x, y, size, fieldWidth, "");
            }
        }

    }

    private void addVector(int id, Vector3fConfigurable vector, float x, float y, float buttonSize, float fieldWidth, String info) {
        float textScale = ScaleGui.get(2f);
        float buttonHeight = buttonSize / 1.5f;
        vectorButtonIds.put(id, vector);
        vectorInfo.put(new Vector2f(x, y-buttonHeight), info);
        GuiTextFieldAdvanced field = new GuiTextFieldWithVector(FontType.FUTURA_PT_MEDIUM, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y+buttonHeight),  ScaleGui.get(fieldWidth), ScaleGui.get(buttonSize/1.25f), ScaleGui.get(2f), vector, 0);
        field.setText(decimalFormat.format(vector.x));
        textFields.add(field);
        buttonList.add(new GuiButtonAdvanced(id, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonSize), ScaleGui.get(buttonHeight),"-", textScale));
        buttonList.add(new GuiButtonAdvanced(++id, ScaleGui.getCenterX(x+buttonSize), ScaleGui.getCenterY(y), ScaleGui.get(buttonSize), ScaleGui.get(buttonHeight),"+", textScale));
        x += fieldWidth;
        field = new GuiTextFieldWithVector(FontType.FUTURA_PT_MEDIUM, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y+buttonHeight), ScaleGui.get(fieldWidth), ScaleGui.get(buttonSize/1.25f), ScaleGui.get(2f), vector, 1);
        field.setText(decimalFormat.format(vector.y));
        textFields.add(field);
        buttonList.add(new GuiButtonAdvanced(++id, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonSize), ScaleGui.get(buttonHeight),"-", textScale));
        buttonList.add(new GuiButtonAdvanced(++id, ScaleGui.getCenterX(x+buttonSize), ScaleGui.getCenterY(y), ScaleGui.get(buttonSize), ScaleGui.get(buttonHeight),"+", textScale));
        x += fieldWidth;
        field = new GuiTextFieldWithVector(FontType.FUTURA_PT_MEDIUM, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y+buttonHeight), ScaleGui.get(fieldWidth), ScaleGui.get(buttonSize/1.25f), ScaleGui.get(2f), vector, 2);
        field.setText(decimalFormat.format(vector.z));
        textFields.add(field);
        buttonList.add(new GuiButtonAdvanced(++id, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonSize), ScaleGui.get(buttonHeight),"-", textScale));
        buttonList.add(new GuiButtonAdvanced(++id, ScaleGui.getCenterX(x+buttonSize), ScaleGui.getCenterY(y), ScaleGui.get(buttonSize), ScaleGui.get(buttonHeight),"+", textScale));
    }

    private void disableStates() {
        loadAnims = false;
        unloadAnims = false;
        baseAnims = false;
        hitAnims = false;
        rightClickAnims = false;
        base = false;
        reloadAnims = false;
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(nodes.containsKey(b.id)) {
            AnimationNode animationNode = nodes.get(b.id);
            int id = animationNode.getId() - 1;
            if(id < 0) id = 0;
            animationNode.setId(id);
            initGui();
        } else if(nodes.containsKey(b.id-1)) {
            AnimationNode animationNode = nodes.get(b.id - 1);
            int id = animationNode.getId() + 1;
            if(id == nodes.size()) id = nodes.size() - 1;
            animationNode.setId(id);
            initGui();
        } else if(nodes.containsKey(b.id-2)) {
            AnimationNode animationNode = nodes.get(b.id - 2);
            int ordinal = animationNode.getType().ordinal() + 1;
            if(ordinal == EnumAnimationType.values().length) ordinal = 0;
            animationNode.setType(EnumAnimationType.values()[ordinal]);
            initGui();
        } else if(nodes.containsKey(b.id-3)) {
            AnimationNode animationNode = nodes.get(b.id - 3);
            int ordinal = animationNode.getType().ordinal() + 1;
            if(ordinal == EnumAnimationType.values().length) ordinal = 0;
            animationNode.setType(EnumAnimationType.values()[ordinal]);
            initGui();
        }

        try {
            Set<Integer> ids = vectorButtonIds.keySet();
            for (Integer id : ids) {
                if(b.id >= id && b.id < id + 6) {
                    Vector3fConfigurable vector = vectorButtonIds.get(id);
                    int finalid = b.id - id;
                    switch (finalid) {
                        case 0: vector.x -= translationValue; break;
                        case 1: vector.x += translationValue; break;
                        case 2: vector.y -= translationValue; break;
                        case 3: vector.y += translationValue; break;
                        case 4: vector.z -= translationValue; break;
                        case 5: vector.z += translationValue;
                    }
                    initGui();
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(b.id == 1000) {
            translationValue /= 10f;
            initGui();
        } else if(b.id == 1001) {
            translationValue *= 10f;
            initGui();
        } else if(b.id == 1002) {
            boolean nextValue = !base;
            disableStates();
            base = nextValue;
            initGui();
        } else if(b.id == 1003) {
            boolean nextValue = !baseAnims;
            disableStates();
            baseAnims = nextValue;
            initGui();
        } else if(b.id == 1004) {
            gunRenderer.setForcePlaySprint(!gunRenderer.isForcePlaySprint());
        } else if(b.id == 1005) {
            gunRenderer.setForcePlayAim(!gunRenderer.isForcePlayAim());
        } else if(b.id == 1006) {
            if(mc.gameSettings.thirdPersonView == 2) mc.gameSettings.thirdPersonView = 0;
            else mc.gameSettings.thirdPersonView = 2;
        } else if(b.id == 1007) {
            gunRenderer.setShowFlash(!gunRenderer.isShowFlash());
        } else if(b.id == 1008) {
            boolean nextValue = !loadAnims;
            disableStates();
            loadAnims = nextValue;
            initGui();
        } else if(b.id == 1009) {
            if(loadAnims) {
                if(gunRenderer.getReloadAnimationNodes().size() < 10)
                    gunRenderer.getReloadAnimationNodes().add(new AnimationNode(EnumAnimationType.Defualt, nodes.size()));
            } else if(unloadAnims) {
                if(gunRenderer.getUnloadAnimationNodes().size() < 10)
                    gunRenderer.getUnloadAnimationNodes().add(new AnimationNode(EnumAnimationType.Defualt, nodes.size()));
            } else if(hitAnims) {
                if(meleeWeaponRenderer.getHitAnimations().size() < 10)
                    meleeWeaponRenderer.getHitAnimations().add(new AnimationNode(EnumAnimationType.Defualt, nodes.size()));
            } else if(rightClickAnims) {
                if(meleeWeaponRenderer.getRightClickAnimations().size() < 10)
                    meleeWeaponRenderer.getRightClickAnimations().add(new AnimationNode(EnumAnimationType.Defualt, nodes.size()));
            } else if(reloadAnims) {
                if(gunRenderer.getFullReloadAnimationNodes().size() < 10)
                    gunRenderer.getFullReloadAnimationNodes().add(new AnimationNode(EnumAnimationType.Defualt, nodes.size()));
            }
            initGui();
        } else if(b.id == 1010) {
            if(loadAnims) {
                List<AnimationNode> animations = gunRenderer.getReloadAnimationNodes();
                if(animations.size() > 0) {
                    animations.remove(animations.size() - 1);
                }
            } else if(unloadAnims) {
                List<AnimationNode> animations = gunRenderer.getUnloadAnimationNodes();
                if(animations.size() > 0) {
                    animations.remove(animations.size() - 1);
                }
            } else if(hitAnims) {
                List<AnimationNode> animations = meleeWeaponRenderer.getHitAnimations();
                if(animations.size() > 0) {
                    animations.remove(animations.size() - 1);
                }
            } else if(rightClickAnims) {
                List<AnimationNode> animations = meleeWeaponRenderer.getRightClickAnimations();
                if(animations.size() > 0) {
                    animations.remove(animations.size() - 1);
                }
            } else if(reloadAnims) {
                List<AnimationNode> animations = gunRenderer.getFullReloadAnimationNodes();
                if(animations.size() > 0) {
                    animations.remove(animations.size() - 1);
                }
            }
            initGui();
        } else if(b.id == 1011) {
            if(loadAnims) gunRenderer.isPlayLoadAnim = !gunRenderer.isPlayLoadAnim;
            else if(unloadAnims) gunRenderer.isPlayUnloadAnim = !gunRenderer.isPlayUnloadAnim;
            else if(hitAnims) meleeWeaponRenderer.setPlayHitAnimation(!meleeWeaponRenderer.isPlayHitAnimation());
            else if(rightClickAnims) meleeWeaponRenderer.setPlayUseAnimation(!meleeWeaponRenderer.isPlayUseAnimation());
            else if(reloadAnims) gunRenderer.isPlayReloadAnim = !gunRenderer.isPlayReloadAnim;
        } else if(b.id == 1012) {
            if(gunRenderer != null) {
                for (AnimationNode animation : gunRenderer.getReloadAnimationNodes()) {
                    Vector3f animation1 = animation.getAnimation();
                    animation1.x = animation1.y = animation1.z = 0f;
                }
                for (AnimationNode animation : gunRenderer.getUnloadAnimationNodes()) {
                    Vector3f animation1 = animation.getAnimation();
                    animation1.x = animation1.y = animation1.z = 0f;
                }
                for (AnimationNode animation : gunRenderer.getFullReloadAnimationNodes()) {
                    Vector3f animation1 = animation.getAnimation();
                    animation1.x = animation1.y = animation1.z = 0f;
                }
                gunRenderer.clearIsAnims();
            } else if(meleeWeaponRenderer != null) {
                for (AnimationNode animation : meleeWeaponRenderer.getRightClickAnimations()) {
                    Vector3f animation1 = animation.getAnimation();
                    animation1.x = animation1.y = animation1.z = 0f;
                }
                for (AnimationNode animation : meleeWeaponRenderer.getHitAnimations()) {
                    Vector3f animation1 = animation.getAnimation();
                    animation1.x = animation1.y = animation1.z = 0f;
                }
                meleeWeaponRenderer.clearIsAnims();
            }

        } else if(b.id == 1013) {
            boolean nextValue = !unloadAnims;
            disableStates();
            unloadAnims = nextValue;
            initGui();
        } else if(b.id == 1014) {
            boolean nextValue = !hitAnims;
            disableStates();
            hitAnims = nextValue;
            initGui();
        } else if(b.id == 1015) {
            boolean nextValue = !rightClickAnims;
            disableStates();
            rightClickAnims = nextValue;
            initGui();
        } else if(b.id == 1016) {
            saveConfig();
        } else if(b.id == 1017) {
            loadConfig();
            initGui();
        } else if(b.id == 1018) {
            boolean nextValue = !reloadAnims;
            disableStates();
            reloadAnims = nextValue;
            initGui();
        }
    }

    private void saveConfig() {
        if(gunRenderer != null) {
            gunRenderer.save();
        } else if(meleeWeaponRenderer != null) {
            meleeWeaponRenderer.save();
        } else if(renderer != null) {
            renderer.save();
        }
    }

    private void loadConfig() {
        if(gunRenderer != null) {
            gunRenderer.init();
        } else if(meleeWeaponRenderer != null) {
            meleeWeaponRenderer.init();
        } else if(renderer != null) {
            renderer.init();
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);

        vectorInfo.forEach((vector2f, s) -> {
            GuiDrawUtils.drawStringCenter(FontType.FUTURA_PT_MEDIUM, s, vector2f.x, vector2f.y, 2f, 0xffffff);
        });

        GL11.glColor3f(0.1f, 0.1f, 0.1f);
        drawButtons(mouseX, mouseY, partialTick);
        GL11.glColor3f(1f, 1f, 1f);
        for(GuiTextField textField : textFields) {
            textField.drawTextBox();
        }

        float x = 1760;
        float y = 50;
        int id = 0;
        int key = 0;
        long animTime = 0;
        long totalAnimTime = 0;
        if(gunRenderer != null) {
            totalAnimTime = gunRenderer.getTotalAnimationTime();
            animTime = gunRenderer.getStartAnimationTime();
        } else if(meleeWeaponRenderer != null) {
            totalAnimTime = meleeWeaponRenderer.getTotalAnimationTime();
            animTime = meleeWeaponRenderer.getStartAnimationTime();
        } else {
            animTime = renderer.getAnimationTime();
        }

        if(animTime == 0) {
            animTime = totalAnimTime;
        } else {
            animTime = System.currentTimeMillis() - animTime;
        }

        GuiDrawUtils.drawStringCenter(FontType.FUTURA_PT_MEDIUM, "time " + animTime + "ms", x, y - 20, 1.5f, 0xffffff);

        for (int key1 : nodes.keySet()) {
            Vector3f a = nodes.get(key).getAnimation();
            GuiDrawUtils.drawStringCenter(FontType.FUTURA_PT_MEDIUM, "vec" + id + " " + decimalFormat.format(a.x) + ", " + decimalFormat.format(a.y) + ", " + decimalFormat.format(a.z), x, y, 1.3f, 0xffffff);
            y+=20;
            id++;
            key +=58;
        }

        if(base) {
            if(matrixSlot != null) matrixSlot.render(mouseX, mouseY, true);
            if(matrixSlotSpecialMain != null) matrixSlotSpecialMain.render(mouseX, mouseY, true);
            if(matrixSlotSpecialPistol != null) matrixSlotSpecialPistol.render(mouseX, mouseY, true);
        }
    }

    @Override
    public void updateScreen() {
        for(GuiTextField textField : textFields) {
            textField.updateCursorCounter();
        }
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (mouseButton == 0) {
            for (int l = 0; l < this.buttonList.size(); ++l) {
                GuiButton guibutton = this.buttonList.get(l);

                if (guibutton.mousePressed(this.mc, mouseX, mouseY)) {
                    GuiScreenEvent.ActionPerformedEvent.Pre event = new GuiScreenEvent.ActionPerformedEvent.Pre(this, guibutton, this.buttonList);
                    if (MinecraftForge.EVENT_BUS.post(event))
                        break;
                    this.selectedButton = event.button;
                    event.button.playClickSound(this.mc.getSoundHandler());
                    this.actionPerformed(event.button);
                    if (this.equals(this.mc.currentScreen))
                        MinecraftForge.EVENT_BUS.post(new GuiScreenEvent.ActionPerformedEvent.Post(this, event.button, this.buttonList));
                    return;
                }
            }
        }
        for(GuiTextField textField : textFields) {
            textField.mouseClicked(mouseX, mouseY, mouseButton);
        }
    }

    @Override
    public void keyTyped(char character, int key) {
        super.keyTyped(character, key);
        for(GuiTextField textField : textFields) {
            textField.textboxKeyTyped(character, key);
        }
    }
}
