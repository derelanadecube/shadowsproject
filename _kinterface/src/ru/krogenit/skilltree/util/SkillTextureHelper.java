package ru.krogenit.skilltree.util;

import ru.xlv.core.skill.Skill;

public class SkillTextureHelper {
    public static String getSkillTextureName(Skill skill) {
        switch (skill.getId()) {
            case 0:
            case 1:
            case 2:
            case 3:
                return "g_29";
            case 4:
            case 5:
            case 6:
            case 7:
                return "b_07";
            case 8:
            case 9:
            case 10:
            case 11:
                return "b_19";
            case 12:
            case 13:
            case 14:
            case 15:
                return "g_22";
            case 16:
            case 17:
            case 18:
            case 19:
                return "p_04";
            case 20:
            case 21:
            case 22:
                return "b_31";
            case 24:
            case 25:
            case 26:
            case 27:
                return "r_47";
            case 28:
            case 29:
            case 30:
            case 31:
                return "g_21";
            case 32:
            case 33:
            case 34:
            case 35:
                return "r_28";
            case 36:
            case 37:
            case 38:
                return "b_12";
            case 39:
            case 40:
            case 41:
            case 42:
                return "b_37";
            case 43:
            case 44:
            case 45:
            case 46:
                return "g_12";
            case 47:
            case 48:
            case 49:
            case 50:
                return "p_05";
            case 51:
            case 52:
            case 53:
            case 54:
                return "b_14";
            case 55:
            case 56:
            case 57:
                return "b_16";
            case 58:
            case 59:
            case 60:
            case 61:
                return "b_21";
            case 62:
            case 63:
            case 64:
            case 65:
                return "r_17";
            default: return "xlv go csgo";
        }
    }
}
