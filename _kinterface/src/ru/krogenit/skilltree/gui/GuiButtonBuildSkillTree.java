package ru.krogenit.skilltree.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.smart.moving.SmartMovingContext;
import net.smart.moving.SmartMovingFactory;
import net.smart.moving.SmartMovingSelf;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.skilltree.type.EnumButtonBuildType;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

public class GuiButtonBuildSkillTree extends GuiButton {

    private EnumButtonBuildType type;
    private float animation;
    private float totalHeight;
    private final GuiSkillTree guiSkillTree;

    private boolean hovered;

    public GuiButtonBuildSkillTree(int id, float xPos, float yPos, float width, float height, String text, EnumButtonBuildType type, GuiSkillTree guiSkillTree) {
        super(id, xPos, yPos, width, height, text);
        this.type = type;
        this.guiSkillTree = guiSkillTree;
    }

    public void drawTooltip(Minecraft mc, int mouseX, int mouseY) {
        if (type != EnumButtonBuildType.LOCKED) {
            if (isHovered(mouseX, mouseY)) {
                float mouseOffset = -mc.displayHeight / 2.7f;
                float x = xPosition + (-xPosition + mouseX) + mouseOffset;
                float y = yPosition + (-yPosition + mouseY) + height / 2f;
                float startY = y;
                if (totalHeight == 0)
                    totalHeight = ScaleGui.get(705f);
                float width = ScaleGui.get(420f);

                GuiDrawUtils.renderTooltipBuild(x, y, x + width, y + totalHeight, mc.displayHeight / 28f);

                float fs = 1.35f;
                x += ScaleGui.get(21.6f);
                y += ScaleGui.get(49f);
                GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "НАЗВАНИЕ БИЛДА №" + (id + 1), x, y, fs, 0xffffff);
                y += ScaleGui.get(22.5f);
                GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x, y, ScaleGui.get(270f), 1);
                y += ScaleGui.get(22.5f);
                float iconWidth = ScaleGui.get(38.5f);
                float iconHeight = ScaleGui.get(38.5f);

                fs = 1.2f;
                int[] skills = XlvsMainMod.INSTANCE.getClientMainPlayer().getActiveSkills().get(id);
                for (int skillId : skills) {
                    GuiSkill guiSkill = guiSkillTree.getGuiSkillById(skillId);
                    if (guiSkill != null) {
                        GuiDrawUtils.drawRect(guiSkill.getSkillLearnedTexture(), x, y, iconWidth, iconHeight);
                        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, guiSkill.getName(), x + iconWidth + ScaleGui.get(18f), y + ScaleGui.get(18f), fs, 0xffffff);
                    }
                    y += iconHeight + ScaleGui.get(22.5f);
                }

                GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x, y, ScaleGui.get(270f), 1);
                y += ScaleGui.get(22.5f);
                iconWidth = ScaleGui.get(31.76f);
                iconHeight = ScaleGui.get(33.75f);
                skills = XlvsMainMod.INSTANCE.getClientMainPlayer().getPassiveSkills().get(id);
                for (int skillId : skills) {
                    GuiSkill guiSkill = guiSkillTree.getGuiSkillById(skillId);
                    if (guiSkill != null) {
                        GuiDrawUtils.drawRect(guiSkill.getSkillLearnedTexture(), x, y, iconWidth, iconHeight);
                        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, guiSkill.getName(), x + iconWidth + ScaleGui.get(18f), y + ScaleGui.get(18f), fs, 0xffffff);
                    }
                    y += iconHeight + ScaleGui.get(22.5f);
                }
                GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x, y, ScaleGui.get(270f), 1);
                y += ScaleGui.get(22.5f);
                iconWidth = ScaleGui.get(18f);
                iconHeight = ScaleGui.get(30.8f);
                GuiDrawUtils.drawRect(TextureRegister.textureSkillIconLeftMouse, x, y, iconWidth, iconHeight);
                y += ScaleGui.get(13.5f);
                x += iconWidth + ScaleGui.get(15.8f);
                fs = 1.08f;
                GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, "ИСПОЛЬЗОВАТЬ НАБОР НАВЫКОВ", x, y, fs, 0x666666);
                y += ScaleGui.get(60f);
                totalHeight = y - startY;

                GL11.glColor4f(1f, 1f, 1f, 1f);
                x += ScaleGui.get(270f);
                y = startY + ScaleGui.get(98f);
                iconWidth = ScaleGui.get(29f);
                iconHeight = ScaleGui.get(29f);
                GuiDrawUtils.drawRect(TextureRegister.textureSkillIconStatHp, x, y, iconWidth, iconHeight);
                y += iconHeight * 1.1f;
                GuiDrawUtils.drawRect(TextureRegister.textureSkillIconStatMana, x, y, iconWidth, iconHeight);
                y += iconHeight * 1.1f;
                GuiDrawUtils.drawRect(TextureRegister.textureSkillIconStatStamina, x, y, iconWidth, iconHeight);
                y += iconHeight * 1.1f;
                GuiDrawUtils.drawRect(TextureRegister.textureSkillIconStatArmor, x, y, iconWidth, iconHeight);
                y += iconHeight * 1.1f;
                GuiDrawUtils.drawRect(TextureRegister.textureSkillIconStatCutDefence, x, y, iconWidth, iconHeight);
                y += iconHeight * 2f;

                GuiDrawUtils.drawRect(TextureRegister.textureInvIconEnergy, x, y, iconWidth, iconHeight);
                y += iconHeight * 1.1f;
                GuiDrawUtils.drawRect(TextureRegister.textureInvIconHeat, x, y, iconWidth, iconHeight);
                y += iconHeight * 1.1f;
                GuiDrawUtils.drawRect(TextureRegister.textureInvIconFire, x, y, iconWidth, iconHeight);
                y += iconHeight * 1.1f;
                GuiDrawUtils.drawRect(TextureRegister.textureInvIconElectricity, x, y, iconWidth, iconHeight);
                y += iconHeight * 1.1f;
                GuiDrawUtils.drawRect(TextureRegister.textureInvIconToxins, x, y, iconWidth, iconHeight);
                y += iconHeight * 1.1f;
                GuiDrawUtils.drawRect(TextureRegister.textureInvIconRadiation, x, y, iconWidth, iconHeight);
                y += iconHeight * 1.1f;
                GuiDrawUtils.drawRect(TextureRegister.textureInvIconExplosion, x, y, iconWidth, iconHeight);

//                y += iconHeight * 2f;
//                GuiDrawUtils.drawRect(new ResourceLocation("inventory", "icon_stat_radiation1.png"), x, y, iconWidth, iconHeight);
//                y += iconHeight * 1.1f;
//                GuiDrawUtils.drawRect(new ResourceLocation("inventory", "icon_stat_burning.png"), x, y, iconWidth, iconHeight);
//                y += iconHeight * 1.1f;
//                GuiDrawUtils.drawRect(new ResourceLocation("inventory", "icon_stat_poisoned.png"), x, y, iconWidth, iconHeight);
//                y += iconHeight * 1.1f;
//                GuiDrawUtils.drawRect(new ResourceLocation("inventory", "icon_stat_bleeding.png"), x, y, iconWidth, iconHeight);

                float ballisticArmorValue = 0;
                float cutProtectionValue = 0;
                float energyProtection = 0;
                float thermalProtection = 0;
                float fireProtection = 0;
                float electricProtection = 0;
                float toxicProtection = 0;
                float radiationProtection = 0;
                float explosionProtection = 0;
                for (int v = MatrixInventory.SlotType.HEAD.ordinal(); v <= MatrixInventory.SlotType.FEET.ordinal(); v++) {
                    MatrixInventory.SlotType slotType = MatrixInventory.SlotType.values()[v];
                    ItemStack itemStack = mc.thePlayer.inventory.getStackInSlot(slotType.getAssociatedSlotIndex());
                    if (itemStack != null) {
                        Item item = itemStack.getItem();
                        if (item instanceof ItemArmor) {
                            ItemArmor itemArmor = (ItemArmor) item;
                            ballisticArmorValue += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.BALLISTIC_PROTECTION);
                            cutProtectionValue += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.CUT_PROTECTION);
                            energyProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.ENERGY_PROTECTION);
                            thermalProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.THERMAL_PROTECTION);
                            fireProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.FIRE_PROTECTION);
                            electricProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.ELECT_PROTECTION);
                            toxicProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.TOXIC_PROTECTION);
                            radiationProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.RADIATION_PROTECTION);
                            explosionProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.EXPLOSION_PROTECTION);
                        }
                    }
                }
                fs = 1.08f;
                x += iconWidth * 1.4f;
                y = startY + ScaleGui.get(12.5f) + ScaleGui.get(98.18f);
                for (int i = 0; i < 12; i++) {
                    String value = "";
                    switch (i) {
                        case 0:
                            value = "" + (int) mc.thePlayer.getHealth();
                            break;
                        case 1:
                            value = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.MANA).getValue();
                            break;
                        case 2:
                            SmartMovingSelf moving = (SmartMovingSelf) SmartMovingFactory.getInstance(mc.thePlayer);
                            float maxExhaustion = SmartMovingContext.Client.getMaximumExhaustion();
                            float fitness = maxExhaustion - Math.min(moving.exhaustion, maxExhaustion);

                            float stamina = fitness / 100f;
                            value = "" + (int) (stamina * 100);
                            break;
                        case 3:
                            value = "" + (int) ballisticArmorValue;
                            break;
                        case 4:
                            value = "" + (int) cutProtectionValue;
                            break;
                        case 5:
                            value = "" + (int) energyProtection;
                            break;
                        case 6:
                            value = "" + (int) thermalProtection;
                            break;
                        case 7:
                            value = "" + (int) fireProtection;
                            break;
                        case 8:
                            value = "" + (int) electricProtection;
                            break;
                        case 9:
                            value = "" + (int) toxicProtection;
                            break;
                        case 10:
                            value = "" + (int) radiationProtection;
                            break;
                        case 11:
                            value = "" + (int) explosionProtection;
                            break;
                    }
                    GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, value, x, y, fs, 0xffffff);
                    if (i == 4 || i == 11) {
                        y += iconHeight * 2f;
                    } else {
                        y += iconHeight * 1.1f;
                    }
                }
            }
        }
    }

    public void drawButtonEffect(int mouseX, int mouseY) {
        if (type != EnumButtonBuildType.LOCKED) {
            if (isHovered(mouseX, mouseY)) {
                animation += AnimationHelper.getAnimationSpeed() * 0.005f;
                if (animation > 1f) {
                    animation -= 1f;
                }

                float off = height / 6f;
                float x = xPosition - off;
                float y = yPosition - off;
                float width = this.width + off * 2;
                float height = this.height + off * 2;
                GL11.glColor4f(1f, 1f, 1f, 1f);
                GuiDrawUtils.drawMaskingButtonEffect(TextureRegister.textureSkillIconSkillMask, x, y, width, height, x - width / 2f, y + height / 4f, width * 2f, height / 2f, animation * 360f);
            }
        }
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        boolean hovered = isHovered(mouseX, mouseY);
        if(hovered && !this.hovered) {
            SoundUtils.playGuiSound(SoundType.BUTTON_HOVER);
        }
        this.hovered = hovered;
        if (type == EnumButtonBuildType.LOCKED) {
            Utils.bindTexture(TextureRegister.textureSkillIconSkillLock);
        } else if (type == EnumButtonBuildType.AVAILABLE) {
            Utils.bindTexture(TextureRegister.textureSkillIconSkillEmpty);
        } else {
            Utils.bindTexture(TextureRegister.textureSkillIconSkillActive);
        }
        GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);
        if (type != EnumButtonBuildType.LOCKED) drawText();
    }

    protected void drawText() {
        int j = 14737632;

        if (packedFGColour != 0) {
            j = packedFGColour;
        } else if (!this.enabled) {
            j = 10526880;
        } else if (this.field_146123_n) {
            j = 16777120;
        }

        float fs = 2f;
        float fsScaled = ScaleGui.get(fs);
        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.Marske, displayString, this.xPosition + this.width / 2.0f,
                this.yPosition + (this.height) / 2.0f - FontType.Marske.getFontContainer().height() / 2.75f * fsScaled, fs, j);
    }

    private boolean isHovered(int mouseX, int mouseY) {
        return this.field_146123_n = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
    }

    public EnumButtonBuildType getType() {
        return type;
    }

    public void setType(EnumButtonBuildType type) {
        this.type = type;
    }
}
