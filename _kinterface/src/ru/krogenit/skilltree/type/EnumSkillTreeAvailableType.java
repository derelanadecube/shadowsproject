package ru.krogenit.skilltree.type;

public enum EnumSkillTreeAvailableType {
    LOCKED, AVAILABLE, LEARNED;
}
