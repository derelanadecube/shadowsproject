package ru.xlv.auction.handle;

import lombok.Getter;
import ru.xlv.auction.common.lot.SimpleAuctionLot;
import ru.xlv.auction.network.PacketAuctionLotsSync;
import ru.xlv.auction.network.PacketAuctionMyList;
import ru.xlv.auction.network.PacketAuctionSearch;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.util.SyncResultHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AuctionHandler {

    @Getter
    private final List<SimpleAuctionLot> auctionLots = Collections.synchronizedList(new ArrayList<>());

    @Getter
    private final List<SimpleAuctionLot> ownAuctionList = Collections.synchronizedList(new ArrayList<>());

    public SyncResultHandler<PacketAuctionMyList.Result> syncMyAuctionLots() {
        return XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketAuctionMyList());
    }

    public SyncResultHandler<PacketAuctionLotsSync.Result> syncAllAuctionLots() {
        return XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketAuctionLotsSync());
    }

    public SyncResultHandler<PacketAuctionSearch.Result> searchLots(String request) {
        return XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketAuctionSearch(request));
    }

    //todo примеры использования
/*    {
        syncAllAuctionLots().thenAcceptSync(result -> {
            if (result.isSuccess()) {
                auctionLots.clear();
                auctionLots.addAll(result.getAuctionLots());
            } else {
                String responseMessage = result.getResponseMessage();
                // ошибка
            }
        });
        syncMyAuctionLots().thenAcceptSync(result -> {
            if (result.isSuccess()) {
                auctionLots.clear();
                auctionLots.addAll(result.getAuctionLots());
            } else {
                String responseMessage = result.getResponseMessage();
                // ошибка
            }
        });
    }*/
}
