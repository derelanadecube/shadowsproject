package ru.xlv.auction;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.xlv.auction.handle.AuctionHandler;
import ru.xlv.auction.network.*;
import ru.xlv.core.XlvsCore;

import static ru.xlv.auction.XlvsAuctionMod.MODID;

@Mod(
        name = "XlvsAuctionMod",
        modid = MODID,
        version = "1.0"
)
public class XlvsAuctionMod {

    public static final String MODID = "xlvsauc";

    @Mod.Instance(MODID)
    public static XlvsAuctionMod INSTANCE;

    @Getter
    private final AuctionHandler auctionHandler = new AuctionHandler();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketAuctionBet(),
                new PacketAuctionMyList(),
                new PacketAuctionLotsSync(),
                new PacketAuctionPlace(),
                new PacketAuctionSearch()
        );
    }
}
