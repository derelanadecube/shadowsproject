package ru.xlv.auction.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.auction.XlvsAuctionMod;
import ru.xlv.auction.handle.lot.AuctionLot;
import ru.xlv.auction.common.util.AuctionUtils;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketAuctionSearch implements IPacketCallbackOnServer {

    private List<AuctionLot> result;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String searchRequest = bbis.readUTF();
        if(searchRequest.isEmpty()) {
            return;
        }
        XlvsAuctionMod.INSTANCE.getAuctionHandler().searchLot(searchRequest).thenAccept(auctionLots -> {
            if (auctionLots != null && !auctionLots.isEmpty()) {
                result = auctionLots;
                packetCallbackSender.send();
            }
        });
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(result != null);
        if(result != null) {
            bbos.writeInt(result.size());
            for (AuctionLot auctionLot : result) {
                AuctionUtils.writeAuctionLot(auctionLot, bbos);
            }
        }
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
