package ru.xlv.group.handle.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.group.XlvsGroupMod;

@Getter
@RequiredArgsConstructor
public enum GroupChangeLeaderResult {

    SUCCESS(XlvsGroupMod.INSTANCE.getLocalization().responseSetLeaderSuccessMessage),
    PLAYER_NOT_FOUND(XlvsGroupMod.INSTANCE.getLocalization().responseSetLeaderPlayerNotFoundMessage),
    PLAYER_ALREADY_LEADER(XlvsGroupMod.INSTANCE.getLocalization().responseSetLeaderPlayerAlreadyLeaderMessage),
    UNKNOWN(XlvsGroupMod.INSTANCE.getLocalization().responseSetLeaderUnknownMessage);

    private final String responseMessage;
}
