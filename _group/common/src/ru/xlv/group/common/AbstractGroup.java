package ru.xlv.group.common;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class AbstractGroup<K> {

    private final List<GroupInvite<K>> invites = new ArrayList<>();
    private final List<K> players = new ArrayList<>();

    @Setter
    private K leader;

    public AbstractGroup(K leader) {
        this.leader = leader;
        players.add(leader);
    }
}
