package ru.xlv.group.common;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class Group extends AbstractGroup<String> {

    public Group(String leader) {
        super(leader);
    }
}
