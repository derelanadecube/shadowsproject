//Fragment Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 330 core
out vec4 outputColor;

#define DIRECTIONAL_LIGHTS 1

uniform sampler2D gColor;
uniform sampler2D gLightMap;
uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gEmission;

uniform sampler2D lightmap;

uniform bool usePointLights;

in vec2 texCoords;
in vec3 dirPos;

const float PI = 3.14159265359;

uniform struct DirLight {
	vec3 dir;
	vec3 color;
	float specular;
} directLight[DIRECTIONAL_LIGHTS];

float DistributionGGX(vec3 N, vec3 H, float roughness) {
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness) {
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}
vec3 fresnelSchlick(float cosTheta, vec3 F0) {
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  

void main() {
	vec4 position = texture(gPosition, texCoords);
	vec3 pos = position.xyz;
	vec2 lightMapCoords = texture(gLightMap, texCoords).xy;
	vec3 colorLightmap = texture(lightmap, lightMapCoords).xyz;
	vec4 diffuse = texture(gColor, texCoords);
	vec3 emission = texture(gEmission, texCoords).rgb;
	
	if(usePointLights) {
		vec3 colorLightmapTest = texture(lightmap, vec2(0.05, lightMapCoords.y)).xyz;
		vec3 albedo = diffuse.rgb;
	
		vec4 gNormalIn = texture(gNormal, texCoords);
		vec3 N = normalize(gNormalIn.xyz);
		
		vec3 V = normalize(-pos);
		vec3 F0 = vec3(0.04); 
		float metallic = clamp(position.w, 0.001, 0.999);
		float roughness = clamp(1.0 - gNormalIn.w, 0.2, 0.999);
		float ao = 0.99;

		
		F0 = mix(F0, albedo, metallic);
		vec3 L = normalize(dirPos);
		vec3 H = normalize(V + L);
		vec3 radiance     = directLight[0].color;     
		float NDF = DistributionGGX(N, H, roughness);        
		float G   = GeometrySmith(N, V, L, roughness);      
		vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);   
		vec3 kS = F;
		vec3 kD = vec3(1.0) - kS;
		kD *= 1.0 - metallic;	  
		vec3 numerator    = NDF * G * F;
		float NdotL = max(dot(N, L), 0.0);    
		float denominator = 1.0 * max(dot(N, V), 0.0) * NdotL;
		vec3 specular     = numerator / max(denominator, 0.001) * directLight[0].specular;  

		vec3 Lo = (kD * albedo / PI + specular) * radiance * NdotL; 
		vec3 ambient = vec3(0.03) * albedo * ao;
		vec3 color = ambient + Lo;
	   
		outputColor = vec4(diffuse.rgb * colorLightmap + color * colorLightmapTest + emission, diffuse.w);
	} else {
		outputColor = vec4(diffuse.rgb * colorLightmap + emission, diffuse.w);
	}
}