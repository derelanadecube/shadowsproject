//gBuffer shader
//Krogenit 
#version 330 core
layout (location = 0) out vec4 gAlbedoSpec;
layout (location = 1) out vec4 gNormal;
layout (location = 2) out vec4 gPosition;
layout (location = 3) out vec4 gLightmap;
layout (location = 4) out vec3 gEmission;

in vec2 texCoords;
in vec2 lightMapCoords;
in vec3 normal;
in vec3 position;
in vec4 color;
in vec3 tangent;

uniform sampler2D texture_diffuse;
uniform sampler2D texture_emission;
uniform float emissionPower;
uniform sampler2D texture_normal;
uniform sampler2D texture_specular;
uniform sampler2D texture_gloss;
uniform sampler2D lightMap;

uniform bool useNormalMapping;
uniform bool useSpecularMapping;
uniform bool useEmissionMapping;
uniform bool useGlossMapping;

uniform bool useLightMap;
uniform bool useTexture;

void main()
{
	if(useLightMap) {
		//gAlbedoSpec = texture(lightMap, lightMapCoords);
		gLightmap.xy = lightMapCoords;
	} else {
		//gAlbedoSpec = vec4(1.0, 1.0, 1.0, 1.0);
		gLightmap.xy = vec2(1.0, 1.0);
	}
	
	if(useSpecularMapping) {
		gPosition.w = texture(texture_specular, texCoords).r;
	} else {
		gPosition.w = 0.01;
	}
	
	gPosition.xyz = position;
	
	if(useTexture) {
		gAlbedoSpec = texture(texture_diffuse, texCoords) * color;
	} else { 
		gAlbedoSpec = color;
	}
	
	if(useEmissionMapping) {
		gEmission = texture(texture_emission, texCoords).rgb * emissionPower;
	}
	
	if(useNormalMapping) {
		vec3 normalTex = texture(texture_normal, texCoords).rgb;
		vec3 normalMapValue = 2.0 * normalTex - 1.0;
		vec3 bitang = cross(normal, tangent);
		vec3 unitNormal = normalize(normalMapValue);
			
		mat3 toTangentSpace = mat3(
			tangent, bitang, normal
		);
				
		gNormal.xyz = normalize(toTangentSpace * unitNormal);
	} else {
		gNormal.xyz = normal;
	}
	
	if(useGlossMapping) {
		gNormal.w = texture(texture_gloss, texCoords).r;
	} else {
		gNormal.w = 0.1;
	}
}