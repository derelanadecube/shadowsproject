//Vertex Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 120

#define DIRECTIONAL_LIGHTS 1

attribute vec3 in_position;
attribute vec2 in_textureCoords;
attribute vec3 in_normal;
attribute vec3 in_tangent;

varying vec2 texCoords;
varying vec2 lightMapCoords;
varying vec4 color;
varying vec3 position;
varying vec3 normal;
varying vec3 tangent;
varying vec3 delta;

uniform mat4 modelView;

uniform struct DirLight {
	vec3 dir;
	vec3 color;
	float specular;
} directLight[DIRECTIONAL_LIGHTS];

void main() {	
	vec4 pos = gl_ModelViewMatrix * gl_Vertex;
	color = gl_Color;
	normal = gl_NormalMatrix * gl_Normal;
	texCoords = gl_MultiTexCoord0.xy;
	position = pos.xyz;
	gl_Position = gl_ProjectionMatrix * pos;
	tangent = gl_NormalMatrix * in_tangent;
	
	lightMapCoords = (gl_TextureMatrix[1] * gl_MultiTexCoord1).xy;
	delta = (modelView * vec4(directLight[0].dir, 1.0)).xyz;
}