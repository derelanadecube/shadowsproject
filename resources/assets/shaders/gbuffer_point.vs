//Krogenit 
#version 120

#define POINT_LIGHTS 1

varying vec2 texCoords;
varying vec3 lightPos;

uniform mat4 modelView;

uniform struct PointLight {
   vec3 position;
   float attenuation;
   vec3 color;
   float specular;
} pointLights[POINT_LIGHTS];

void main() 
{	
	lightPos = (modelView * vec4(pointLights[0].position, 1.0)).xyz;
	texCoords = gl_MultiTexCoord0.xy;
	gl_Position = gl_Vertex;
}