package ru.xlv.cases.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.item.ItemStack;
import ru.xlv.cases.pojo.Case;
import ru.xlv.cases.pojo.CaseItem;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class PacketCaseListSync implements IPacketCallbackEffective<List<Case>> {

    private List<Case> result;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {}

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result = new ArrayList<>();
        int c = bbis.readInt();
        for (int i = 0; i < c; i++) {
            String name = bbis.readUTF();
            int price = bbis.readInt();
            String imageUrl = bbis.readUTF();
            Case aCase = new Case(name, price, imageUrl);
            int c1 = bbis.readInt();
            for (int j = 0; j < c1; j++) {
                ItemStack itemStack = ByteBufUtils.readItemStack(bbis.getBuffer());
                int chance = bbis.readInt();
                aCase.getItems().add(new CaseItem(itemStack, chance));
            }
            result.add(aCase);
        }
    }

    @Nullable
    @Override
    public List<Case> getResult() {
        return result;
    }
}
