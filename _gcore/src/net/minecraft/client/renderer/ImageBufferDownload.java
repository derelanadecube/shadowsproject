package net.minecraft.client.renderer;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

@SideOnly(Side.CLIENT)
public class ImageBufferDownload implements IImageBuffer
{
    private int[] imageData;
    private int imageWidth;
    private int imageHeight;

    public BufferedImage parseUserSkin(BufferedImage par1BufferedImage)
    {
        if (par1BufferedImage == null)
        {
            return null;
        }
        else
        {
            this.imageWidth = 64;
            this.imageHeight = 32;
            int srcWidth = par1BufferedImage.getWidth();
            int srcHeight = par1BufferedImage.getHeight();

            if (srcWidth != 64 || srcHeight != 32 && srcHeight != 64)
            {
                while (this.imageWidth < srcWidth || this.imageHeight < srcHeight)
                {
                    this.imageWidth *= 2;
                    this.imageHeight *= 2;
                }
            }

            BufferedImage bufferedimage = new BufferedImage(this.imageWidth, this.imageHeight, 2);
            Graphics g = bufferedimage.getGraphics();
            g.drawImage(par1BufferedImage, 0, 0, null);
            g.dispose();
            this.imageData = ((DataBufferInt)bufferedimage.getRaster().getDataBuffer()).getData();
            int w = this.imageWidth;
            int h = this.imageHeight;
            this.setAreaOpaque(0, 0, w / 2, h / 2);
            this.setAreaTransparent(w / 2, 0, w, h);
            this.setAreaOpaque(0, h / 2, w, h);
            return bufferedimage;
        }
    }

    public void func_152634_a() {}

    /**
     * Makes the given area of the image transparent if it was previously completely opaque (used to remove the outer
     * layer of a skin around the head if it was saved all opaque; this would be redundant so it's assumed that the skin
     * maker is just using an image editor without an alpha channel)
     */
    private void setAreaTransparent(int p_78434_1_, int p_78434_2_, int p_78434_3_, int p_78434_4_)
    {
        if (!this.hasTransparency(p_78434_1_, p_78434_2_, p_78434_3_, p_78434_4_))
        {
            for (int i1 = p_78434_1_; i1 < p_78434_3_; ++i1)
            {
                for (int j1 = p_78434_2_; j1 < p_78434_4_; ++j1)
                {
                    this.imageData[i1 + j1 * this.imageWidth] &= 16777215;
                }
            }
        }
    }

    /**
     * Makes the given area of the image opaque
     */
    private void setAreaOpaque(int p_78433_1_, int p_78433_2_, int p_78433_3_, int p_78433_4_)
    {
        for (int i1 = p_78433_1_; i1 < p_78433_3_; ++i1)
        {
            for (int j1 = p_78433_2_; j1 < p_78433_4_; ++j1)
            {
                this.imageData[i1 + j1 * this.imageWidth] |= -16777216;
            }
        }
    }

    /**
     * Returns true if the given area of the image contains transparent pixels
     */
    private boolean hasTransparency(int p_78435_1_, int p_78435_2_, int p_78435_3_, int p_78435_4_)
    {
        for (int i1 = p_78435_1_; i1 < p_78435_3_; ++i1)
        {
            for (int j1 = p_78435_2_; j1 < p_78435_4_; ++j1)
            {
                int k1 = this.imageData[i1 + j1 * this.imageWidth];

                if ((k1 >> 24 & 255) < 128)
                {
                    return true;
                }
            }
        }

        return false;
    }
}