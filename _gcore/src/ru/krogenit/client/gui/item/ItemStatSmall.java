package ru.krogenit.client.gui.item;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.util.ResourceLocation;

@Getter
@AllArgsConstructor
public class ItemStatSmall {
    private final ResourceLocation texture;
    private final String value;
}

