package ru.krogenit.client.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.minecraft.client.gui.GuiChat;

@RequiredArgsConstructor
@Getter
public class EventChatGui extends Event {
    public enum EnumChatEventType {
        INIT, DRAW, UPDATE, KEY_TYPED, MOUSE_CLICKED;
    }

    private final GuiChat guiChat;
    private final EnumChatEventType type;
    @Setter private char keyCharacter;
    @Setter private int key;
    @Setter private int mouseX, mouseY, mouseButton;

    @Override
    public boolean isCancelable() {
        return true;
    }
}
