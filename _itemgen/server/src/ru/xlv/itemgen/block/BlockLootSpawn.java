package ru.xlv.itemgen.block;

import lombok.Getter;
import net.minecraft.block.BlockChest;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import ru.xlv.itemgen.XlvsItemGenMod;
import ru.xlv.itemgen.tile.TileEntityLootSpawn;

public class BlockLootSpawn extends BlockChest {

    @Getter
    private final int type;

    public BlockLootSpawn(String name, int type) {
        super(0);
        this.type = type;
        setBlockTextureName("chest");
        setBlockName(name);
        setCreativeTab(CreativeTabs.tabMisc);
    }

    @Override
    public boolean onBlockActivated(World p_149727_1_, int p_149727_2_, int p_149727_3_, int p_149727_4_, EntityPlayer p_149727_5_, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
        TileEntity tileEntity = p_149727_1_.getTileEntity(p_149727_2_, p_149727_3_, p_149727_4_);
        if (tileEntity instanceof TileEntityLootSpawn) {
            ((TileEntityLootSpawn) tileEntity).interact();
        }
        return super.onBlockActivated(p_149727_1_, p_149727_2_, p_149727_3_, p_149727_4_, p_149727_5_, p_149727_6_, p_149727_7_, p_149727_8_, p_149727_9_);
    }

    @Override
    public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
        return new TileEntityLootSpawn(XlvsItemGenMod.INSTANCE.getConfig().getLootSpawnConfig(type));
    }
}
