package ru.xlv.shop.handle.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.shop.XlvsShopMod;

@Getter
@RequiredArgsConstructor
public enum ShopBuyResult {

    SUCCESS(XlvsShopMod.INSTANCE.getLocalization().getResponseShopBuyResultSuccessMessage()),
    ITEM_NOT_FOUND(XlvsShopMod.INSTANCE.getLocalization().getResponseShopBuyResultItemNotFoundMessage()),
    NOT_ENOUGH_MONEY(XlvsShopMod.INSTANCE.getLocalization().getResponseShopBuyResultNotEnoughMoneyMessage()),
    UNSUPPORTED_COST(""),
    APPLICATION_IN_PROCESS("Your application is already in progress. Please wait."),
    UNKNOWN_ERROR(XlvsShopMod.INSTANCE.getLocalization().getResponseShopBuyResultUnknownErrorMessage()),
    NOT_ENOUGH_INV_SPACE(XlvsShopMod.INSTANCE.getLocalization().getResponseShopBuyResultNotEnoughInvSpaceMessage());

    private final String responseMessage;
}
