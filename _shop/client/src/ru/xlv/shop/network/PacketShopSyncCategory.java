package ru.xlv.shop.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.shop.common.ShopItem;
import ru.xlv.shop.common.ShopItemCategory;
import ru.xlv.shop.common.ShopItemCost;
import ru.xlv.shop.common.ShopItemType;
import ru.xlv.shop.handle.ShopItemStack;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class PacketShopSyncCategory implements IPacketCallbackEffective<List<ShopItem>> {

    private final List<ShopItem> result = new ArrayList<>();

    private ShopItemCategory shopItemCategory;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(shopItemCategory.ordinal());
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        int c = bbis.readInt();
        for (int i = 0; i < c; i++) {
            ShopItemType shopItemType = ShopItemType.values()[bbis.readInt()];
            int id = bbis.readInt();
            String name = bbis.readUTF();
            String description = bbis.readUTF();
            List<ShopItemCategory> shopItemCategories = new ArrayList<>();
            int c1 = bbis.readInt();
            for (int j = 0; j < c1; j++) {
                shopItemCategories.add(ShopItemCategory.values()[bbis.readInt()]);
            }
            List<ShopItemCost> shopItemCosts = new ArrayList<>();
            c1 = bbis.readInt();
            for (int j = 0; j < c1; j++) {
                shopItemCosts.add(new ShopItemCost(ShopItemCost.Type.values()[bbis.readInt()], bbis.readInt()));
            }
            switch (shopItemType) {
                case ITEM: {
                    ItemStack itemStack = ByteBufUtils.readItemStack(bbis.getBuffer());
                    result.add(new ShopItemStack(shopItemType, id, name, description, shopItemCategories, shopItemCosts, itemStack));
                }
            }
        }
//        List<ShopItemStack> shopItemStacks = readList(bbis, () -> {
////            ShopItemType shopItemType = ShopItemType.values()[bbis.readInt()];
////            int id = bbis.readInt();
////            String name = bbis.readUTF();
////            String description = bbis.readUTF();
////            List<ShopItemCategory> shopItemCategories = readList(bbis, ByteBufInputStream::readInt)
////                    .stream()
////                    .map(integer -> ShopItemCategory.values()[integer])
////                    .collect(Collectors.toList());
////            List<ShopItemCost> shopItemCosts = readList(bbis, byteBufInputStream -> {
////                ShopItemCost.Type type = ShopItemCost.Type.values()[byteBufInputStream.readInt()];
////                int amount = byteBufInputStream.readInt();
////                return new ShopItemCost(type, amount);
////            });
////            switch (shopItemType) {
////                case ITEM: {
////                    ItemStack itemStack = ByteBufUtils.readItemStack(bbis.getBuffer());
////                    return new ShopItemStack(shopItemType, id, name, description, shopItemCategories, shopItemCosts, itemStack);
////                }
////            }
//            return null;
//        });
////        result.addAll(shopItemStacks);
    }

    @Nullable
    @Override
    public List<ShopItem> getResult() {
        return result;
    }
}
