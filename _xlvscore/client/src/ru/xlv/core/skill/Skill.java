package ru.xlv.core.skill;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.item.Item;
import ru.xlv.core.common.player.character.ExperienceType;

import java.util.List;

@Getter
public class Skill {

    private final int id, familyId;
    private final int[] parentIds;
    private boolean isActivable, isActive;
    private long cooldown, maxCooldown, activationPeriod;
    private final String name, description;
    private final List<SkillCost> skillCosts;
    private final List<SkillLearnRule> skillRules;
    private final List<String> skillTags;

    private final float iconX, iconY;
    private final String textureName;

    public Skill(int id, int familyId, int[] parentIds, String name, String description, float iconX, float iconY, String textureName, List<SkillCost> skillCosts, List<SkillLearnRule> skillRules, List<String> skillTags) {
        this.id = id;
        this.familyId = familyId;
        this.parentIds = parentIds;
        this.name = name;
        this.description = description;
        this.skillCosts = skillCosts;
        this.skillRules = skillRules;
        this.skillTags = skillTags;
        this.iconX = iconX;
        this.iconY = iconY;
        this.textureName = textureName;
    }

    public Skill(int id, int familyId, int[] parentId, String name, String description, float iconX, float iconY, String textureName, List<SkillCost> skillCosts, List<SkillLearnRule> skillRules, List<String> skillTags, boolean isActive, long cooldown, long maxCooldown, long activationPeriod) {
        this(id, familyId, parentId, name, description, iconX, iconY, textureName, skillCosts, skillRules, skillTags);
        this.isActivable = true;
        this.isActive = isActive;
        this.cooldown = cooldown;
        this.maxCooldown = maxCooldown;
        this.activationPeriod = activationPeriod;
    }

    @Getter
    public static class SkillCost {

        private int amount;

        public SkillCost(int amount) {
            this.amount = amount;
        }
    }

    public static class SkillCostMana extends SkillCost {

        public SkillCostMana(int amount) {
            super(amount);
        }
    }

    @Getter
    public static class SkillCostItem extends SkillCost {

        private Item item;

        public SkillCostItem(int amount, Item item) {
            super(amount);
            this.item = item;
        }
    }

    public interface SkillLearnRule {}

    @Getter
    @RequiredArgsConstructor
    public static class SkillLearnRuleExp implements SkillLearnRule {
        private final ExperienceType experienceType;
        private final double amount;
    }

    @Getter
    @RequiredArgsConstructor
    public static class SKillLearnRuleItem implements SkillLearnRule {
        private final Item item;
        private final int amount;
    }

    @Getter
    @RequiredArgsConstructor
    public static class SkillLearnRuleAnother implements SkillLearnRule {
        private final int[] skillId;
    }
}
