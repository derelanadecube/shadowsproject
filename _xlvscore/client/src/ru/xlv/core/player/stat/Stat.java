package ru.xlv.core.player.stat;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.player.stat.IStatProvider;

@Getter
@RequiredArgsConstructor
public class Stat {

    private final IStatProvider statProvider;
    private final String displayName, name;
}
