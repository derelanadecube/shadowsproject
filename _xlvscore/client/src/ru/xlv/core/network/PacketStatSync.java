package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.player.stat.IStatProvider;
import ru.xlv.core.common.player.stat.StatDoubleProvider;
import ru.xlv.core.common.player.stat.StatIntProvider;
import ru.xlv.core.common.player.stat.StatValueType;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.stat.Stat;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class PacketStatSync implements IPacketCallbackEffective<List<Stat>> {

    private List<Stat> result;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {}

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result = new ArrayList<>();
        int c = bbis.readInt();
        for (int i = 0; i < c; i++) {
            String name = bbis.readUTF();
            String displayName = bbis.readUTF();
            int type = bbis.readInt();
            IStatProvider statProvider = null;
            switch (StatValueType.values()[type]) {
                case INT:
                    statProvider = new StatIntProvider(bbis.readInt());
                    break;
                case DOUBLE:
                    statProvider = new StatDoubleProvider(bbis.readDouble());
            }
            result.add(new Stat(statProvider, displayName, name));
        }
    }

    @Nullable
    @Override
    public List<Stat> getResult() {
        return result;
    }
}
