package ru.xlv.core.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillSelect implements IPacketOut {

    @Setter
    private int[] ids, hotSlotIndices;
    @Setter
    private boolean[] isActiveSkillArr;

    public PacketSkillSelect(int[] ids, int[] hotSlotIndices, boolean[] isActiveSkillArr) {
        this.ids = ids;
        this.hotSlotIndices = hotSlotIndices;
        this.isActiveSkillArr = isActiveSkillArr;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(isActiveSkillArr.length);
        for (int i = 0; i < isActiveSkillArr.length; i++) {
            bbos.writeBoolean(isActiveSkillArr[i]);
            bbos.writeInt(ids[i]);
            bbos.writeInt(hotSlotIndices[i]);
        }
    }
}
