package ru.xlv.core.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
public class PacketSkillBuildCreate implements IPacketCallbackEffective<String> {

    private String responseMessage;
    private int buildIndex;
    private int[] skillIds;

    public PacketSkillBuildCreate(int buildIndex, int[] skillIds) {
        this.buildIndex = buildIndex;
        this.skillIds = skillIds;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(buildIndex);
        bbos.writeInt(skillIds.length);
        for (int skillId : skillIds) {
            bbos.writeInt(skillId);
        }
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        responseMessage = bbis.readUTF();
    }

    @Nullable
    @Override
    public String getResult() {
        return responseMessage;
    }
}
