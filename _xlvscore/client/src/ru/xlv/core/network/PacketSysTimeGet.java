package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketSysTimeGet implements IPacketIn, IPacketOut {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(this);
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeLong(System.currentTimeMillis());
    }
}
