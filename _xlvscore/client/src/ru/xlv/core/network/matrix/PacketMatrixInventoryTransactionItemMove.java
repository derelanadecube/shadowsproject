package ru.xlv.core.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventoryTransactionItemMove implements IPacketOut {

    private int fromX, fromY, toX, toY, rotation;
    private boolean fromMyInventory, toMyInventory;

    public PacketMatrixInventoryTransactionItemMove(int fromX, int fromY, int toX, int toY, int rotation, boolean fromMyInventory, boolean toMyInventory) {
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.rotation = rotation;
        this.fromMyInventory = fromMyInventory;
        this.toMyInventory = toMyInventory;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(fromX);
        bbos.writeInt(fromY);
        bbos.writeInt(toX);
        bbos.writeInt(toY);
        bbos.writeInt(rotation);
        bbos.writeBoolean(fromMyInventory);
        bbos.writeBoolean(toMyInventory);
    }
}
