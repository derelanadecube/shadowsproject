package ru.xlv.core.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

public class PacketMatrixInventoryItemMove implements IPacketOut {

    private int fromX, fromY, toX, toY, rotation;

    public PacketMatrixInventoryItemMove(int fromX, int fromY, int toX, int toY, int rotation) {
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.rotation = rotation;
    }

    public PacketMatrixInventoryItemMove() {
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(fromX);
        bbos.writeInt(fromY);
        bbos.writeInt(toX);
        bbos.writeInt(toY);
        bbos.writeInt(rotation);
    }
}
