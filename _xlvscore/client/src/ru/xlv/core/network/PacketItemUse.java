package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketItemUse implements IPacketCallbackEffective<PacketItemUse.Result> {

    private final Result result = new Result();

    private int slotIndex;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(slotIndex);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result.success = bbis.readBoolean();
    }

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }

    @Getter
    public static class Result {
        private boolean success;
    }
}
