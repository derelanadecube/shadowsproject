package ru.xlv.core.gui;

import net.minecraft.client.gui.ScaledResolution;
import net.minecraftforge.client.event.RenderGameOverlayEvent;

public abstract class GameOverlayElement {

    private ScaledResolution scaledResolution;

    public void renderPre(RenderGameOverlayEvent.Pre event) {}

    public void renderPost(RenderGameOverlayEvent.Post event) {}

    public void init(ScaledResolution scaledResolution) {
        this.scaledResolution = scaledResolution;
    }

    public ScaledResolution getScaledResolution() {
        return scaledResolution;
    }

    public abstract void mouseClicked(int mouseX, int mouseY, int mouseButton);
}
