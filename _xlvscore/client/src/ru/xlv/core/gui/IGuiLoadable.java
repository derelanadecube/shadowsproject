package ru.xlv.core.gui;

import net.minecraft.util.ResourceLocation;
import ru.xlv.core.resource.ResourceLoadingState;
import ru.xlv.core.resource.ResourceLocationStateful;
import ru.xlv.core.util.Flex;

import java.util.HashMap;
import java.util.Map;

public interface IGuiLoadable {

    Map<Class<?>, ResourceLocationStateful[]> LOADABLE_MAP = new HashMap<>();

    default void startLoading(ResourceLocation... resourceLocations) {
        ResourceLocationStateful[] output = new ResourceLocationStateful[resourceLocations.length];
        Flex.remapArray(resourceLocations, output, ResourceLocationStateful::new);
        startLoading(output);
    }

    default void startLoading(ResourceLocationStateful... resourceLocationStatefuls) {
        LOADABLE_MAP.put(this.getClass(), resourceLocationStatefuls);
    }

    default boolean isLoaded() {
        ResourceLocationStateful[] resourceLocationStatefuls = LOADABLE_MAP.get(this.getClass());
        if (resourceLocationStatefuls != null) {
            for (ResourceLocationStateful resourceLocationStateful : resourceLocationStatefuls) {
                if(resourceLocationStateful.getLoadingState() != ResourceLoadingState.DONE) {
                    return false;
                }
            }
        }
        return true;
    }
}
