package ru.xlv.mochar.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.player.stat.IStatProvider;
import ru.xlv.core.common.player.stat.StatDoubleProvider;
import ru.xlv.core.common.player.stat.StatIntProvider;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.stat.StatManager;
import ru.xlv.mochar.player.stat.StatType;

import java.io.IOException;

@NoArgsConstructor
public class PacketStatSync implements IPacketCallbackOnServer {

    private static final RequestController<EntityPlayer> REQUEST_CONTROLLER = new RequestController.Periodic<>(1000L);

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        REQUEST_CONTROLLER.doCompletedRequestAsync(entityPlayer, packetCallbackSender::send);
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer.isOnline()) {
            StatManager statManager = serverPlayer.getSelectedCharacter().getStatManager();
            bbos.writeInt(statManager.getStatProviderMap().size());
            for (StatType type : statManager.getStatProviderMap().keySet()) {
                IStatProvider statProvider = statManager.getStatProviderMap().get(type);
                bbos.writeUTF(type.name());
                bbos.writeUTF(type.getDisplayName());
                bbos.writeInt(type.getValueType().ordinal());
                if(statProvider instanceof StatIntProvider) {
                    bbos.writeInt(((StatIntProvider) statProvider).getStatValue());
                } else if(statProvider instanceof StatDoubleProvider) {
                    bbos.writeDouble(((StatDoubleProvider) statProvider).getStatValue());
                }
            }
        }
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
