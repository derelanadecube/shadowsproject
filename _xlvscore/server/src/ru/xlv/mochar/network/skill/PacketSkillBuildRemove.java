package ru.xlv.mochar.network.skill;

import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillBuildRemove implements IPacketInOnServer {
    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int index = bbis.readInt();
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        serverPlayer.getSelectedCharacter().getSkillManager().removeSkillBuild(index);
    }
}
