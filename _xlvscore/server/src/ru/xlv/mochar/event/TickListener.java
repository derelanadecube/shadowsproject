package ru.xlv.mochar.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.CharacterAttribute;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.event.ServerPlayerRespawnEvent;
import ru.xlv.core.player.InsuranceManager;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.inventory.MatrixInventoryController;
import ru.xlv.mochar.player.character.Character;
import ru.xlv.mochar.player.character.CharacterAttributeUpdated;

public class TickListener {

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask = new ScheduledConsumableTask<>(500L, serverPlayer -> MatrixInventoryController.INSTANCE.closeStaticMatrixInventory(serverPlayer.getEntityPlayer()));

    @SubscribeEvent
    public void event(ServerPlayerRespawnEvent event) {
        InsuranceManager.INSTANCE.giveInsured(event.getServerPlayer().getEntityPlayer(), event.getServerPlayer().getSelectedCharacter().getMatrixInventory());
    }

    @SubscribeEvent
    public void event(TickEvent.PlayerTickEvent event) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(event.player);
        if(serverPlayer != null) {
            scheduledConsumableTask.update(serverPlayer);
            Character character = serverPlayer.getSelectedCharacter();
            if(character != null) {
                updateAttribute(event.player, SharedMonsterAttributes.movementSpeed, character.getAttributeCompleteValue(CharacterAttributeType.MOVE_SPEED) /*- finalWeight*/);
                updateAttribute(event.player, SharedMonsterAttributes.maxHealth, character.getAttributeCompleteValue(CharacterAttributeType.MAX_HEALTH));
                updateAttribute(event.player, SharedMonsterAttributes.knockbackResistance, 1D);
                for (CharacterAttribute characterAttribute : character.getCharacterAttributes()) {
                    if(characterAttribute instanceof CharacterAttributeUpdated) {
                        ((CharacterAttributeUpdated) characterAttribute).update(serverPlayer);
                    }
                }
            }
        }
    }

    private void updateAttribute(EntityPlayer entityPlayer, IAttribute defaultAttribute, double characterAttributeValue) {
        entityPlayer.getEntityAttribute(defaultAttribute).setBaseValue(characterAttributeValue);
    }
}
