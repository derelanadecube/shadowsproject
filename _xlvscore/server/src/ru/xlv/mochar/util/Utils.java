package ru.xlv.mochar.util;

import lombok.experimental.UtilityClass;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import ru.xlv.core.common.player.character.ExperienceType;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.SkillLearnRules;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.*;

@UtilityClass
public class Utils {

    private final String SKILL_LEARN_RULE_EXP_TEMPLATE = "Требуется {0} опыта типа {1}";
    private final String SKILL_LEARN_RULE_ITEM_TEMPLATE = "Требуется {0}x{1}";
    private final String SKILL_LEARN_RULE_ANOTHER_TEMPLATE = "Требуется изучить способность {0}";

    public String getStringedSkillRule(SkillLearnRules.SkillLearnRule skillLearnRule) {
        if(skillLearnRule instanceof SkillLearnRules.SkillLearnRuleItem) {
            return format(SKILL_LEARN_RULE_ITEM_TEMPLATE, ((SkillLearnRules.SkillLearnRuleItem) skillLearnRule).getItem(), ((SkillLearnRules.SkillLearnRuleItem) skillLearnRule).getAmount());
        } else if(skillLearnRule instanceof SkillLearnRules.SkillLearnRuleExp) {
            return format(SKILL_LEARN_RULE_EXP_TEMPLATE, ((SkillLearnRules.SkillLearnRuleExp) skillLearnRule).getAmount(), getExpLocalizedName(((SkillLearnRules.SkillLearnRuleExp) skillLearnRule).getExperienceType()));
        } else if(skillLearnRule instanceof SkillLearnRules.SkillLearnRuleAnother) {
            return format(SKILL_LEARN_RULE_ANOTHER_TEMPLATE, ((SkillLearnRules.SkillLearnRuleAnother) skillLearnRule).getSkillId());
        }
        return null;
    }

    public void sendMessage(ServerPlayer serverPlayer, String message, Object... params) {
        if(serverPlayer.isOnline()) {
            serverPlayer.getEntityPlayer().addChatMessage(new ChatComponentText(format(message, params)));
        }
    }

    public void sendMessage(EntityPlayer entityPlayer, String message, Object... params) {
        entityPlayer.addChatMessage(new ChatComponentText(format(message, params)));
    }

    public String format(String s, Object... objects) {
        for (int i = 0; i < objects.length; i++) {
            Object o = objects[i];
            if(o == null) continue;
            s = s.replace("{" + i + "}", String.valueOf(o));
        }
        return s;
    }

    public String getExpLocalizedName(ExperienceType experienceType) {
        switch (experienceType) {
            case BATTLE: return "Боевой";
            case EXPLORE: return "Исследовательский";
            case SURVIVE: return "Выживания";
        }
        return null;
    }

    public int[] integerArrayToIntArray(Integer[] integers) {
        int[] ii = new int[integers.length];
        for (int i = 0; i < ii.length; i++) {
            ii[i] = integers[i];
        }
        return ii;
    }

    public boolean isPlayerInside(EntityPlayer entityPlayer, WorldPosition worldPosition, double radius) {
        return isPlayerInside(entityPlayer, worldPosition.getDimension(), worldPosition.getX(), worldPosition.getY(), worldPosition.getZ(), radius);
    }

    public boolean isPlayerOp(EntityPlayer entityPlayer) {
        return MinecraftServer.getServer().getConfigurationManager().func_152596_g(entityPlayer.getGameProfile());
    }

    public boolean isPlayerInside(EntityPlayer entityPlayer, int dim, double x, double y, double z, double radius) {
        return entityPlayer.dimension == dim
                && entityPlayer.posX >= x - radius
                && entityPlayer.posY >= y - radius
                && entityPlayer.posZ >= z - radius
                && entityPlayer.posX < x + radius
                && entityPlayer.posY < y + radius
                && entityPlayer.posZ < z + radius;
    }

    public Item getItemByUnlocalizedName(String unlocalizedName) {
        Object object = Item.itemRegistry.getObject(unlocalizedName);
        return object instanceof Item ? (Item) object : null;
    }

    public <T> T castNumber(Number number, Class<T> tClass) {
        Object result = null;
        if(tClass.isAssignableFrom(Double.class)) {
            result = Double.parseDouble(number.toString());
        } else if(tClass.isAssignableFrom(Float.class)) {
            result = Float.parseFloat(number.toString());
        } else if(tClass.isAssignableFrom(Short.class)) {
            result = Short.parseShort(number.toString());
        } else if(tClass.isAssignableFrom(Long.class)) {
            result = Long.parseLong(number.toString());
        } else if(tClass.isAssignableFrom(Byte.class)) {
            result = Byte.parseByte(number.toString());
        }
        return result == null ? null : tClass.cast(result);
    }

    public long getPeriod(long start, long end) {
        return end - start;
    }

    public void printlnColored(@Nonnull Color color, @Nullable String message) {
//        System.out.println(String.format("\033[%s;%s;%sm%s\033[0m", color.getRed(), color.getGreen(), color.getBlue(), message));
        System.out.println(String.format("\033[131;1;0m%s\033[0m", message));
    }

    public static void main(String[] args) {
        printlnColored(Color.ORANGE, "asdasdasdasd");
    }
}

