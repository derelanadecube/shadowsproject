package ru.xlv.mochar.player.character.skill;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.network.IPacketComposable;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.result.SkillExecuteResult;

import javax.annotation.Nonnull;
import java.util.List;

public abstract class Skill implements IPacketComposable, Cloneable {

    private final SkillType skillType;

    public Skill(SkillType skillType) {
        this.skillType = skillType;
    }

    public SkillExecuteResult execute(ServerPlayer serverPlayer) {
        return SkillExecuteResult.UNKNOWN_ERROR;
    }

    public SkillType getSkillType() {
        return skillType;
    }

    protected int getSkillCategoryId() {
        return 0;
    }

    public void onSelected(ServerPlayer serverPlayer) {}

    public void onDeselected(ServerPlayer serverPlayer) {}

    @Override
    public void writeDataToPacket(List<Object> writableList, ByteBufOutputStream byteBufOutputStream) {
        writableList.add(getSkillCategoryId());
        writableList.add(getSkillType().getSkillId());
        writableList.add(getSkillType().getFamilyId());
        writeArray(getSkillType().getParentIds(), writableList);
        writableList.add(getSkillType().getName());
        writableList.add(getSkillType().getDescription());
        writeComposableCollection(getSkillType().getSkillCosts(), writableList, byteBufOutputStream);
        writeComposableCollection(getSkillType().getLearnRules(), writableList, byteBufOutputStream);
        writeSerializableCollection(getSkillType().getTags(), writableList);
        writableList.add(getSkillType().getIconX());
        writableList.add(getSkillType().getIconY());
        writableList.add(getSkillType().getTextureName());
    }

    @Nonnull
    @Override
    public abstract Skill clone();
}
