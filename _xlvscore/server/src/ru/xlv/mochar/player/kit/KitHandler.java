package ru.xlv.mochar.player.kit;

import lombok.experimental.UtilityClass;
import net.minecraft.item.ItemStack;
import ru.xlv.core.item.ItemStackFactory;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.Flex;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

@UtilityClass
public class KitHandler {

    private final Map<String, Kit> REGISTRY = new HashMap<>();

    static {
        Kit kit = new Kit();
        addItem(kit, ItemStackFactory.create("guns:item.spitter"));
        addItem(kit, ItemStackFactory.create("guns:ammo_spitter", 3));
        addItem(kit, ItemStackFactory.create("guns:item.incarn09"));
        addItem(kit, ItemStackFactory.create("guns:ammo_incarn09", 3));
        addItem(kit, ItemStackFactory.create("armor:item.bku0992Head"));
        addItem(kit, ItemStackFactory.create("armor:item.bku0992Body"));
        addItem(kit, ItemStackFactory.create("armor:item.bku0992Bracers"));
        addItem(kit, ItemStackFactory.create("armor:item.bku0992Legs"));
        addItem(kit, ItemStackFactory.create("armor:item.bku0992Feet"));
        addItem(kit, ItemStackFactory.create("guns:item.firstaidkit_small", 5));
        REGISTRY.put("start", kit);
    }

    private void addItem(Kit kit, ItemStack itemStack) {
        if (itemStack != null) {
            kit.getItems().add(itemStack);
        }
    }

    public void giveOut(ServerPlayer serverPlayer, @Nonnull Kit kit) {
        kit.getItems().forEach(itemStack -> {
            for (int i = 0; i < itemStack.stackSize; i++) {
                ItemStack itemStack1 = itemStack.copy();
                itemStack1.stackSize = 1;
                serverPlayer.getSelectedCharacter().getMatrixInventory().addItem(itemStack1);
            }
        });
    }

    @Nullable
    public Kit getKit(String key) {
        return Flex.getMapEntryValue(REGISTRY, stringKitEntry -> stringKitEntry.getKey().equals(key));
    }
}
