package ru.xlv.mochar.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.ExperienceType;
import ru.xlv.core.player.ServerPlayer;

public class CommandExp extends CommandBase {
    @Override
    public String getCommandName() {
        return "skillexp";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return null;
    }

    @Override
    public void processCommand(ICommandSender p_71515_1_, String[] p_71515_2_) {
        int exp = Integer.parseInt(p_71515_2_[1]);
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer((EntityPlayer) p_71515_1_);
        serverPlayer.getSelectedCharacter().getExperienceManager().addExp(serverPlayer, ExperienceType.valueOf(p_71515_2_[0]), exp);
    }
}
