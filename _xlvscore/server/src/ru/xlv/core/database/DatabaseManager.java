package ru.xlv.core.database;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntIntMap;
import lombok.Data;
import lombok.Getter;
import lombok.SneakyThrows;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import org.bson.Document;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import ru.xlv.core.common.database.DatabaseAdaptedBy;
import ru.xlv.core.common.database.DatabaseValue;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.util.BiHolder;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.database.mariadb.ConfigMariaDB;
import ru.xlv.core.database.mariadb.MariaDBProvider;
import ru.xlv.core.database.mongodb.ConfigMongoDB;
import ru.xlv.core.database.mongodb.MongoProvider;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.IConverter;
import ru.xlv.core.util.JsonUtils;
import ru.xlv.mochar.XlvsMainMod;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class DatabaseManager {

    @Data
    private static class EntityPlayerDAO {
        private final String playerName;
        private final CharacterType characterType;
        private final String nbtTagCompound;
    }

    @Data
    private static class PlayerSpawn {
        private final String playerName;
        private final WorldPosition worldPosition;
    }

    public static final Gson DB_GSON;

    static {
        DB_GSON = new GsonBuilder()
                .registerTypeAdapter(TIntList.class, JsonUtils.TINT_LIST_TYPE_ADAPTER)
                .registerTypeAdapter(TIntIntMap.class, JsonUtils.TINTINT_MAP_TYPE_ADAPTER)
                .registerTypeAdapter(ItemStack.class, JsonUtils.ITEM_STACK_TYPE_ADAPTER)
                .registerTypeAdapterFactory(new TypeAdapterFactory() {
                    private final Map<Class<?>, Object> fieldTypeAdapters = new HashMap<>();

                    @SneakyThrows
                    @Override
                    @SuppressWarnings("unchecked")
                    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
                        DatabaseAdaptedBy annotation = type.getRawType().getAnnotation(DatabaseAdaptedBy.class);
                        if(annotation != null) {
//                                System.out.println(type.getRawType());
                            return (TypeAdapter<T>) annotation.value().newInstance();
                        }
                        Object o = fieldTypeAdapters.get(type.getRawType());
                        if(o != null) {
                            return (TypeAdapter<T>) fieldTypeAdapters.remove(type.getRawType());
                        }
                        for (Field field : type.getRawType().getDeclaredFields()) {
                            annotation = field.getAnnotation(DatabaseAdaptedBy.class);
                            if(annotation != null) {
                                o = fieldTypeAdapters.get(field.getType());
                                if (o != null) {
                                    System.out.println("field serialize adapter for " + field + " already registered. This one will be ignored.");
                                } else {
//                                        System.out.println("found serialize adapter for " + field.getType());
                                    fieldTypeAdapters.put(field.getType(), annotation.value().newInstance());
                                }
                            }
                        }
                        return null;
                    }
                })
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().getAnnotation(DatabaseValue.class) == null && f.getAnnotation(DatabaseValue.class) == null;
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .setPrettyPrinting()
                .serializeNulls()
                .create();
    }

    private final IConverter<ServerPlayer, Document> playerDocumentConverter = new IConverter<ServerPlayer, Document>() {
        @Override
        public Document convertTo(ServerPlayer serverPlayer) {
            return Document.parse(JsonUtils.DB_GSON.toJson(serverPlayer));
        }

        @Override
        public ServerPlayer convertFrom(Document document) {
            JsonWriterSettings writerSettings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();
            String json = document.toJson(writerSettings);
            Long lastOnlineTimeMills = document.getLong("lastOnlineTimeMills");
            if (lastOnlineTimeMills != null) {
                json = json.replace("{\"$numberLong\":\"" + lastOnlineTimeMills + "\"}", Long.toString(lastOnlineTimeMills));
            }
            ServerPlayer serverPlayer;
            try {
                serverPlayer = DB_GSON.fromJson(json, ServerPlayer.class);
            } catch (Exception e) {
                System.out.println("Document: " + document);
                throw e;
            }
            return serverPlayer;
        }
    };
    private final IConverter<PlayerSpawn, Document> playerSpawnDocumentConverter = new IConverter<PlayerSpawn, Document>() {
        @Override
        public Document convertTo(PlayerSpawn playerSpawn) {
            return Document.parse(JsonUtils.SIMPLE_GSON.toJson(playerSpawn));
        }

        @Override
        public PlayerSpawn convertFrom(Document document) {
            return JsonUtils.SIMPLE_GSON.fromJson(document.toJson(), PlayerSpawn.class);
        }
    };

    private final String PLAYER_NAME_KEY = "playerName";
    private final String CHARACTER_TYPE_KEY = "characterType";

    @Getter
    private ServerIndex currentServerIndex;
    @Getter
    private List<ServerIndex> serverIndexList = new ArrayList<>();

    private ExecutorService executorService;
    private MongoProvider mongoProvider;
    private MariaDBProvider mariaDBProvider;
//    private RedisProvider redisProvider;
    private MongoCollection<Document> serverIndexingCollection, serverPlayerCollection, entityPlayerCollection, playerSpawnCollection;

    private String playerDefaultNbtTagCompound;

    public void init() {
        executorService = Executors.newFixedThreadPool(16);
        ConfigMongoDB config = new ConfigMongoDB("config/xlvscore/database/mongo.json");
        config.load();
        mongoProvider = new MongoProvider(config);
        mongoProvider.init();
        serverIndexingCollection = mongoProvider.getDatabase().getCollection("server_index_registry");
        serverPlayerCollection = mongoProvider.getDatabase().getCollection("player");
        entityPlayerCollection = mongoProvider.getDatabase().getCollection("e_player");
        playerSpawnCollection = mongoProvider.getDatabase().getCollection("p_spawn");
        mariaDBProvider = new MariaDBProvider(new ConfigMariaDB("config/xlvscore/database/maria.json"));
        mariaDBProvider.init();
//        redisProvider = new RedisProvider(new ConfigRedis("config/xlvscore/database/redis.json"));
//        redisProvider.init();
//        registerServer();

        PlayerDefaultNBT playerDefaultNBT = new PlayerDefaultNBT();
        playerDefaultNBT.load();
        playerDefaultNbtTagCompound = playerDefaultNBT.getNbtTagCompound();
    }

    public void shutdown() {
//        unregisterServer();
        executorService.shutdown();
        try {
            executorService.awaitTermination(10000, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        redisProvider.shutdown();
        mongoProvider.shutdown();
    }

    private void registerServer() {
        int availableIndex = 0;
        TIntList serverIndices = new TIntArrayList();
        serverIndexingCollection.find().forEach((Consumer<? super Document>) document -> serverIndices.add(JsonUtils.SIMPLE_GSON.fromJson(document.toJson(), ServerIndex.class).getIndex()));
        while(serverIndices.contains(availableIndex)) availableIndex++;
        currentServerIndex = new ServerIndex(MinecraftServer.getServer().getHostname() + ":" + MinecraftServer.getServer().getServerPort(), availableIndex);
        serverIndexingCollection.insertOne(Document.parse(JsonUtils.SIMPLE_GSON.toJson(currentServerIndex)));
    }

    private void unregisterServer() {
        if (currentServerIndex == null) {
            System.out.println("Warning! Couldn't unregister server from db.");
            return;
        }
        serverIndexingCollection.deleteOne(Filters.eq("index", currentServerIndex.getIndex()));
    }

    public CompletableFuture<Boolean> updatePlayerAsync(ServerPlayer serverPlayer) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                Document document = playerDocumentConverter.convertTo(serverPlayer);
//            redisProvider.putObject(PLAYER_CATEGORY_KEY + CATEGORY_DELIMITER + serverPlayer.getPlayerName(), playerDocumentConverter.convertTo(serverPlayer).toJson());
                UpdateResult updateResult = serverPlayerCollection.updateOne(Filters.eq(PLAYER_NAME_KEY, serverPlayer.getPlayerName()), new Document("$set", document));
                if (updateResult.getModifiedCount() == 0) {
                    serverPlayerCollection.insertOne(playerDocumentConverter.convertTo(serverPlayer));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return updateEntityPlayer1(serverPlayer.getPlayerName(), serverPlayer.getEntityPlayer(), serverPlayer.getSelectedCharacter().getCharacterType());
        }, executorService);
    }

    public CompletableFuture<Boolean> updateEntityPlayerAsync(String username, EntityPlayer entityPlayer, CharacterType characterType) {
        return CompletableFuture.supplyAsync(() -> updateEntityPlayer1(username, entityPlayer, characterType));
    }

    public CompletableFuture<Boolean> createEmptyPlayerAsync(String username, CharacterType characterType) {
        return CompletableFuture.supplyAsync(() -> updateEntityPlayer0(username, playerDefaultNbtTagCompound, characterType));
    }

    private boolean updateEntityPlayer1(String username, EntityPlayer entityPlayer, CharacterType characterType) {
        try {
            NBTTagCompound nbtTagCompound = new NBTTagCompound();
            if(entityPlayer == null) return false;
            XlvsMainMod.INSTANCE.getCharacterLoader().saveEntityPlayerToNBT(entityPlayer, nbtTagCompound);
            String nbtJson = JsonUtils.nbtToJson(nbtTagCompound);
            return updateEntityPlayer0(username, nbtJson, characterType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean updateEntityPlayer0(String username, String nbtJson, CharacterType characterType) {
        EntityPlayerDAO entityPlayerDAO = new EntityPlayerDAO(username, characterType, nbtJson);
        String json = JsonUtils.SIMPLE_GSON.toJson(entityPlayerDAO, EntityPlayerDAO.class);
        Document document1 = Document.parse(json);
        UpdateResult updateResult = entityPlayerCollection.updateOne(
                Filters.and(
                        Filters.eq(PLAYER_NAME_KEY, username),
                        Filters.eq(CHARACTER_TYPE_KEY, characterType.name())
                ),
                new Document("$set", document1)
        );
        if(updateResult.getMatchedCount() + updateResult.getModifiedCount() == 0) {
            entityPlayerCollection.insertOne(document1);
        }
        return true;
    }

    public ServerPlayer getPlayer(String username) {
//        String json = redisProvider.getObject(PLAYER_CATEGORY_KEY + CATEGORY_DELIMITER + username, String.class);
//        if (json != null) {
//            return playerDocumentConverter.convertFrom(Document.parse(json));
//        }
        Document first = serverPlayerCollection.find(Filters.eq(PLAYER_NAME_KEY, username)).first();
        if (first != null) {
            ServerPlayer serverPlayer = playerDocumentConverter.convertFrom(first);
            try {
                double playerPlatinum0 = getPlayerPlatinumSync(serverPlayer.getPlayerName());
                serverPlayer.setPlatinum(playerPlatinum0);
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    serverPlayer.setPlatinum(getPlayerPlatinumSync(serverPlayer.getPlayerName()));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
            return serverPlayer;
        }
        return null;
    }

    public NBTTagCompound getEntityPlayerNBTTags(String username, CharacterType characterType) {
        Document first = entityPlayerCollection.find(
                Filters.and(
                        Filters.eq(PLAYER_NAME_KEY, username),
                        Filters.eq(CHARACTER_TYPE_KEY, characterType.name())
                )
        ).first();
        if (first != null) {
            EntityPlayerDAO entityPlayerDAO = JsonUtils.SIMPLE_GSON.fromJson(first.toJson(), EntityPlayerDAO.class);
            return JsonUtils.jsonToNBT(entityPlayerDAO.getNbtTagCompound());
        }
        return null;
    }

    public CompletableFuture<ServerPlayer> getPlayerAsync(String username) {
        return CompletableFuture.supplyAsync(() -> getPlayer(username), executorService);
    }

    public CompletableFuture<Double> getPlayerPlatinumAsync(String username) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return getPlayerPlatinumSync(username);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            return 0D;
        }, executorService);
    }

    public double getPlayerPlatinumSync(String username) throws SQLException {
        String SQL_GET_PLAYER_PLATINUM_FORMAT = "SELECT money FROM users WHERE name='%s'";
        BiHolder<Statement, ResultSet> statementResultSetBiHolder = mariaDBProvider.doRequestAsync(String.format(SQL_GET_PLAYER_PLATINUM_FORMAT, username));
        if (statementResultSetBiHolder != null) {
            ResultSet resultSet = statementResultSetBiHolder.getSecond();
            if (resultSet != null) {
                resultSet.next();
                double value = resultSet.getDouble(1);
                statementResultSetBiHolder.getFirst().close();
                resultSet.close();
                return value;
            }
        }
        return 0;
    }

    @SneakyThrows
    public boolean setPlayerPlatinumSync(String username, double amount) {
        String SQL_SET_PLAYER_PLATINUM_FORMAT = "UPDATE users SET money=%s WHERE name='%s'";
        BiHolder<Statement, ResultSet> statementResultSetBiHolder = mariaDBProvider.doRequestAsync(String.format(SQL_SET_PLAYER_PLATINUM_FORMAT, amount, username));
        if (statementResultSetBiHolder != null) {
            statementResultSetBiHolder.getFirst().close();
            statementResultSetBiHolder.getSecond().close();
            return true;
        }
        return false;
    }

    public List<ServerPlayer> getAllPlayers() {
        List<ServerPlayer> list = new ArrayList<>();
        serverPlayerCollection.find().forEach((Consumer<? super Document>) document -> {
            try {
                list.add(playerDocumentConverter.convertFrom(document));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return list;
    }

    public Map<String, NBTTagCompound> getAllPlayerNBTTags() {
        Map<String, NBTTagCompound> map = new HashMap<>();
        entityPlayerCollection.find().forEach((Consumer<? super Document>) document -> {
            try {
                NBTTagCompound nbtTagCompound = JsonUtils.jsonToNBT(document.toJson());
                if (nbtTagCompound != null) {
                    String username = nbtTagCompound.getString(PLAYER_NAME_KEY);
                    if (username == null) {
                        return;
                    }
                    map.put(username, nbtTagCompound);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return map;
    }

    public CompletableFuture<Void> setPlayerSpawnPosition(String username, WorldPosition worldPosition) {
        return CompletableFuture.runAsync(() -> playerSpawnCollection.insertOne(playerSpawnDocumentConverter.convertTo(new PlayerSpawn(username, worldPosition))));
    }

    public WorldPosition getPlayerSpawnPosition(String username) {
        FindIterable<Document> documents = playerSpawnCollection.find(Filters.eq(PLAYER_NAME_KEY, username));
        MongoCursor<Document> iterator = documents.iterator();
        if (iterator.hasNext()) {
            Document first = iterator.next();
            return playerSpawnDocumentConverter.convertFrom(first).getWorldPosition();
        }
        return null;
    }

    public CompletableFuture<Boolean> removePlayerSpawnPositionAsync(String username) {
        return CompletableFuture.supplyAsync(() -> {
            DeleteResult deleteResult = playerSpawnCollection.deleteOne(Filters.eq(PLAYER_NAME_KEY, username));
            return deleteResult.getDeletedCount() > 0;
        });
    }
}
