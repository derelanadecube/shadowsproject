package ru.xlv.core.database.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bson.Document;
import ru.xlv.core.database.AbstractProviderDB;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.logging.Level;
import java.util.logging.Logger;

@Getter
public class MongoProvider extends AbstractProviderDB<ConfigMongoDB> {

    private MongoClient client;
    private MongoDatabase database;

    public MongoProvider(ConfigMongoDB config) {
        super(config);
    }

    @Override
    public void init() {
        MongoCredential mongoCredential = MongoCredential.createCredential(config.username, config.database_name, config.password.toCharArray());
        ServerAddress serverAddress = new ServerAddress(config.host, config.port);
        MongoClientOptions mongoClientOptions = MongoClientOptions.builder()
                .connectionsPerHost(32)
                .maxConnectionIdleTime(1000)
                .maxConnectionLifeTime(1000)
                .serverSelectionTimeout(5000)
                .connectTimeout(60000)
                .socketTimeout(60000)
                .build();
        client = new MongoClient(serverAddress, mongoCredential, mongoClientOptions);
        database = client.getDatabase(config.database_name);
        Logger.getLogger("org.mongodb.driver.connection").setLevel(Level.OFF);
    }

    @Override
    public void shutdown() {
        if(client != null) {
            client.close();
        }
    }

    @SneakyThrows(IllegalAccessException.class)
    public Document writeAsDocument(Object object) {
        Document document = new Document();
        for (Field field : object.getClass().getDeclaredFields()) {
            if(Modifier.isTransient(field.getModifiers())) {
                continue;
            }
            document.append(field.getName(), field.get(object));
        }
        return document;
    }
}
