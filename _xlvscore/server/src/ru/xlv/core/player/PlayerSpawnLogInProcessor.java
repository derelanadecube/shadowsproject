package ru.xlv.core.player;

import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.database.DatabaseManager;

import java.util.HashMap;
import java.util.Map;

public class PlayerSpawnLogInProcessor implements IPlayerLogInProcessor {

    private final Map<ServerPlayer, WorldPosition> playerSpawnPositions = new HashMap<>();

    @Override
    public void playerAboutToLogIn(EntityPlayer entityPlayer) {}

    @Override
    public void playerLoggedIn(ServerPlayer serverPlayer) {
        String username = serverPlayer.getPlayerName();
        DatabaseManager databaseManager = XlvsCore.INSTANCE.getDatabaseManager();
        WorldPosition playerSpawnPosition = databaseManager.getPlayerSpawnPosition(username);
        if (playerSpawnPosition == null) {
            playerSpawnPosition = playerSpawnPositions.remove(serverPlayer);
        } else {
            databaseManager.removePlayerSpawnPositionAsync(username);
        }
        if (playerSpawnPosition != null) {
            serverPlayer.movePlayer(playerSpawnPosition.getDimension(), playerSpawnPosition.getX(), playerSpawnPosition.getY(), playerSpawnPosition.getZ());
        }
    }

    public void setPlayerSpawnPosition(ServerPlayer serverPlayer, WorldPosition worldPosition) {
        synchronized (playerSpawnPositions) {
            playerSpawnPositions.put(serverPlayer, worldPosition);
        }
    }
}
