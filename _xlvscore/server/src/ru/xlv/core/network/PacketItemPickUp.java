package ru.xlv.core.network;

import lombok.NoArgsConstructor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.result.AddItemResult;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.network.matrix.PacketMatrixPlayerInventorySync;

import java.io.IOException;

@NoArgsConstructor
public class PacketItemPickUp implements IPacketInOnServer {

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int entityId = bbis.readInt();
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer != null) {
            Entity entity = entityPlayer.worldObj.getEntityByID(entityId);
            if (entity instanceof EntityItem) {
                entity.worldObj.playSoundEffect(entityPlayer.posX, entityPlayer.posY, entityPlayer.posZ, "random.pop", 1.0f, 1.0f);
                for (int i = 0 ; i < ((EntityItem) entity).getEntityItem().stackSize; i++) {
                    ItemStack itemStack = ((EntityItem) entity).getEntityItem().copy();
                    itemStack.stackSize = 1;
                    AddItemResult addItemResult = serverPlayer.getSelectedCharacter().getMatrixInventory().addItem(itemStack);
                    if(addItemResult == AddItemResult.SUCCESS) {
                        serverPlayer.getSelectedCharacter().onMatrixInvItemAdded(itemStack);
                        XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixPlayerInventorySync(serverPlayer));
                    } else {
                        ((EntityItem) entity).getEntityItem().stackSize -= i;
                        if (XlvsCore.INSTANCE.getNotificationService() != null) {
                            XlvsCore.INSTANCE.getNotificationService().sendNotification(serverPlayer, XlvsCore.INSTANCE.getLocalization().responseMatrixInvAddItemError);
                        }
                        return;
                    }
                }
                entity.setDead();
            }
        }
    }
}
