package ru.xlv.core.module;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.module.Module;

@Getter(AccessLevel.PROTECTED)
@RequiredArgsConstructor
public abstract class ServerModule<T extends ServerModule<T>> extends Module<T> {

    private final XlvsCore core;
}
