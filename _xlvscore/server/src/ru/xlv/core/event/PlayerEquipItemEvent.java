package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ru.xlv.core.player.ServerPlayer;

@Getter
@RequiredArgsConstructor
public class PlayerEquipItemEvent extends Event {

    private final ServerPlayer serverPlayer;
    private final ItemStack itemStack;
}
