package ru.xlv.core.achievement;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import ru.xlv.core.common.database.AbstractDatabaseAdapter;

import java.io.IOException;

public class AchievementDatabaseAdapter extends AbstractDatabaseAdapter<Achievement> {

    @Override
    public void write(JsonWriter out, Achievement value) throws IOException {
        if(value instanceof AchievementBase) {
            out.value(0);
            out.value(value.getAchievementType().ordinal());
            out.value(value.isAchieved());
        } else if(value instanceof AchievementPhased) {
            out.value(1);
            out.value(value.getAchievementType().ordinal());
            out.value(((AchievementPhased) value).getProgress());
            out.value(((AchievementPhased) value).getPhase());
        } else if(value instanceof AchievementProgressive) {
            out.value(2);
            out.value(value.getAchievementType().ordinal());
            out.value(((AchievementProgressive) value).getProgress());
        }
    }

    @Override
    public Achievement read(JsonReader in) throws IOException {
        Achievement achievement;
        switch (in.nextInt()) {
            case 0:
                achievement = new AchievementBase(AchievementType.values()[in.nextInt()]);
                achievement.setAchieved(in.nextBoolean());
                break;
            case 1:
                AchievementType value = AchievementType.values()[in.nextInt()];
                achievement = new AchievementPhased(value, (int[]) value.params[0]);
                ((AchievementPhased) achievement).progress = in.nextInt();
                ((AchievementPhased) achievement).phase = in.nextInt();
                break;
            case 2:
                AchievementType achievementType = AchievementType.values()[in.nextInt()];
                achievement = new AchievementProgressive(achievementType, in.nextInt());
                ((AchievementProgressive) achievement).progress = in.nextInt();
                break;
            default: return null;
        }
        return achievement;
    }
}
