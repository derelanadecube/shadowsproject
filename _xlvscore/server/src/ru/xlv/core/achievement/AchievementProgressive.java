package ru.xlv.core.achievement;

import lombok.Getter;

@Getter
public class AchievementProgressive extends Achievement {

    private final AchievementType achievementType;
    protected int maxProgress;
    protected int progress;

    public AchievementProgressive(AchievementType achievementType, int maxProgress) {
        this.achievementType = achievementType;
        this.maxProgress = maxProgress;
    }

    public void updateProgress() {
        if (!isAchieved()) {
            progress++;
        }
    }

    @Override
    public void achieve() {
        progress = maxProgress;
    }

    @Override
    public void setAchieved(boolean flag) {
        if(flag) {
            progress = maxProgress;
        }
    }

    @Override
    public boolean isAchieved() {
        return progress >= maxProgress;
    }

    @Override
    public AchievementType getAchievementType() {
        return achievementType;
    }

    @Override
    public Achievement clone() {
        AchievementProgressive achievementProgressive = new AchievementProgressive(achievementType, maxProgress);
        achievementProgressive.progress = progress;
        return achievementProgressive;
    }
}
