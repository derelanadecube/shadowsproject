package ru.xlv.core.achievement;

import javax.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class AchievementFactory {

    private static final Map<AchievementType, Achievement> achievementBlanks = new HashMap<>();

    public static Achievement getAchievement(AchievementType achievementType) {
        Achievement achievement = achievementBlanks.get(achievementType);
        if (achievement != null) {
            return achievement.clone();
        }
        Class<?>[] paramClasses = new Class[achievementType.params.length + 1];
        paramClasses[0] = AchievementType.class;
        for (int i = 0; i < achievementType.params.length; i++) {
            Class<?> aClass = achievementType.params[i].getClass();
            Class<?> primitiveOfClass = getPrimitiveOf(aClass);
            if (primitiveOfClass != null) {
                aClass = primitiveOfClass;
            }
            paramClasses[i + 1] = aClass;
        }
        Object[] params = new Object[achievementType.params.length + 1];
        params[0] = achievementType;
        System.arraycopy(achievementType.params, 0, params, 1, achievementType.params.length);
        try {
            achievementBlanks.put(achievementType, achievementType.achievementClass.getConstructor(paramClasses).newInstance(params));
            return achievementType.achievementClass.getConstructor(paramClasses).newInstance(params);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        throw new RuntimeException();
    }

    @Nullable
    private static Class<?> getPrimitiveOf(Class<?> clazz) {
        if(clazz == Integer.class) {
            return int.class;
        } else if(clazz == Double.class) {
            return double.class;
        } else if(clazz == Float.class) {
            return float.class;
        } else if(clazz == Long.class) {
            return long.class;
        } else if(clazz == Short.class) {
            return short.class;
        } else if(clazz == Byte.class) {
            return byte.class;
        } else if(clazz == Character.class) {
            return char.class;
        } else if(clazz == Boolean.class) {
            return boolean.class;
        }
        return null;
    }
}
