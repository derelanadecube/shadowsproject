package ru.xlv.core.util.flex;

import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.player.ServerPlayer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FlexPlayerList<T> extends ContainerList<FlexPlayer, ServerPlayer> {

    public FlexPlayerList(Collection<FlexPlayer> object) {
        super(object);
        this.stream = object.stream();
    }

    public FlexPlayerList<T> onlineOnly() {
        return filter(ServerPlayer::isOnline);
    }

    public FlexPlayerList<T> around(EntityPlayer entityPlayer, double radius) {
        return filter(serverPlayer1 -> serverPlayer1.getEntityPlayer().getDistanceToEntity(entityPlayer) < radius);
    }

    public FlexPlayerList<T> around(ServerPlayer serverPlayer, double radius) {
        return filter(serverPlayer1 -> serverPlayer1.getEntityPlayer().getDistanceToEntity(serverPlayer.getEntityPlayer()) < radius);
    }

    public FlexPlayerList<T> around(double x, double y, double z, double radius) {
        return filter(serverPlayer1 -> serverPlayer1.getEntityPlayer().getDistance(x, y, z) < radius);
    }

    @Override
    public FlexPlayerList<T> filter(Predicate<ServerPlayer> predicate) {
        return (FlexPlayerList<T>) super.filter(predicate);
    }

    @Override
    public FlexPlayerList<T> forEach(Consumer<ServerPlayer> consumer) {
        return (FlexPlayerList<T>) super.forEach(consumer);
    }

    @Override
    public FlexPlayerList<T> forEachFlex(Consumer<FlexPlayer> consumer) {
        return (FlexPlayerList<T>) super.forEachFlex(consumer);
    }

    public FlexPlayerList<T> sendPacket(IPacketOutServer packet) {
        Set<FlexPlayer> collect = stream.collect(Collectors.toSet());
        collect.forEach(serverPlayerFlexPlayer -> serverPlayerFlexPlayer.sendPacket(packet));
        stream = collect.stream();
        return this;
    }

    public FlexPlayerList<T> sendPacket(Function<ServerPlayer, IPacketOutServer> function) {
        Set<FlexPlayer> collect = stream.collect(Collectors.toSet());
        collect.forEach(serverPlayerFlexPlayer -> serverPlayerFlexPlayer.sendPacket(function));
        stream = collect.stream();
        return this;
    }

    public FlexPlayerList<T> sendMessage(String message, Object... params) {
        Set<FlexPlayer> collect = stream.collect(Collectors.toSet());
        collect.forEach(serverPlayerFlexPlayer -> serverPlayerFlexPlayer.sendMessage(message, params));
        stream = collect.stream();
        return this;
    }

    public FlexPlayerList<T> sendMessage(Function<ServerPlayer, String> function) {
        Set<FlexPlayer> collect = stream.collect(Collectors.toSet());
        collect.forEach(serverPlayerFlexPlayer -> serverPlayerFlexPlayer.sendMessage(function.apply(serverPlayerFlexPlayer.object)));
        stream = collect.stream();
        return this;
    }

    @Override
    protected Container<Collection<FlexPlayer>> createContainer(Collection<FlexPlayer> flexPlayers) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected <A> BiContainer<Container<Collection<FlexPlayer>>, Collection<FlexPlayer>, A> createBiContainer(A a) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    public static FlexPlayerList<Collection<FlexPlayer>> of(@Nullable ServerPlayer... serverPlayers) {
        return serverPlayers == null ? new FlexPlayerList<>(new ArrayList<>()) : new FlexPlayerList<>(Arrays.stream(serverPlayers).map(FlexPlayer::of).collect(Collectors.toList()));
    }

    @Nonnull
    public static FlexPlayerList<Collection<FlexPlayer>> of(@Nullable Collection<ServerPlayer> serverPlayers) {
        return serverPlayers == null ? new FlexPlayerList<>(new ArrayList<>()) : new FlexPlayerList<>(serverPlayers.stream().map(FlexPlayer::of).collect(Collectors.toList()));
    }

    @Nonnull
    public static FlexPlayerList<Collection<FlexPlayer>> actual() {
        return new FlexPlayerList<>(XlvsCore.INSTANCE.getPlayerManager().getPlayerList().stream().map(FlexPlayer::of).collect(Collectors.toList()));
    }
}
