package ru.xlv.core.common.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;

@Getter
@RequiredArgsConstructor
public class OnQuestCompleteEvent extends Event {
    private final EntityPlayer player;
    private final int experienceType;
    private final int combatReward, researchReward, surviveReward;
    private final int credits, platina;
}
