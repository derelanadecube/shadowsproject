package ru.xlv.core.common.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public interface IItemUsable {

    ItemUseResult useItem(EntityPlayer entityPlayer, ItemStack itemStack, int slotIndex);
    void update(ItemStack itemStack);
    float getAnimation(ItemStack itemStack);
}
