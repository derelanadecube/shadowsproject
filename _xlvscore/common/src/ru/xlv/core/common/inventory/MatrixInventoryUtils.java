package ru.xlv.core.common.inventory;

import lombok.experimental.UtilityClass;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.common.item.ItemBase;

@UtilityClass
public class MatrixInventoryUtils {

    private final String TAG_ITEM_LIST_NOT_ALLOWED_TO_EQUIP = "itemListNotAllowedToEquipWith";
    private final String TAG_INV_MATRIX_WIDTH = "invMatrixWidth";
    private final String TAG_INV_MATRIX_HEIGHT = "invMatrixHeight";
    private final String TAG_INV_MATRIX_ROTATION = "invMatrixRotation";

    public final int DEFAULT_INV_MATRIX_ITEM_WIDTH = 1;
    public final int DEFAULT_INV_MATRIX_ITEM_HEIGHT = 1;

    public void moveItem(MatrixInventory fromInventory, MatrixInventory toInventory, int fromX, int fromY, int toX, int toY, int rotation) {
        ItemStack itemStack = fromInventory.getMatrixItem(fromX, fromY);
        if (itemStack != null) {
            int w = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
            int h = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
            int clickedId = fromInventory.getMatrix()[fromY][fromX];
            if (toInventory.isSpaceFree(toX, toY, w, h, clickedId)) {
                if (fromInventory.removeItem(itemStack)) {
                    MatrixInventoryUtils.setInvMatrixRotation(itemStack, rotation);
                    toInventory.setItem(toX, toY, itemStack);
                }
            }
        }
    }

    public int getInvMatrixWidth(ItemStack itemStack) {
        if(itemStack == null) return 0;
        if(mustBeInited(itemStack)) {
            setupMatrixItem(itemStack);
        }
        int rotation = getInvMatrixRotation(itemStack);
        if(rotation == 0 || rotation == 2) {
            return itemStack.getTagCompound().hasKey(TAG_INV_MATRIX_WIDTH) ? itemStack.getTagCompound().getInteger(TAG_INV_MATRIX_WIDTH) : DEFAULT_INV_MATRIX_ITEM_WIDTH;
        } else  {
            return itemStack.getTagCompound().hasKey(TAG_INV_MATRIX_HEIGHT) ? itemStack.getTagCompound().getInteger(TAG_INV_MATRIX_HEIGHT) : DEFAULT_INV_MATRIX_ITEM_HEIGHT;
        }
    }

    public int getInvMatrixHeight(ItemStack itemStack) {
        if(itemStack == null) return 0;
        if(mustBeInited(itemStack)) {
            setupMatrixItem(itemStack);
        }
        int rotation = getInvMatrixRotation(itemStack);
        if(rotation == 0 || rotation == 2) {
            return itemStack.getTagCompound().hasKey(TAG_INV_MATRIX_HEIGHT) ? itemStack.getTagCompound().getInteger(TAG_INV_MATRIX_HEIGHT) : DEFAULT_INV_MATRIX_ITEM_HEIGHT;
        } else {
            return itemStack.getTagCompound().hasKey(TAG_INV_MATRIX_WIDTH) ? itemStack.getTagCompound().getInteger(TAG_INV_MATRIX_WIDTH) : DEFAULT_INV_MATRIX_ITEM_WIDTH;
        }
    }

    public int getInvMatrixRotation(ItemStack itemStack) {
        if(itemStack == null) return 0;
        return itemStack.getTagCompound() == null ? 0 : itemStack.getTagCompound().getInteger(TAG_INV_MATRIX_ROTATION);
    }

    public void setInvMatrix(ItemStack itemStack, int width, int height) {
        if(itemStack == null) return;
        NBTTagCompound nbtTagCompound = itemStack.getTagCompound() == null ? new NBTTagCompound() : itemStack.getTagCompound();
        nbtTagCompound.setInteger(TAG_INV_MATRIX_WIDTH, width);
        nbtTagCompound.setInteger(TAG_INV_MATRIX_HEIGHT, height);
        nbtTagCompound.setInteger(TAG_INV_MATRIX_ROTATION, 0);
        itemStack.setTagCompound(nbtTagCompound);
    }

    public void setInvMatrixRotation(ItemStack itemStack, int rotation) {
        if(itemStack == null) return;
        NBTTagCompound nbtTagCompound = itemStack.getTagCompound() == null ? new NBTTagCompound() : itemStack.getTagCompound();
        nbtTagCompound.setInteger(TAG_INV_MATRIX_ROTATION, rotation < 4 ? rotation : 0);
        itemStack.setTagCompound(nbtTagCompound);
    }

    public void setupMatrixItem(ItemStack itemStack) {
        if(itemStack == null) return;
        int w = itemStack.getItem() instanceof ItemBase ? ((ItemBase) itemStack.getItem()).getInvMatrixWidth() : DEFAULT_INV_MATRIX_ITEM_WIDTH;
        int h = itemStack.getItem() instanceof ItemBase ? ((ItemBase) itemStack.getItem()).getInvMatrixHeight() : DEFAULT_INV_MATRIX_ITEM_HEIGHT;
        setInvMatrix(itemStack, w, h);
    }

    private boolean mustBeInited(ItemStack itemStack) {
        return itemStack.getTagCompound() == null || !itemStack.getTagCompound().hasKey(TAG_INV_MATRIX_WIDTH) || !itemStack.getTagCompound().hasKey(TAG_INV_MATRIX_HEIGHT);
    }

    public void setItemIdsNotAllowedToEquipWith(ItemStack itemStack, int... ids) {
        NBTTagCompound nbtTagCompound = itemStack.getTagCompound() == null ? new NBTTagCompound() : itemStack.getTagCompound();
        nbtTagCompound.setIntArray(TAG_ITEM_LIST_NOT_ALLOWED_TO_EQUIP, ids);
        itemStack.setTagCompound(nbtTagCompound);
    }

//    public static boolean equipItem(MatrixInventory matrixInventory, MatrixInventory.SlotType slotType, ItemStack itemStack) {
//        if(hasInventoryItemNotAllowedToEquipWith(matrixInventory, itemStack)) {
//            matrixInventory.setItemToSpecifiedSlot(slotType, itemStack);
//            return true;
//        }
//        return false;
//    }
//
//    public static boolean hasInventoryItemNotAllowedToEquipWith(MatrixInventory matrixInventory, ItemStack itemStack) {
//        int[] blockedToEquipWithList = getItemIdsNotAllowedToEquipWith(itemStack);
//        if(blockedToEquipWithList.length > 0) {
//            for (int id : blockedToEquipWithList) {
//                Item item = Item.getItemById(id);
//                for (MatrixInventory.SlotType value : MatrixInventory.SlotType.values()) {
//                    ItemStack result = matrixInventory.getItemFromSpecifiedSlot(value);
//                    if(result != null && result.getItem() == item) {
//                        return true;
//                    }
//                }
//            }
//        }
//        return false;
//    }

    public int[] getItemIdsNotAllowedToEquipWith(ItemStack itemStack) {
        int[] ints = new int[0];
        if(itemStack.getTagCompound() != null && itemStack.getTagCompound().hasKey(TAG_ITEM_LIST_NOT_ALLOWED_TO_EQUIP)) {
            ints = itemStack.getTagCompound().getIntArray(TAG_ITEM_LIST_NOT_ALLOWED_TO_EQUIP);
        }
        return ints;
    }
}
