package ru.xlv.core.common.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

/*
 * Don't forget about NBT!
 * */
public interface IMatrixInventoryProvider {

    MatrixInventory getMatrixInventory();

    boolean canInteractWith(@Nonnull EntityPlayer entityPlayer);

    default void onMatrixInvItemAdded(@Nonnull ItemStack itemStack) {}
    default void onMatrixInvItemRemoved(@Nonnull ItemStack itemStack) {}
    default void onMatrixInvCleaned() {}
    default void onInvItemAddedToSpecSlot(@Nonnull ItemStack itemStack) {}
    default void onInvItemRemovedFromSpecSlot(@Nonnull ItemStack itemStack) {}

    default boolean isAddingItemNotAllowed(@Nonnull ItemStack itemStack) {
        return false;
    }

    default boolean isRemovingItemNotAllowed(@Nonnull ItemStack itemStack) {
        return false;
    }
}
