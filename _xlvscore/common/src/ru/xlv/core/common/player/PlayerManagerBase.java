package ru.xlv.core.common.player;

import ru.xlv.core.util.Flex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class PlayerManagerBase<T extends Player> {

    protected final List<T> playerList = Collections.synchronizedList(new ArrayList<>());

    /**
     * @return false если перебор был остановлен
     * */
    protected boolean forEach(Function<T, Boolean> function) {
        synchronized (playerList) {
            return Flex.forEach(playerList, function);
        }
    }

    protected void forEach(Consumer<T> consumer) {
        synchronized (playerList) {
            playerList.forEach(consumer);
        }
    }

    protected void addPlayer(T player) {
        synchronized (playerList) {
            playerList.add(player);
        }
    }

    public T getPlayer(String name) {
        synchronized (playerList) {
            return Flex.getCollectionElement(playerList, t -> t.getPlayerName().equalsIgnoreCase(name));
        }
    }

    public synchronized List<T> getPlayerList() {
        return playerList;
    }
}
