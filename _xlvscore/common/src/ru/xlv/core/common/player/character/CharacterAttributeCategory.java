package ru.xlv.core.common.player.character;

public enum CharacterAttributeCategory {

    BASE,
    REGEN,
    DAMAGE,
    PROTECTION

}
