package ru.xlv.core.common.player.character;

public enum ExperienceType {

    BATTLE,
    EXPLORE,
    SURVIVE

}
