package ru.xlv.core.common.player.character;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Objects;
import java.util.UUID;

@ToString
@Setter
@Getter
@Builder
public class CharacterAttributeMod implements Cloneable {

    private final CharacterAttributeType attributeType;
    private UUID uuid;
    @Builder.Default
    private double valueMod = 0;
    @Builder.Default
    private double valueAdd = 0;
    @Builder.Default
    private double maxValueMod = 0;
    @Builder.Default
    private double maxValueAdd = 0;
    @Builder.Default
    private long period = -1;
    @Builder.Default
    private long creationTimeMills = System.currentTimeMillis();

    public boolean shouldBeRemoved() {
        return period != -1 && System.currentTimeMillis() - creationTimeMills >= period;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        CharacterAttributeMod that = (CharacterAttributeMod) object;
        return Double.compare(that.valueMod, valueMod) == 0 &&
                Double.compare(that.valueAdd, valueAdd) == 0 &&
                Double.compare(that.maxValueMod, maxValueMod) == 0 &&
                Double.compare(that.maxValueAdd, maxValueAdd) == 0 &&
                period == that.period &&
                creationTimeMills == that.creationTimeMills &&
                attributeType == that.attributeType &&
                Objects.equals(uuid, that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attributeType, uuid, valueMod, valueAdd, maxValueMod, maxValueAdd, period, creationTimeMills);
    }

    @Override
    public CharacterAttributeMod clone() {
        return new CharacterAttributeMod(attributeType, uuid, valueMod, valueAdd, maxValueMod, maxValueAdd, period, System.currentTimeMillis());
    }
}
