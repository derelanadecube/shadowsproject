package ru.xlv.core.common.player;

import lombok.Getter;
import lombok.Setter;
import ru.xlv.core.common.database.DatabaseValue;

public class Player {

    @Getter
    @DatabaseValue
    protected String playerName;

    @Setter
    @Getter
    @DatabaseValue
    private boolean isOnline;
    @Setter
    @Getter
    @DatabaseValue
    private long lastOnlineTimeMills;

    public Player(String playerName) {
        this.playerName = playerName;
    }
}
