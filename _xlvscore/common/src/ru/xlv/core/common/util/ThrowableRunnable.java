package ru.xlv.core.common.util;

import lombok.SneakyThrows;

public interface ThrowableRunnable extends Runnable {

    @SneakyThrows
    @Override
    default void run() {
        execute();
    }

    void execute() throws Throwable;
}
