package ru.xlv.core.common.util.config.yaml;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

import java.io.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Convert a JavaBean object to yaml and back
 * */
public interface IConfigYaml {

    default <T> T load(Class<T> model) throws IOException {
        InputStream inputStream = new FileInputStream(new File(getFilePath()));
        T t = model.cast(new Yaml(new Constructor(model)).load(inputStream));
        inputStream.close();
        return t;
    }

    default void save(Object data) throws IOException {
        Yaml yaml = new Yaml();
        File file = new File(getFilePath());
        if(!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        StringWriter writer = new StringWriter();
        yaml.dump(data, writer);
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.append(writer.getBuffer());
        fileWriter.close();
        writer.close();
    }

    default <T extends IMappedYamlModel> void saveAsMap(T object) throws IOException {
        String output = getYaml(object).dump(object);
        File file = new File(getFilePath());
        if(!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        new FileWriter(file).append(output).close();
    }

    default <T extends IMappedYamlModel> T load1(Class<T> clazz) throws IOException, IllegalAccessException, InstantiationException {
        InputStream inputStream = new FileInputStream(new File(getFilePath()));
        T t = clazz.cast(getYaml(clazz.newInstance()).load(inputStream));
        inputStream.close();
        return t;
    }

    default <T extends IMappedYamlModel> Yaml getYaml(T object) {
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);
        Representer representer = new Representer();
        object.getTags().forEach((aClass, s) -> {
            representer.addClassTag(aClass, new Tag(s));
            System.out.println(aClass + " " + s);
        });
        return new Yaml(new Constructor(), representer, options);
    }

    default <T extends IMappedYamlModel> Map<String, Map<String, ?>> loadAsMap(T object) throws IOException {
        InputStream inputStream = new FileInputStream(new File(getFilePath()));
        Map<String, Map<String, ?>> map = (Map) getYaml(object).load(inputStream);
        inputStream.close();
        return map;
    }

    default <T extends IMappedYamlModel> T getObjectFromMap(Class<T> clazz, Map<String, ?> map) throws Exception {
        T t = clazz.newInstance();
        for (Field declaredField : clazz.getDeclaredFields()) {
            if(map.containsKey(declaredField.getName())) {
                declaredField.set(t, map.get(declaredField.getName()));
            }
        }
        return t;
    }

    default <T> Map<String, Map> getMap(T object) {
        Map<String, Map> map = new HashMap<>();
        Map<String, Object> map1 = new HashMap<>();
        for (Field declaredField : object.getClass().getDeclaredFields()) {
            try {
                map1.put(declaredField.getName(), declaredField.get(object));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        map.put(object.getClass().getSimpleName(), map1);
        return map;
    }

    String getFilePath();
}
