package ru.xlv.core.common.util.config.yaml;

import java.util.Map;

public interface IMappedYamlModel {

    Map<Class<?>, String> getTags();
}
