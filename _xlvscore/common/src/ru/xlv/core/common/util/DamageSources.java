package ru.xlv.core.common.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;

public class DamageSources {

    public static final DamageSource RADIATION = new DamageSource("radiation");

    public static DamageSource causeBulletDamage(EntityPlayer shooter) {
        return new EntityDamageSource("player_bullet", shooter);
    }
}
