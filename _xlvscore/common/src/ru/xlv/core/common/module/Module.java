package ru.xlv.core.common.module;

public abstract class Module<T extends Module<T>> {

    public abstract String getModuleId();
}
