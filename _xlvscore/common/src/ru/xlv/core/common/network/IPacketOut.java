package ru.xlv.core.common.network;

import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;

public interface IPacketOut extends IPacket {

    /**
     * Calls when the packet is sending out.
     * */
    void write(ByteBufOutputStream bbos) throws IOException;

}
