package ru.xlv.overview;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import ru.xlv.overview.util.Camera;


public class XlvsEvents {

    private static boolean toggleModeEnabled = false;
    private static boolean isOverviewInitialPress = false;
    private static boolean overviewEnabled = false;

    @SubscribeEvent
    public void event(PlayerEvent.PlayerRespawnEvent e) {
        isOverviewInitialPress = false;
        toggleModeEnabled = false;
        overviewEnabled = false;
        Camera.setCamera();
    }

    @SubscribeEvent
    public void event(TickEvent.RenderTickEvent event) {
        EntityPlayer player = Minecraft.getMinecraft().thePlayer;
        if (player == null) {
            return;
        }
        if (!player.isRiding()) {
            if (!toggleModeEnabled) {
                if ((XlvsOverviewMod.keyOverview.getIsKeyPressed()) && (!isOverviewInitialPress)) {
                    Camera.setCamera();
                    isOverviewInitialPress = true;
                }
                if ((XlvsOverviewMod.keyOverview.getIsKeyPressed()) && (isOverviewInitialPress)) {
                    Camera.update(event.phase == TickEvent.Phase.START);
                }
                if ((!XlvsOverviewMod.keyOverview.getIsKeyPressed()) && (isOverviewInitialPress)) {
                    Camera.resetCamera();
                    isOverviewInitialPress = false;
                }
            } else if (overviewEnabled) {
                Camera.cameraEnabled(event.phase == TickEvent.Phase.START);
            } else {
                Camera.resetCamera();
            }
        }
    }

    @SubscribeEvent
    public void event(TickEvent.ClientTickEvent event) {
        EntityPlayer player = Minecraft.getMinecraft().thePlayer;
        if (XlvsOverviewMod.keyToggleMode.isPressed()) {
            toggleModeEnabled = !toggleModeEnabled;
            Minecraft.getMinecraft().thePlayer.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Режим обзора: " + (toggleModeEnabled ? "переключение" : "удержание")));
        }
        if (player == null) return;
        if (!player.isRiding()) {
            if ((XlvsOverviewMod.keyOverview.isPressed()) && (toggleModeEnabled)) {
                overviewEnabled = !overviewEnabled;
            }
        }
    }
}