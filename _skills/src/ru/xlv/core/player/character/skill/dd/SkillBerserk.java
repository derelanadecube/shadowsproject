package ru.xlv.core.player.character.skill.dd;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ISkillUpdatable;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SkillBerserk extends Skill implements ISkillUpdatable {

    private static final UUID MOD_UUID = UUID.randomUUID();
    private static final UUID MOD_UUID1 = UUID.randomUUID();

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    public SkillBerserk(SkillType skillType) {
        super(skillType);
        double healthLimit = skillType.getCustomParam("if_hp_is_lower_than", double.class);
        double damageMod = 1D + skillType.getCustomParam("outcome_damage_value", double.class) / 100D;
        double protectionMod = 1D + skillType.getCustomParam("income_damage_value", double.class) / 100D;
        CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(protectionMod)
                .period(1000L)
                .uuid(MOD_UUID)
                .build();
        CharacterAttributeMod characterAttributeMod1 = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_MOD)
                .valueMod(damageMod)
                .period(1000L)
                .uuid(MOD_UUID1)
                .build();
        scheduledConsumableTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> {
            if(serverPlayer.getEntityPlayer().getHealth() < healthLimit) {
                serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
                serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod1, true);
            }
        });
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        scheduledConsumableTask.update(serverPlayer);
    }

    @Nonnull
    @Override
    public SkillBerserk clone() {
        return new SkillBerserk(getSkillType());
    }
}
