package ru.xlv.core.player.character.skill.tank;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillStrengthenPosition extends ActivableSkill {

    private final CharacterAttributeMod characterAttributeMod;

    public SkillStrengthenPosition(SkillType skillType) {
        super(skillType);
        double protectionAdd = 1D + skillType.getCustomParam("protect_add_self_all_value", double.class) / 100D;
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(protectionAdd)
                .period(skillType.getDuration())
                .build();
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
    }

    @Nonnull
    @Override
    public SkillStrengthenPosition clone() {
        return new SkillStrengthenPosition(getSkillType());
    }
}
