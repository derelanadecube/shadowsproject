package ru.xlv.core.player.character.skill.tank;

import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.player.character.skill.SkillUtils;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ISkillUpdatable;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.Set;
import java.util.UUID;

public class SkillAdaptiveMaterials extends Skill implements ISkillUpdatable {

    private static final UUID MOD_UUID = UUID.randomUUID();

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    public SkillAdaptiveMaterials(SkillType skillType) {
        super(skillType);
        double protectionMod = skillType.getCustomParam("protect_add_self_all_value", double.class) / 100D;
        double radius = skillType.getCustomParam("range", double.class);
        scheduledConsumableTask = new ScheduledConsumableTask<>(500L, serverPlayer -> {
            Set<EntityPlayer> enemyPlayersAround = SkillUtils.getEnemyPlayersAround(serverPlayer.getEntityPlayer(), radius);
            int amount = Math.min(enemyPlayersAround.size(), 4);
            CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                    .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                    .valueMod(1D + protectionMod * amount)
                    .period(500L)
                    .uuid(MOD_UUID)
                    .build();
            serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
        });
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        scheduledConsumableTask.update(serverPlayer);
    }

    @Nonnull
    @Override
    public SkillAdaptiveMaterials clone() {
        return new SkillAdaptiveMaterials(getSkillType());
    }
}
