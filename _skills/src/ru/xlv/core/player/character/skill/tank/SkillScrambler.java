package ru.xlv.core.player.character.skill.tank;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.player.character.skill.SkillUtils;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.Set;

public class SkillScrambler extends ActivableSkill {

    private final double radius;
    private final CharacterAttributeMod characterAttributeMod, characterAttributeMod1, characterAttributeMod2;

    public SkillScrambler(SkillType skillType) {
        super(skillType);
        radius = skillType.getCustomParam("range", double.class);
        double cooldownMod = 1D - skillType.getCustomParam("ally_skill_add_recharge_time", double.class) / 100D;
        double allyManaRegen = skillType.getCustomParam("mana_regen_ally_all_value", double.class) / 100D;
        double enemyManaRegen = Math.abs(skillType.getCustomParam("mana_regen_enemy_all_value", double.class) / 100D);
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MANA)
                .valueMod(allyManaRegen)
                .period(skillType.getDuration())
                .build();
        characterAttributeMod1 = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MANA)
                .valueMod(enemyManaRegen)
                .period(skillType.getDuration())
                .build();
        characterAttributeMod2 = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.COOLDOWN_MOD)
                .valueMod(cooldownMod)
                .period(skillType.getDuration())
                .build();
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        Set<ServerPlayer> enemyServerPlayersAround = SkillUtils.getEnemyServerPlayersAround(serverPlayer, radius);
        enemyServerPlayersAround.forEach(serverPlayer1 -> serverPlayer1.getSelectedCharacter().addAttributeMod(characterAttributeMod1, true));
        SkillUtils.getAllyPlayersAround(serverPlayer, radius).forEach(serverPlayer1 -> {
            serverPlayer1.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
            serverPlayer1.getSelectedCharacter().addAttributeMod(characterAttributeMod2, true);
        });
    }

    @Nonnull
    @Override
    public SkillScrambler clone() {
        return new SkillScrambler(getSkillType());
    }
}
