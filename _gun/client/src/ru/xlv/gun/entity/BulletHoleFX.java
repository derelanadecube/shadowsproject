package ru.xlv.gun.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class BulletHoleFX extends EntityFX {

    private static final ResourceLocation textureBulletHole = new ResourceLocation("batmod", "textures/bullet_hole.png");
    private int side;

    public static Block block;

    public BulletHoleFX(World world) {
        super(world, 0,0,0);
    }

    public BulletHoleFX(World world, double x, double y, double z, int sideHit, Block blockm) {
        super(world, x, y, z);
        this.motionX = this.motionY = this.motionZ = 0.0D;
        this.side = sideHit;
        this.particleMaxAge = 100;
        block = blockm;
    }

    @Override
    public void renderParticle(Tessellator tessellator, float p_70539_2_, float p_70539_3_, float p_70539_4_, float p_70539_5_, float p_70539_6_, float p_70539_7_) {
        Entity player = Minecraft.getMinecraft().thePlayer;
        interpPosX = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) p_70539_2_;
        interpPosY = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) p_70539_2_;
        interpPosZ = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) p_70539_2_;
        float f8 = 0.125F;
        float f9 = (float) (this.posX - interpPosX);
        float f10 = (float) (this.posY - interpPosY);
        float f11 = (float) (this.posZ - interpPosZ);
        GL11.glPushMatrix();
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(774, 768);
        GL11.glColor4f(1f, 1f, 1f, 0.2f);
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240.0F, 0.0F);
        ru.xlv.core.util.Utils.bindTexture(textureBulletHole);
        tessellator.startDrawingQuads();
        switch (side) {
            case 0:
                tessellator.addVertexWithUV((double) (f9 - f8), (double) f10 - 0.01F, (double) (f11 - f8), 0.0D, 1.0D);
                tessellator.addVertexWithUV((double) (f9 + f8), (double) f10 - 0.01F, (double) (f11 - f8), 1.0D, 1.0D);
                tessellator.addVertexWithUV((double) (f9 + f8), (double) f10 - 0.01F, (double) (f11 + f8), 1.0D, 0.0D);
                tessellator.addVertexWithUV((double) (f9 - f8), (double) f10 - 0.01F, (double) (f11 + f8), 0.0D, 0.0D);
                break;
            case 1:
                tessellator.addVertexWithUV((double) (f9 - f8), (double) f10 + 0.01F, (double) (f11 + f8), 0.0D, 0.0D);
                tessellator.addVertexWithUV((double) (f9 + f8), (double) f10 + 0.01F, (double) (f11 + f8), 1.0D, 0.0D);
                tessellator.addVertexWithUV((double) (f9 + f8), (double) f10 + 0.01F, (double) (f11 - f8), 1.0D, 1.0D);
                tessellator.addVertexWithUV((double) (f9 - f8), (double) f10 + 0.01F, (double) (f11 - f8), 0.0D, 1.0D);
                break;
            case 2:
                tessellator.addVertexWithUV((double) (f9 - f8), (double) (f10 + f8), (double) (f11 - 0.01F), 0.0D, 1.0D);
                tessellator.addVertexWithUV((double) (f9 + f8), (double) (f10 + f8), (double) (f11 - 0.01F), 1.0D, 1.0D);
                tessellator.addVertexWithUV((double) (f9 + f8), (double) (f10 - f8), (double) (f11 - 0.01F), 1.0D, 0.0D);
                tessellator.addVertexWithUV((double) (f9 - f8), (double) (f10 - f8), (double) (f11 - 0.01F), 0.0D, 0.0D);
                break;
            case 3:
                tessellator.addVertexWithUV((double) (f9 - f8), (double) (f10 - f8), (double) (f11 + 0.01F), 0.0D, 0.0D);
                tessellator.addVertexWithUV((double) (f9 + f8), (double) (f10 - f8), (double) (f11 + 0.01F), 1.0D, 0.0D);
                tessellator.addVertexWithUV((double) (f9 + f8), (double) (f10 + f8), (double) (f11 + 0.01F), 1.0D, 1.0D);
                tessellator.addVertexWithUV((double) (f9 - f8), (double) (f10 + f8), (double) (f11 + 0.01F), 0.0D, 1.0D);
                break;
            case 4:
                tessellator.addVertexWithUV((double) (f9 - 0.01F), (double) (f10 + f8), (double) (f11 + f8), 1.0D, 1.0D);
                tessellator.addVertexWithUV((double) (f9 - 0.01F), (double) (f10 + f8), (double) (f11 - f8), 1.0D, 0.0D);
                tessellator.addVertexWithUV((double) (f9 - 0.01F), (double) (f10 - f8), (double) (f11 - f8), 0.0D, 0.0D);
                tessellator.addVertexWithUV((double) (f9 - 0.01F), (double) (f10 - f8), (double) (f11 + f8), 0.0D, 1.0D);
                break;
            case 5:
                tessellator.addVertexWithUV((double) (f9 + 0.01F), (double) (f10 - f8), (double) (f11 + f8), 0.0D, 1.0D);
                tessellator.addVertexWithUV((double) (f9 + 0.01F), (double) (f10 - f8), (double) (f11 - f8), 0.0D, 0.0D);
                tessellator.addVertexWithUV((double) (f9 + 0.01F), (double) (f10 + f8), (double) (f11 - f8), 1.0D, 0.0D);
                tessellator.addVertexWithUV((double) (f9 + 0.01F), (double) (f10 + f8), (double) (f11 + f8), 1.0D, 1.0D);
                break;
        }
        tessellator.draw();
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glPopMatrix();
    }

    @Override
    public void onUpdate() {
        if (this.particleAge++ >= this.particleMaxAge) this.setDead();
    }

    @Override
    public int getFXLayer() {
        return 3;
    }
}
