package ru.xlv.gun.render.guns.rifle;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.ModelWrapperDisplayList;
import ru.xlv.gun.render.RenderAttachments;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.util.Utils;

public class AK74URender implements IItemRenderer {
	
    public static final ResourceLocation tex = new ResourceLocation("batmod", "models/guns/ak74u/texture.png");
    private static RenderAttachments renderAtt;
    RenderWeaponThings rWT = new RenderWeaponThings();    
	Minecraft mc = Minecraft.getMinecraft();
	IModelCustom model;
	
	public AK74URender() {
		renderAtt = new RenderAttachments();
		this.model = ModelLoader.loadModel(new ResourceLocation("batmod:models/guns/ak74u/model.sp"));
        this.model = new ModelWrapperDisplayList((WavefrontObject)this.model);
	}
	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}


    
    
	
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;
		ItemWeapon weapon = (ItemWeapon)is.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		//анимация прицеливания
		//анимация бега
		//анимация выстрела
		//анимация перезарядки
	    //анимация ливинг
		float aimAnim = rWT.lastRunProgress + (rWT.runProgress - rWT.lastRunProgress) * RenderWeaponThings.renderTicks;
	
		float runAnim = rWT.lastRun1Progress + (rWT.run1Progress - rWT.lastRun1Progress)* RenderWeaponThings.renderTicks;
	
		float shootAnim = rWT.lastRun2Progress + (rWT.run2Progress - rWT.lastRun2Progress)* RenderWeaponThings.renderTicks;

	    float reloadAnim = rWT.lastRun3Progress + (rWT.run3Progress - rWT.lastRun3Progress)* RenderWeaponThings.renderTicks;
	 
	    float livingAnim = rWT.lastRun4Progress + (rWT.run4Progress - rWT.lastRun4Progress)* RenderWeaponThings.renderTicks;
	    
		switch (type) {
		case EQUIPPED_FIRST_PERSON: {
			rWT.doAnimations();
 			
			float x = 0;
			float y = 0;
			float z = 0;
			GL11.glPushMatrix();
			if (is.getTagCompound().getString("scope").length() > 2) {
				if (ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("scope"))
						.getItem() == Items.pkas) {
					x = -0.075F;
					y = -0.14F;
					z = 0.0957F;
				}
			}
			if (is.getTagCompound().getString("scope").length() > 2) {
				if (ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("scope"))
						.getItem() == Items.pso) {
					// gameSettings.fovSetting = 90 - 60 * aimAnim;
					x = 0.196F;
					y = -0.117F;
					z = -0.13F;
				}

			}
			if (is.getTagCompound().getString("scope").length() > 2) {
				if (ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("scope"))
						.getItem() == Items.kobra) {
				// gameSettings.fovSetting = 90 - 10 * aimAnim;
				x = 0.042F;
				y = -0.138F;
				z = -0.013F;
				}
			}
			float dSWS = 1F;
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
			if(Utils.isPlayerAiming()){
			   dSWS = 0.15F;
			}
			float fov = gameSettings.fovSetting/10000;
			if(!Utils.isPlayerAiming())
				GL11.glTranslatef(0,0.05F*livingAnim - aimAnim/30,0);
			else
			GL11.glTranslatef(0,-0.6F*livingAnim*fov,0);
			GL11.glRotatef(30 * reloadAnim, 1, 0, 0);
			GL11.glRotatef(-25*runAnim, 1, 0, 0);
			GL11.glRotatef(-15*runAnim, 0, 0, 1);
			GL11.glTranslatef(-0.4F*runAnim, -0.8F*runAnim, 0.5F*runAnim);
			GL11.glRotated(-1F*aimAnim, 1.0, 0.0, 1.0);
			GL11.glTranslatef(-0.686F * aimAnim+ x*aimAnim, 0.072F * aimAnim + y*aimAnim, 0.1315F * aimAnim + z*aimAnim);
			GL11.glRotated(4.0F*shootAnim*dSWS, 1.0, 0.0, 1.0);
			GL11.glRotated(-1.0F*shootAnim*dSWS, 0.0, 1.0, 1.0);
			GL11.glTranslatef(-0.085F * shootAnim*dSWS, 0.1F*shootAnim*dSWS, 0.084F * shootAnim*dSWS);
			
			GL11.glPushMatrix();
			GL11.glScalef(1.4F, 1.4F, 1.4F);
			
			GL11.glRotated(0.0, 1.0, 0.0, 0.0);
			GL11.glRotated(45.0, 0.0, 1.0, 0.0);
			GL11.glRotated(1.0, 0.0, 0.0, 1.0);
			//GL11.glTranslatef(-0.4F * aimAnim, 0.203F * aimAnim, -0.570F * aimAnim);
			GL11.glTranslatef(-0.26F, 0.49F, -0.215F);
			renderAttachments(type, is);
			ru.xlv.core.util.Utils.bindTexture(tex);
			model.renderAll();
			if(shootTimer == 1){
				GL11.glPushMatrix();
				GL11.glScalef(0.13F, 0.13F, 0.13F);
				GL11.glRotatef(90, 0, 1, 0);
				GL11.glTranslatef(-0.7F + 0.7F * aimAnim, 2.0F, 34F);
				rWT.flash();
				GL11.glPopMatrix();
			}	
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			rWT.renderRightArm(0.4F, -1.65F, 0.92F, -90, -5, -25, 2.1F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			GL11.glTranslatef(-0.6f * reloadAnim, -0.3f * reloadAnim, 0);
			GL11.glRotatef(10 * reloadAnim, 1, 0, 0);
			rWT.renderLeftArm(-0.62F, -0.41F, -0.36F, 20, 25, -80, 2.4F);
			GL11.glPopMatrix();
			
			GL11.glPopMatrix();
		}
		break;
		case EQUIPPED: {
			GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    GL11.glTranslatef(0.2F, -0.05F, -0.5F);
	        GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
	        renderEntityAndEquipped(type, is);
		}
		break;
		case ENTITY: {
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
		
		
	}
	public void renderAttachments(ItemRenderType type, ItemStack is){
		NBTTagCompound nbtstack = is.getTagCompound();
		if(is != null) {
			if(nbtstack != null) {
				if(is.stackTagCompound != null) {
						if(nbtstack.getString("scope").length() > 2) {
							if(ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("scope")).getItem() == Items.pso) {
								GL11.glPushMatrix();
								GL11.glTranslatef(0.1F, -0.04F, -0.041F);
								renderAtt.renderPSO(Utils.isPlayerAiming());
								GL11.glPopMatrix();
							
							}
							if(ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("scope")).getItem() == Items.pkas) {
								GL11.glPushMatrix();
								GL11.glTranslatef(0F, -0.01F, -0.01F);
								renderAtt.renderPKAS(Utils.isPlayerAiming());
								GL11.glPopMatrix();
							}
							if(ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("scope")).getItem() == Items.kobra) {
								GL11.glPushMatrix();
								GL11.glTranslatef(0F, 0.01F, -0.02F);
								renderAtt.renderKobra(Utils.isPlayerAiming());
								GL11.glPopMatrix();
							}
						}
					
						if(nbtstack.getString("stvol").length() > 2 && ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("stvol")).getItem() == Items.pbs4) {
							GL11.glPushMatrix();
							GL11.glTranslatef(-0.72F, -0.175F, -0.035F);
							renderAtt.renderPBS4();
							GL11.glPopMatrix();
						}
						if(nbtstack.getString("grip").length() > 2 && ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("grip")).getItem() == Items.laser) {
							 GL11.glPushMatrix();
							 GL11.glScalef(0.05F, 0.05F, 0.05F);
							 GL11.glTranslatef(15.7F, -9.8F, -1.47F);
							 renderAtt.renderLaser(type);
							 GL11.glPopMatrix();
						}
				}
			}
		}
	}
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){
    	
    	GL11.glPushMatrix();
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0, 0, 0);
		NBTTagCompound tagz = is.getTagCompound();
		if(tagz != null) {	
			if(tagz.getString("scope").length() > 0 || tagz.getString("stvol").length() > 0 || tagz.getString("grip").length() > 0) {
				renderAttachments(type, is);
			}
		}
		ru.xlv.core.util.Utils.bindTexture(tex);
		model.renderAll();
		GL11.glPopMatrix();
    }

}
