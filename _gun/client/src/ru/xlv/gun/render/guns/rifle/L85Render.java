package ru.xlv.gun.render.guns.rifle;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.ModelWrapperDisplayList;
import ru.xlv.gun.render.RenderAttachments;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.util.Utils;

public class L85Render implements IItemRenderer {
	
	
	
	
	//public static final IModelCustom model = spproject.loaderModel.Modell.loadModel(new ResourceLocation("batmod", "models/guns/ak74.sp"));
	
    public static final ResourceLocation tex = new ResourceLocation("batmod", "models/guns/l85/texture.png");
    public static final ResourceLocation sighttex = new ResourceLocation("batmod", "models/guns/l85/sight.png");
    RenderWeaponThings rWT = new RenderWeaponThings();
    private static RenderAttachments renderAtt;
	Minecraft mc = Minecraft.getMinecraft();
	public L85Render() {
		
		renderAtt = new RenderAttachments();
		this.model = ModelLoader.loadModel(new ResourceLocation("batmod:models/guns/l85/model.sp"));
        this.model = new ModelWrapperDisplayList((WavefrontObject)this.model);
	}
	IModelCustom model;

	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}


    
    
	
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;

		if(Minecraft.getMinecraft().isSingleplayer()) throw new RuntimeException("Something go wrong...");
		ItemWeapon weapon = (ItemWeapon)is.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		//анимация прицеливания
		//анимация бега
		//анимация выстрела
		//анимация перезарядки
	    //анимация ливинг
		float aimAnim = rWT.lastRunProgress + (rWT.runProgress - rWT.lastRunProgress) * RenderWeaponThings.renderTicks;
	
		float runAnim = rWT.lastRun1Progress + (rWT.run1Progress - rWT.lastRun1Progress)* RenderWeaponThings.renderTicks;
	
		float shootAnim = rWT.lastRun2Progress + (rWT.run2Progress - rWT.lastRun2Progress)* RenderWeaponThings.renderTicks;

	    float reloadAnim = rWT.lastRun3Progress + (rWT.run3Progress - rWT.lastRun3Progress)* RenderWeaponThings.renderTicks;
	 
	    float livingAnim = rWT.lastRun4Progress + (rWT.run4Progress - rWT.lastRun4Progress)* RenderWeaponThings.renderTicks;
	    
		switch (type) {
		case EQUIPPED_FIRST_PERSON: {
			rWT.doAnimations();
 			
			float x = 0;
			float y = 0;
			float z = 0;
			GL11.glPushMatrix();
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
			if(AttachmentType.checkAttachment(is,"scope", Items.leupold)){
				//gameSettings.fovSetting = 90 - 40 * aimAnim;
				x = 0.04F;
				y = 0.128F;
				z = -0.032F;
				
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.acog)){
				//gameSettings.fovSetting = 90 - 40 * aimAnim;
				x = 0.1F;
				y = 0.132F;
				z = -0.091F;
				
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.susat)){
				//gameSettings.fovSetting = 90 - 60 * aimAnim;
				x = -0.0545F;
				y = 0.028F;
				z = 0.06F;
				
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.barska)){
				//gameSettings.fovSetting = 90 - 60 * aimAnim;
				x = -0.125F;
				y = 0.0975F;
				z = 0.121F;
				
			}
			float dSWS = 1F;
			float f1 = 1F;
			if(Utils.isPlayerAiming()){
				dSWS = 0.2F;
				f1 = 1F;
			}
			float fov = gameSettings.fovSetting/10000;
			if(!Utils.isPlayerAiming())
				GL11.glTranslatef(0,0.05F*livingAnim - aimAnim/30,0);
			else
				GL11.glTranslatef(0,-0.6F*livingAnim*fov,0);
			GL11.glRotatef(65*reloadAnim, 0, 0, 1);
			GL11.glRotatef(-25*runAnim, 1, 0, 0);
			GL11.glRotatef(-15*runAnim, 0, 0, 1);
			GL11.glTranslatef(-0.4F*runAnim, -1.0F*runAnim, 0.5F*runAnim);
			GL11.glRotated(1.0F*aimAnim, 1.0, 0.0, 1.0);
			GL11.glRotated(-0.0F*aimAnim, 0.0, 1.0, 0.0);
			GL11.glTranslatef(-0.352F * aimAnim+ x*aimAnim, -0.105F * aimAnim + y*aimAnim, -0.115F * aimAnim + z*aimAnim);
			GL11.glRotated(4.0F*shootAnim*dSWS, 1.0, 0.0, 1.0);
			GL11.glRotated(-1.0F*shootAnim*dSWS, 0.0, 1.0, 1.0);
			GL11.glTranslatef(-0.085F * shootAnim*dSWS, 0.1F*shootAnim*dSWS, 0.084F * shootAnim*dSWS);
			
			GL11.glPushMatrix();
			GL11.glScalef(1.37F, 1.37F, 1.37F);
			
			GL11.glRotated(0.0, 1.0, 0.0, 0.0);
			GL11.glRotated(45.0, 0.0, 1.0, 0.0);
			GL11.glRotated(0.0, 0.0, 0.0, 1.0);
			//GL11.glTranslatef(-0.4F * aimAnim, 0.203F * aimAnim, -0.570F * aimAnim);
			GL11.glTranslatef(-0.65F, 0.34F, -0.29F);
			renderAttachments(type, is);
			ru.xlv.core.util.Utils.bindTexture(tex);
			model.renderPart("gun");
			if(shootTimer == 1){
				GL11.glPushMatrix();
				GL11.glScalef(0.13F, 0.13F, 0.13F);
				GL11.glRotatef(90, 0, 1, 0);
				GL11.glTranslatef(-0.1F, 5F, 28F);
				rWT.flash();
				GL11.glPopMatrix();
			}	
			
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			GL11.glPopMatrix();
			
			GL11.glPushMatrix();
			rWT.renderRightArm(0.55F, -1.1F, 0.9F, -90, -10, -30, 2.1F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			rWT.renderLeftArm(-0.67F, -0.45F, -0.39F, 20, 28, -80, 2.3F);
			GL11.glPopMatrix();
			
		    
			GL11.glPopMatrix();
		}
		break;
		case EQUIPPED: {
			GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    GL11.glTranslatef(0.15F, -0.07F, -0.5F);
	        GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
	        renderEntityAndEquipped(type, is);
		}
		break;
		case ENTITY: {
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
		
		
	}
	public void renderAttachments(ItemRenderType type, ItemStack is){
		//render scope//
				if(is.getTagCompound().getString("scope").length() < 3){
				ru.xlv.core.util.Utils.bindTexture(sighttex);
				model.renderPart("sight");
				}
				else if(AttachmentType.checkAttachment(is,"scope", Items.leupold)){
				GL11.glPushMatrix();
				GL11.glScalef(1F, 1F, 1F);
				GL11.glTranslatef(0.2F, 0.11F, 0.02F);
				renderAtt.renderLeupold(Utils.isPlayerAiming());
				GL11.glPopMatrix();
				}
				else if(AttachmentType.checkAttachment(is,"scope", Items.acog)){
					GL11.glPushMatrix();
					GL11.glScalef(1F, 1F, 1F);
					GL11.glTranslatef(0.2F, 0.11F, 0.02F);
					renderAtt.renderACOG(Utils.isPlayerAiming());
					GL11.glPopMatrix();
					}
				else if(AttachmentType.checkAttachment(is,"scope", Items.susat)){
					GL11.glPushMatrix();
					GL11.glScalef(1F, 1F, 1F);
					GL11.glTranslatef(0.15F, 0.11F, 0.025F);
					renderAtt.renderSUSAT(Utils.isPlayerAiming());
					GL11.glPopMatrix();
				}
				else if(AttachmentType.checkAttachment(is,"scope", Items.barska)){
					GL11.glPushMatrix();
					GL11.glScalef(1F, 1F, 1F);
					GL11.glTranslatef(0.1F, 0.11F, 0.028F);
					renderAtt.renderBarska(Utils.isPlayerAiming());
					GL11.glPopMatrix();
				}
				//render scope//
				if(AttachmentType.checkAttachment(is,"stvol", Items.natoSilencer)){
				     GL11.glPushMatrix();
				     GL11.glTranslatef(0.05F, 0.02F, -0.02F);
					 renderAtt.renderPBS();
					 GL11.glPopMatrix();
					
				}	
				if(AttachmentType.checkAttachment(is,"grip", Items.laser)){
					 GL11.glPushMatrix();
					 GL11.glScalef(0.05F, 0.05F, 0.05F);
					 GL11.glTranslatef(17.7F, -7.8F, -1.47F);
		           renderAtt.renderLaser(type);
		           GL11.glPopMatrix();
				 }
	}
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){
    	
    	GL11.glPushMatrix();
		ru.xlv.core.util.Utils.bindTexture(tex);
		GL11.glScalef(0.42F, 0.42F, 0.42F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0, 0, 0);
		model.renderPart("gun");
		NBTTagCompound tagz = is.getTagCompound();
		if(tagz != null) {	
			if(tagz.getString("scope").length() > 0 || tagz.getString("stvol").length() > 0 || tagz.getString("laser").length() > 0) {
				renderAttachments(type, is);
			}
		}
		
		GL11.glPopMatrix();
    }
    
}

