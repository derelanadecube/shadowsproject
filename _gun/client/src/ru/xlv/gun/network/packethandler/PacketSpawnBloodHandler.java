package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.gun.entity.ParticleBlood;
import ru.xlv.gun.network.packets.PacketSpawnBlood;

import java.util.Random;

public class PacketSpawnBloodHandler implements IMessageHandler<PacketSpawnBlood, IMessage> {

	public static Random random = new Random();

	@SideOnly(Side.CLIENT)
	@Override
	public IMessage onMessage(PacketSpawnBlood message, MessageContext ctx) {
		EntityPlayer clientPlayer = Minecraft.getMinecraft().thePlayer;
		double x = message.x;
		double y = message.y;
		double z = message.z;
		int count;
		if (message.isHead) {
			count = 64;
		} else {
			count = 16;
		}
		for (int i = 0; i < count; i++) {
			float xRandMotion = (random.nextInt(2) == 0 ? -1 : 1) * random.nextFloat() * 0.5f;
			float yRandMotion = (random.nextInt(2) == 0 ? -1 : 1) * random.nextFloat() * 0.5f;
			float zRandMotion = (random.nextInt(2) == 0 ? -1 : 1) * random.nextFloat() * 0.5f;
			Minecraft.getMinecraft().effectRenderer.addEffect(new ParticleBlood(clientPlayer.worldObj, x, y, z, xRandMotion, yRandMotion, zRandMotion));
		}
		return null;
	}
}
