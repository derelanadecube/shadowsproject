package ru.xlv.gun.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ru.xlv.gun.inventory.InventoryGunModTable;

public class ContainerGunModTable extends Container {

   private InventoryGunModTable inventory;
   public World world;

   public ContainerGunModTable(InventoryPlayer i, World w) {
      this.inventory = new InventoryGunModTable();
      this.world = w;
      SlotGun gunSlot = new SlotGun(this.inventory, 0, 80, 110, null);
      this.addSlotToContainer(gunSlot);
      this.addSlotToContainer(new SlotGun(this.inventory, 1, 54, 110, gunSlot));
      this.addSlotToContainer(new SlotGun(this.inventory, 2, 80, 84, gunSlot));
      this.addSlotToContainer(new SlotGun(this.inventory, 3, 106, 110, gunSlot));
      this.addSlotToContainer(new SlotGun(this.inventory, 4, 80, 136, gunSlot));

      for (int row = 0; row < 4; ++row) {
         for (int col = 0; col < 2; ++col) {
            this.addSlotToContainer(new SlotGun(this.inventory, 5 + row * 2 + col, 10 + col * 18, 83 + row * 18, gunSlot));
         }
      }

      for (int row = 0; row < 3; ++row) {
         for (int col = 0; col < 9; ++col) {
            this.addSlotToContainer(new Slot(i, col + row * 9 + 9, 8 + col * 18, 176 + row * 18));
         }
      }

      for (int col = 0; col < 9; ++col) {
         this.addSlotToContainer(new Slot(i, col, 8 + col * 18, 234));
      }
   }

   public void onContainerClosed(EntityPlayer player) {
      if (this.inventory.getStackInSlot(0) != null) {
         player.dropPlayerItemWithRandomChoice(this.inventory.getStackInSlot(0), false);
      }
   }

   public boolean canInteractWith(EntityPlayer entityplayer) {
      return true;
   }

   public ItemStack transferStackInSlot(EntityPlayer player, int slotID) {
      ItemStack stack = null;
      Slot currentSlot = (Slot) super.inventorySlots.get(slotID);
      if (currentSlot != null && currentSlot.getHasStack()) {
         ItemStack slotStack = currentSlot.getStack();
         stack = slotStack.copy();
         if (slotID >= 13) {
            return null;
         }

         if (!this.mergeItemStack(slotStack, 13, super.inventorySlots.size(), true)) {
            return null;
         }

         if (slotStack.stackSize == 0) {
            currentSlot.putStack(null);
         } else {
            currentSlot.onSlotChanged();
         }
         if (slotStack.stackSize == stack.stackSize) {
            return null;
         }
         currentSlot.onPickupFromSlot(player, slotStack);
      }
      return stack;
   }
}
