package ru.xlv.gun.asm;

import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockTallGrass;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import ua.agravaine.hooklib.asm.Hook;
import ua.agravaine.hooklib.asm.ReturnCondition;

public class XlvsHooks {

    @Hook(returnCondition = ReturnCondition.ALWAYS, createMethod = true)
    public static AxisAlignedBB getCollisionBoundingBoxFromPool(BlockLeaves that, World world, int x, int y, int z) {
        return null;
    }

    @Hook(returnCondition = ReturnCondition.ALWAYS, createMethod = true)
    public static AxisAlignedBB getCollisionBoundingBoxFromPool(BlockTallGrass that, World world, int x, int y, int z) {
        return null;
    }

    @Hook(returnCondition = ReturnCondition.ALWAYS)
    public static boolean isOpaqueCube(BlockLeaves blockLeaves) {
        return false;
    }
}
