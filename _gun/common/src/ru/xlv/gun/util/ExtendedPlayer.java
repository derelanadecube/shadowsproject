package ru.xlv.gun.util;


import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;
import ru.xlv.gun.inventory.InventoryCustomPlayer;

public class ExtendedPlayer implements IExtendedEntityProperties {

   public final InventoryCustomPlayer inventory = new InventoryCustomPlayer();
   public static final String EXT_PROP_NAME = "ExtendedPlayer";
   private final EntityPlayer player;
   public EntityPlayer otherPlayer;
   public boolean readyToTrade = false;
   public int moneyInTradeOtherPlayer;
   private int timer;
   private int MaxTimer;
   private boolean ReloadKeyPressed;
   private boolean UnloadKeyPressed;
   private boolean ShootKeyPressed;
   private int soundTimer;
   private int shootTimer;

   public float bulletDefence;

   public ExtendedPlayer(EntityPlayer player) {
      this.player = player;
      this.timer = this.MaxTimer = 21;
      this.shootTimer = 0;
      this.bulletDefence = 0F;
      this.ReloadKeyPressed = false;
      this.ShootKeyPressed = false;
   }

   public static void register(EntityPlayer player) {
      player.registerExtendedProperties("ExtendedPlayer", new ExtendedPlayer(player));
   }

   public static ExtendedPlayer get(EntityPlayer player) {
      ExtendedPlayer extendedPlayer = (ExtendedPlayer) player.getExtendedProperties("ExtendedPlayer");
      if(extendedPlayer == null) {
         register(player);
         return get(player);
      }
      return extendedPlayer;
   }

   public void saveNBTData(NBTTagCompound compound) {
      NBTTagCompound properties = new NBTTagCompound();
      this.inventory.writeToNBT(properties);
      properties.setInteger("TIMER", this.timer);
      properties.setInteger("MAXTimer", this.MaxTimer);
      properties.setInteger("ST", this.shootTimer);
      properties.setBoolean("ReloadKeyPressed", this.ReloadKeyPressed);
      properties.setBoolean("UnloadKeyPressed", this.UnloadKeyPressed);
      properties.setBoolean("ShootKeyPressed", this.ShootKeyPressed);
      properties.setFloat("bulletDef", this.bulletDefence);
      compound.setTag("ExtendedPlayer", properties);
   }

   public void loadNBTData(NBTTagCompound compound) {
      NBTTagCompound properties = (NBTTagCompound) compound.getTag("ExtendedPlayer");
      this.inventory.readFromNBT(properties);
      this.timer = properties.getInteger("TIMER");
      this.MaxTimer = properties.getInteger("MAXTimer");
      this.bulletDefence = properties.getFloat("bulletDef");
      this.shootTimer = properties.getInteger("ST");
      this.ReloadKeyPressed = properties.getBoolean("ReloadKeyPressed");
      this.UnloadKeyPressed = properties.getBoolean("UnloadKeyPressed");
      this.ShootKeyPressed = properties.getBoolean("ShootKeyPressed");
   }

   public void dropAllItems() {
      int i;
      for (i = 0; i < 3; ++i) {
         if (this.inventory.getStackInSlot(i) != null) {
            this.player.func_146097_a(this.inventory.getStackInSlot(i), true, false);
            this.inventory.setInventorySlotContents(i, null);
         }
      }
   }

//   public static void saveProxyData(EntityPlayer player) {
//      ExtendedPlayer playerData = get(player);
//      if (playerData != null) {
//         NBTTagCompound savedData = new NBTTagCompound();
//         playerData.saveNBTData(savedData);
//         CommonProxy.storeEntityData(getSaveKey(player), savedData);
//      }
//   }
//
//   public static void loadProxyData(EntityPlayer player) {
//      ExtendedPlayer playerData = get(player);
//      NBTTagCompound savedData = CommonProxy.getEntityData(getSaveKey(player));
//      if (savedData != null) {
//         playerData.loadNBTData(savedData);
//      }
//   }

   private static String getSaveKey(EntityPlayer player) {
      return player.getCommandSenderName() + ":" + "MPlayer";
   }

   public void init(Entity entity, World world) {}

   public int getST() {
      return this.shootTimer;
   }

   public void soundTimer() {
      this.soundTimer = 50;
   }

   public boolean soundTimerTick() {
      if (this.soundTimer > 0) {
         --this.soundTimer;
         return false;
      } else {
         this.soundTimer();
         return true;
      }
   }

   public float getBulletDef() {
      return this.bulletDefence;
   }

   public void setBulletDefence(float f) {
      this.bulletDefence = f;
   }

   public int ST() {
      return this.shootTimer;
   }

   public void consumeST() {
      if (this.shootTimer > 0) {
         --this.shootTimer;
      }

   }

   public void upST(int i) {
      this.shootTimer = i;
   }

   public boolean Timerset() {
      if (this.timer == 0) {
         this.replenishTimer();
         return true;
      } else {
         --this.timer;
         return false;
      }
   }

   public void replenishTimer() {
      this.timer = 21;
   }

   public void setReloadKeyPressed() {
      this.ReloadKeyPressed = true;
   }

   public void setReloadKeyUnpressed() {
      this.ReloadKeyPressed = false;
   }

   public boolean getReloadKeyPressed() {
      return this.ReloadKeyPressed;
   }

   public void setUnloadKeyPressed() {
      this.UnloadKeyPressed = true;
   }

   public void setUnloadKeyUnPressed() {
      this.UnloadKeyPressed = false;
   }

   public boolean getUnloadKeyPressed() {
      return this.UnloadKeyPressed;
   }

   public void setShootKeyPressed() {
      this.ShootKeyPressed = true;
   }

   public void setShootKeyUnpressed() {
      this.ShootKeyPressed = false;
   }

   public boolean getShootKeyPressed() {
      return this.ShootKeyPressed;
   }
}
