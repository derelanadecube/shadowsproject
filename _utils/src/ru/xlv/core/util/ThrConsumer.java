package ru.xlv.core.util;

import java.io.IOException;

public interface ThrConsumer<T> {

    void accept(T t) throws IOException;
}
