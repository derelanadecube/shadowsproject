package ru.xlv.training.network;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.training.XlvsTrainingMod;
import ru.xlv.training.common.trainer.ITrainer;
import ru.xlv.training.common.trainer.TrainerIO;

import java.io.IOException;

@NoArgsConstructor
public class PacketTrainingSync implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        ITrainer trainer = TrainerIO.read(bbis);
        XlvsTrainingMod.INSTANCE.setActiveTrainer(trainer);
    }
}
