package ru.krogenit.render;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.training.XlvsTrainingMod;
import ru.xlv.training.common.trainer.ITrainer;
import ru.xlv.training.common.trainer.MarkerTrainer;
import ru.xlv.training.common.trainer.NpcInteractTrainer;

import static org.lwjgl.opengl.GL11.*;

public class TrainerRenderer {

    private static final Model markModel = new Model(new ResourceLocation("training", "models/mark.obj"));
    private static final TextureDDS markTexture = new TextureDDS(new ResourceLocation("training", "textures/mark.dds"));
    private boolean isScaling;
    private final Vector3f animation = new Vector3f(2f, 2f, 2f);

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(RenderWorldLastEvent event) {
        ITrainer activeTrainer = XlvsTrainingMod.INSTANCE.getActiveTrainer();
        if(activeTrainer != null) {
            if(activeTrainer instanceof MarkerTrainer) {
                MarkerTrainer markerTrainer = (MarkerTrainer) activeTrainer;
                render(markerTrainer);
            } else if(activeTrainer instanceof NpcInteractTrainer) {
                NpcInteractTrainer npcInteractTrainer = (NpcInteractTrainer) activeTrainer;
                String npcName = npcInteractTrainer.getNpcName();

            }
        }
        else {
//            render(new MarkerTrainer(new WorldPosition(0, -11, 45, -50), 1, "FASFA"));
        }
    }

    private void render(MarkerTrainer markerTrainer) {
        glPushMatrix();
        glDisable(GL_CULL_FACE);
        glEnable(GL_BLEND);
        glAlphaFunc(GL_GREATER, 0.0000001f);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDepthMask(false);
        float minValue = 1f;
        float maxValue = 1.5f;
        float minSpeed = 0.0004f;
        float speed = 0.04f;

        if(!isScaling) {
            animation.x = AnimationHelper.getSlowAnim(animation.x, minValue, maxValue,-speed, -minSpeed);
            if(animation.x <= minValue) {
                isScaling = true;
                animation.x = minValue;
            } else if(animation.x >= maxValue) {
                isScaling = false;
                animation.x = maxValue;
            }
        } else {
            animation.x = AnimationHelper.getSlowAnim(animation.x, minValue, maxValue, speed, minSpeed);
            if(animation.x >= maxValue) {
                isScaling = false;
                animation.x = maxValue;
            } else if(animation.x <= minValue) {
                isScaling = false;
                animation.x = minValue;
            }
        }

        WorldPosition worldPosition = markerTrainer.getWorldPosition();
        GL11.glTranslatef(-(float) TileEntityRendererDispatcher.staticPlayerX + (float)worldPosition.getX(),
                -(float) TileEntityRendererDispatcher.staticPlayerY + (float)worldPosition.getY(),
                -(float) TileEntityRendererDispatcher.staticPlayerZ + (float)worldPosition.getZ());
        GL11.glScalef(animation.x, 1.5f, animation.x);

        renderMark();

        glDepthMask(true);
        glAlphaFunc(GL_GREATER, 0.5f);
        glDisable(GL_BLEND);
        glEnable(GL_CULL_FACE);
        glPopMatrix();
    }

    private void renderMark() {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        shader.setLightMapping(false);
        shader.useLighting(false);
        shader.setColor(2f, 1.5f, 5f, 1f);
        TextureLoaderDDS.bindTexture(markTexture);
        markModel.render(shader);
        shader.setColor(1f, 1f, 1f, 1f);
        shader.useLighting(true);
    }

    private void renderDefaultMark() {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(true);
        shader.setLightMapping(false);
        shader.setUseTexture(false);
        glShadeModel(GL_SMOOTH);

        float height = 64;
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        float r = 1f;
        float g = 1f;
        float b = 1f;
        tessellator.setColorRGBA_F(r, g, b, 0.25f);
        tessellator.addVertexWithUV(0f, 0f, -0.5f, 0f, 0f);
        tessellator.addVertexWithUV(1f, 0f, -0.5f, 1f, 0f);
        tessellator.setColorRGBA_F(r, g, b, 0f);
        tessellator.addVertexWithUV(1f, 1f * height, -0.5f, 1f, 1f);
        tessellator.addVertexWithUV(0f, 1f * height, -0.5f, 0f, 1f);

        tessellator.setColorRGBA_F(r, g, b, 0.25f);
        tessellator.addVertexWithUV(0.5f, 0f, 0f, 0f, 0f);
        tessellator.addVertexWithUV(0.5f, 0f, -1f, 1f, 0f);
        tessellator.setColorRGBA_F(r, g, b, 0f);
        tessellator.addVertexWithUV(0.5f, 1f * height, -1f, 1f, 1f);
        tessellator.addVertexWithUV(0.5f, 1f * height, 0f, 0f, 1f);
        tessellator.draw();

        glShadeModel(GL_FLAT);
        shader.setUseTexture(true);
        shader.setLightMapping(true);
    }
}
